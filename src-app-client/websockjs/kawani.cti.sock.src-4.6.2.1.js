/*!
 * Kawani -- An open source Kawani Websocket Client toolkit.
 *
 * Copyright (C) 2019 - 2021, (PDS) Peranti Digital Solusindo, PT.
 *
 * omen <jombi_par@yahoo.com>
 *
 * See http://perantidigital.co.id for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * <BEGIN>
 *
 * @product			smart.1.1
 * @version 		4.6.2 
 * @revision 		0.0.1
 * @date 			2021/07/21
 * 
 * </BEGIN>
 *
 */ 
const AST_AGENT_LOGOUT 		= 0;	/*!< your description on here .. */
const AST_AGENT_READY 		= 1;    /*!< your description on here .. */
const AST_AGENT_NOT_READY 	= 2;    /*!< your description on here .. */
const AST_AGENT_LOGIN 		= 3;    /*!< your description on here .. */
const AST_AGENT_BUSY 		= 4;    /*!< your description on here .. */
const AST_AGENT_ACW 		= 5;    /*!< your description on here .. */
const AST_AGENT_RESERVED 	= 6;    /*!< your description on here .. */
const AST_AGENT_TALKING 	= 7;    /*!< your description on here .. */
const AST_AGENT_REJECTED 	= 9;    /*!< your description on here .. */
const AST_AGENT_REGISTER 	= 10;   /*!< your description on here .. */
const AST_AGENT_CALLASSIGN 	= 11;   /*!< your description on here .. */
const AST_AGENT_IDLE		= 25;   /*!< your description on here .. */
const AST_SKILL_INBOUND 	= 1;    /*!< your description on here .. */
const AST_SKILL_OUTBOUND 	= 2;    /*!< your description on here .. */
const AST_SKILL_PREDICTIVE 	= 3;    /*!< your description on here .. */
const AST_CALL_PREDICTIVE 	= 3;    /*!< your description on here .. */
const AST_CALL_OUTBOUND 	= 2;    /*!< your description on here .. */
const AST_CALL_INBOUND 		= 1;    /*!< your description on here .. */
const AST_CALL_DEVELOPER 	= 0; 	/*!< matikan log di sisi clients */  
const AST_CALL_MASKING 		= 0; 	/*!< matikan masking di sisi clients */ 

// MASTER REF
window.WebSocketInterval = 0;
window.WebSocketTimeout = 10000;
var fd_libs = { 
  // INBOUND:
	1 : {
		999:'Call Incoming Ringing',  
		100:'Call Incoming Ringing', 
		101:'Call Incoming Ringing',  
		102:'Call Incoming Ringing',  
		201:'Call Incoming Ringing',  
		202:'Call Incoming Connected',  
		300:'Call Incoming Answered', 
		301:'Call Incoming Ignore',
		302:'Call Incoming Congestion',
		303:'Call Incoming Rejected',
		318:'Call Incoming Misscall'
	},
// OUTBOUND:	
   2 : {
		999:'Call Iniate', 
		100:'Call Offered', 
		101:'Call Dialing', 
		102:'Call Ring', 
		201:'Call Ringing', 
		202:'Call Connected', 		 
		300:'Call Answered', 
		301:'Call Busy',
		302:'No User Response',
		303:'No Answered',
		304:'Call Rejected',
		305:'Change Number',
		306:'Destination Out Of Order',
		307:'Invalid Number Format',
		308:'Facility Rejected',
		309:'Response To Status Enquiry',
		311:'Normal Unspecified',
		312:'Normal Circuit Congestion',
		313:'Network Out Of Order',
		314:'Chan Not Implemented'
	},
	// PREDICTIVE:
	3 : {
		999:'Predictive from ',  
		100:'Predictive Ringing', 
		101:'Predictive with',  
		102:'Predictive from',  
		201:'Predictive from',  
		202:'Predictive Connected',  
		300:'Predictive Answered', 
		301:'Predictive Ignore',
		302:'Predictive Congestion',
		303:'Predictive Rejected',
		310:'Predictive Routed',
		318:'Predictive Misscall'
	}
};

// New On MediaChannel source
if ( typeof fd_Media != 'object' ){
 var fd_Media = {
	'IVR'   : { id: 1, name: 'IVR'  }, 	/*!< media channel event IVR blaster 	*/
    'Call'  : { id: 2, name: 'IVR'  }, 	/*!< media channel event Call directly 	*/	
	'Sms'   : { id: 3, name: 'SMS'  },	/*!< media channel event SMS directly	*/
	'Mail'  : { id: 4, name: 'Mail' },	/*!< media channel event Mail directly  */
	'Chat'  : { id: 5, name: 'Chat' }	/*!< media channel event Chat directly	*/
 }; 
}
// https://www.codegrepper.com/code-examples/typescript/how+to+check+browser+type+with+javascript
window.strtotime = function( now ){
	var retval = ''; if ( typeof now == 'undefined' ){
		var now = Date.now();
	}
	// finally "string"
	retval = fd_string(now);
	return retval;
} 
// https://www.codegrepper.com/code-examples/typescript/how+to+check+browser+type+with+javascript
window.detectBrowser = function(){ 
   /*!< OP= Opera , CH= Chrome, SF= Safari, FF= Firefox, IE= MSIE, DF= Unknown */
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
        return 'OP';
    } else if(navigator.userAgent.indexOf("Chrome") != -1 ) {
        return 'CH';
    } else if(navigator.userAgent.indexOf("Safari") != -1) {
        return 'SF';
    } else if(navigator.userAgent.indexOf("Firefox") != -1 ){
        return 'FF';
    } else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {
        return 'IE';//crap
    } else {
        return 'DF';
    }
	return "";
}  
// http://www.navioo.com/javascript/tutorials/Javascript_microtime_1583.html
window.microtime = function(get_as_float) {  
    // Returns either a string or a float containing the current time in seconds and microseconds    
    //   
    // version: 812.316  
    // discuss at: http://phpjs.org/functions/microtime  
    // +   original by: Paulo Ricardo F. Santos  
    // *     example 1: timeStamp = microtime(true);  
    // *     results 1: timeStamp > 1000000000 && timeStamp < 2000000000  
    var now = new Date().getTime() / 1000;  
    var s = parseInt(now);  
    return (get_as_float) ? now : (((now - s) * 1000)/1000).toFixed(8) + ' ' + s;  
}  

if ( typeof(window.sprintf) != 'function' ){   
window.sprintf = function( format ) {
  for( var i=1; i < arguments.length; i++ ) {
	format = format.replace( /%s/, arguments[i] );
  }
  return format;
};
}
if ( typeof(window.strcmp) != 'function' ){  
	window.strcmp = function( key, val ) {
		if( key!== null && key.localeCompare(val) == 0 ){
			return true;
		} 
		return false;
	};
}
if ( typeof(window.fd_isfunc) != 'function' ){
function fd_isfunc(data){
	if( typeof(data) == 'function' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_isobj) != 'function' ){
function fd_isobj(data){
	if( typeof(data) == 'object' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_undef) != 'function' ){
function fd_undef(data){
	if( typeof(data) == 'undefined' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_sigint) != 'function' ){
function fd_sigint(data){
	return parseInt(data);
 } 
}
if( typeof(window.fd_string) != 'function' ){
function fd_string(data){
	if( fd_undef(data) ){
		return "";
	}
	return data.toString();
 } 
}
if( typeof(window.fd_length) != 'function' ){
function fd_length(data){
	return fd_string(data).length;
 } 
}

if( typeof(window.fd_msg) != 'function' ){
function fd_msg(data){
	window.alert(data);
 } 
}
if( typeof(window.fd_unixtime) != 'function' ){
function fd_unixtime(time){
	var time_to_show = fd_sigint(time); // unix timestamp in seconds
	var t = new Date(time_to_show * 1000);
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
}
}
if( typeof(window.timestamp) != 'function' ){
 function timestamp(){
	var t = new Date();
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
 }
} 
if( typeof(window.zeropad) != 'function' ){
 window.zeropad = function(nr,base){
  var  len = (String(base).length - String(nr).length)+1;
  return len > 0? new Array(len).join('0')+nr : nr;
 }
}
 
if( typeof(window.duration) != 'function' ){
window.duration = function(second){
	var sec = 0, min = 0, hour= 0; 
	var retval = ""; 
	// next process calculation :
	sec = second%60; 
    second = Math.floor(second/60);  
	if(second){
		min  = second%60;
        hour = Math.floor(second/60);
    }
	if(second== 0 && sec == 0) return "";
    else {
	  retval = window.sprintf("%s:%s:%s", zeropad(hour,10), zeropad(min, 10), zeropad(sec,10)); 
	}
	return retval;
 }
} 
/**
 * --TODO: 
 *  @method		 :  object class [writeloger]
 *	@description :	window.writeloger['onready', 'onConnect', 'onThread', 'onClose']
 */
if( typeof(window.writeloger) != 'function' ){
window.writeloger = function(message){
	var verbose='', logWrite = sprintf('%s WS >> %s',timestamp(), message);
	if(fd_isobj(message)){
		verbose = sprintf('VERBOSE:%s\n','WS');
		console.log(verbose);
		console.log(message);
		console.log('\n');
	}
	// not object:
	else if(!fd_isobj(message) &&(fd_length(logWrite)>0)){
		if(parseInt(AST_CALL_DEVELOPER)>0){
			console.log(logWrite);
		}
	}
	return;
 } 
}
/**
 * --TODO: 
 *  @method		 :  object class [ctimasking]
 *	@description :	window.ctimasking[string]
 */
if( typeof(window.ctimasking) != 'function' ){
window.ctimasking = function(vl){
 if(fd_sigint(AST_CALL_MASKING)<1) return fd_string(vl);
 else if(fd_sigint(AST_CALL_MASKING)>0) {
	var lt = 3, rs = [], retval = '';
	if(fd_length(vl)<1) return retval;
	else if(fd_length(vl)>2){
		var fs = fd_string(vl).split(''), ar= [], dl= Math.round((fs.length/lt)), ts = 0, sl = null;
		for(var i = 0; i<dl; i++){
			ts = (lt*i);
			if(!ts){
				sl = fs.slice(ts,lt); sv = sl.join('');
				if(fd_length(sv)>0){ 
					ar.push(fd_string(sv));
				}
			}
			// jika ts lebih besar untuk data process 
			if(ts>0){
				sl = fs.slice(ts,lt+ts); sv = sl.join('');
				if(fd_length(sv)>0){ 
				  ar.push(fd_string(sv));
				} 
			}
		}
		
		// INFO: check setiap block  
		if(!fd_isobj(ar)) return retval;
		else if(fd_isobj(ar)) {
			for(var j in ar){
				if(j%2!=0) {
					var rv = ar[j].split(''); for( var ri in rv ){
						rs.push('x');
					}
				}
				if(j%2==0) rs.push(ar[j]);  
			}
		}
		// INFO : nilai balikan data process OK 
		retval = (fd_isobj(rs)?rs.join('') : '');
     }
	 return fd_string(retval);
   }
 } 
}
 
/**
 * --TODO: 
 *  @method		 :  object class [ctiKawani]
 *	@description :	window.ctiKawani['onready', 'onConnect', 'onThread', 'onClose']
 */
function ctiKawani( p1, p2, p3, p4, p5){ 
	this.onType 	= 'ws';
	this.onSocket 	= null;
	this.onHost 	= null;
	this.onPort 	= null;
	this.onReady 	= p1;
	this.onConnect 	= p2;
	this.onThread 	= p3;
	this.onClose 	= p4; 
	this.onError 	= p5; 
	this.onUrl		= null;
	this.onReject	= 0;
	return this;
};
// ctiKawani.setConfigServer[object] 
ctiKawani.prototype.setConfigServer = function(param){
	if(fd_isobj(param)){
		this.session = param;
		// tambahan untuk session IVR khusus Inbound 
		if(fd_undef(this.session.ivr)){
			this.session.ivr = {};
		}									   				 
		if(fd_undef(this.session.caller)){
			this.session.caller = {};
		}
	}
};
// ctiKawani.setConnectServer[boolean, object] 
ctiKawani.prototype.setConnectServer = function(sockStatus, server){
	// console.log(server);
	if(sockStatus > 0 &&(fd_sigint(server.readyState)>0)){
		var p = ctiClient.session.label.serverState;
		if( fd_isobj(p)){
			writeloger('INFO: Connecting ...');
			p.html('Connecting ...');
		}
		ctiClient.setAgentRegister();
	}
};
// ctiKawani.getClientSession[string] 
ctiKawani.prototype.getClientSession = function(key){
	var param = this.session.client, result = '';
	//console.log(param);	
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};
// ctiKawani.getCallerSession[string] 
ctiKawani.prototype.getCallerSession = function(key){
	var param = this.session.caller,
		result = "";
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};
// ctiKawani.getIvrSession[string] 
ctiKawani.prototype.getIvrSession = function(key){
	var param = this.session.ivr, result = "";
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};		

// ctiKawani.setStringToBinary[string] 
ctiKawani.prototype.setStringToBinary = function(buff){
 var buffs = buff.split(''), byteASCII = new Array(), byteString = '';
 for(var i = 0; i<buffs.length; i++){
	byteASCII.push(buff.charCodeAt(i)); 
 } 
 // set to data string;
 if(byteASCII.length>1){
	byteString = byteASCII.join('x');
 }
 return fd_string(byteString);
 
}
	
// ctiKawani.setBinaryToString[string] 
ctiKawani.prototype.setBinaryToString = function(buff){
 var chr = buff.split('x'), message = '';
 for(var i in chr){
	 if(chr[i] == '13') continue;
	 else if(chr[i] == '10') continue;
	 else if(chr[i] != '') {
		message+= String.fromCharCode(chr[i]);
	}
 }
 //return to string:
 return fd_string(message);
}
	
// ctiKawani.setParseMessage[string] 
ctiKawani.prototype.setParseMessage = function( message ){
	var message = this.setBinaryToString(message);
	var param = message.split('|'),
		buffer = {};
	for( i in param  ){
		var ptr = param[i].split(':');
		if(fd_isobj(ptr)){
			buffer[ptr[0]] = ptr[1];
		}
	}	
	var tParam = {
		get : function( key ){
			if(!fd_undef(buffer[key])){
				return buffer[key];
			}
			return "";
		},
		fetch : function(){
			return (fd_isobj(buffer) ? buffer : false);
		}
	}
	return tParam;
};
// ctiKawani.setThreadCallState[object] 
ctiKawani.prototype.setThreadCallState = function(param){
	if(!fd_isobj(param)){
		return 0;
	}
	// typeof inbound 	
	var fd = {};
		fd.p1 = param.CallDirection;
		fd.p2 = param.CallStatus;
		fd.p4 = window.ctimasking(param.CallerID);
		fd.p7 = ctiClient.session.label.callState;
		//console.log(fd.p7);
	
	// inbound Call 
	if(fd_isobj(param) && fd.p1==1){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// outbound call 
	else if(fd_isobj(param) && fd.p1==2){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}	
	// Predictve Call
	else if(fd_isobj(param) && fd.p1==3){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// end md:
	return 0;
};
// ctiKawani.setThreadPeers[object] 
ctiKawani.prototype.setThreadPeers = function(param){
	// console.log(param);
	if( !fd_isobj(param) ){
		return 0;
	}
	// -- WARNING INFO :
	var fd = param.fetch();
		fd.p0 = ctiClient.session.label.extenState;
		
	// console.log(fd);
	// SIP:UNREGISTERED
	if(strcmp(fd.ReasonId,'101')){	
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Not Registered', fd.Extens)
			}
		});
		
		return 0;
	}
	// SIP:UKNOWN REASON
	else if(strcmp(fd.ReasonId,'102')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Not Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	// sip on data process Registered
	else if(strcmp(fd.ReasonId,'103')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	// sip data process on her look like 
	else if(strcmp(fd.ReasonId,'104')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	
	// sip on register on following data 
	else if(strcmp(fd.ReasonId,'105')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		return 0;
	}
	
	// SIP:Broken ACD Terjadi kesalahan pada saat distribute call 
	else if(strcmp(fd.ReasonId,'106')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Broken ACD', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s -- INFO: Broken Network', fd.Extens)
			}
		});
		
		// next to Timeout 
		window.setTimeout(function(){
		   fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));	
		},5000);
		return 0;
	}
	return 0;
};
// ctiKawani.setThreadLogin[object] 
ctiKawani.prototype.setThreadLogin = function(fd){ 
	if(!fd_isobj(fd)){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will__data 
	if(fd_length(fd.ReasonVal)<0){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will set _key 
	if(fd.ReasonVal =='success'){
		ctiClient.setClientSession('ctiAgentKey', fd.AgentKey);
		window.setTimeout(function(){
			var c = ctiClient.getLocalAgentStatus(fd.AgentId);
			if(c != null &&(c.status == AST_AGENT_NOT_READY)){
			  ctiClient.setAgentNotReady(c.reason);
			}else {
			  ctiClient.setAgentReady();
			}
		}, 1000);
		return 0;
	}
	else {
		// console.log(fd);
		fd_msg('-- WARNING -- Invalid Login');
		var p0 = ctiClient.session.label.agentState
		if( fd_isobj(p0)){
			p0.html(fd.Message);
		}
		// stop don't  try to connected . close(this);
		ctiClient.onReject ++; 
		return 0;
	}
	return 0;
};
// ctiKawani.setThreadRegister[object] 
ctiKawani.prototype.setThreadRegister = function(param){ 
	var fd = {}
		fd.p0 = param.ReasonVal;
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.extenState;
		
		// if length invalid 
		if ( fd_length(fd.p0)<1) {
			return 0;
		}
		
		// if response CTI "success"
		if ( fd.p0 == 'success' ){
			window.setTimeout( function(){
				ctiClient.setAgentLogin();
			}, 1000);
			return 0;
		}
		
		// if response CTI "failed"
		else {
			// then  console.log(param);
			writeloger( sprintf('WARNING: Invalid Register host=%s exts= %s used by other user.',param.Address, param.Extens) );
			if ( fd_isobj(fd.p1) ){
			   fd.p1.html('Register');
			}
			if ( fd_isobj(fd.p2) ) {
			  fd.p2.html('-- INFO: Rejected');
			}
			
			// Add Incremnet if Reject don't try connected 
			// again .!
			ctiClient.onReject ++;
			return 0;
		}
		return 0;
};
// ctiKawani.setThreadState[object] 
ctiKawani.prototype.setThreadState = function(param){ 
	var tParam = param.fetch(),
		tTrack = tParam.Value; 
		
	var fd = {};
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.callState; 
		fd.p3 = ctiClient.session.label.serverState; 
		
	 
	// on track agents:	
	if(fd_sigint(tTrack) == AST_AGENT_LOGOUT){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(fd_isobj(fd.p1)){
		   fd.p1.html('logout');
		   fd.p3.html('');
		}
		if(fd_isobj(fd.p2)){
		   fd.p2.html('');
		}
		return 0;
	}
	
	// on track agents:	
	if(fd_sigint(tTrack) == AST_AGENT_REGISTER){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(fd_isobj(fd.p1)){
		   fd.p1.html('Register');
		}
		if(fd_isobj(fd.p2)){
		   fd.p2.html('idle');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_LOGIN){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(!fd_undef(tParam.Skill) &&(fd_isfunc(ctiClient.setThreadSkill))){
			ctiClient.setThreadSkill.apply(this, new Array(param)); 
		}
		if(fd_isobj(fd.p1)){
			fd.p1.text('Login');
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_READY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(!fd_undef(tParam.Skill) &&(fd_isfunc(ctiClient.setThreadSkill))){
		  ctiClient.setThreadSkill.apply(this, new Array(param));
		}
		// then :
		if(fd_isobj(fd.p1)){
			fd.p1.html('Ready');
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		} 
	}
	else if(fd_sigint(tTrack) == AST_AGENT_NOT_READY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(!fd_undef(tParam.Skill) &&(fd_isfunc(ctiClient.setThreadSkill))){
			ctiClient.setThreadSkill.apply(this, new Array(param));
		}
		// then :
		var p1 = tParam.ReasonVal, p2 = 'Not Ready';
		if(fd_length(p1)> 2){
			p2 = sprintf('%s ( %s )', p2, p1);
		}
		if(fd_isobj(fd.p1)){
			fd.p1.html(p2);
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		} 
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_BUSY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.html('Busy');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_ACW){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.html('Acw');
		}
		return 0;
	}
	return 0;
};
// ctiKawani.setCallIvrData[string, string] 
ctiKawani.prototype.setCallIvrData = function(key, val){ 
	ctiClient.session.ivr[key] = val; 
	return 0;
};	
// ctiKawani.setCallSession[string, string] 
ctiKawani.prototype.setCallSession = function(key, val){ 
	ctiClient.session.caller[key] = val; 
	return 0;
};
// ctiKawani.setClientSession[string, string] 
ctiKawani.prototype.setClientSession = function(key, val){ 
	ctiClient.session.client[key] = val; 
	return 0;
};

// ctiKawani.setThreadError[string]
ctiKawani.prototype.setThreadError = function(p){ 
var tParam = (typeof(p) == 'object' ? p: false);
 if(fd_isobj(tParam)){
	 ctiClient.setIntefaceThread({
		event : fd_string(p.event),
		body : {
			type : fd_string(p.type),
			data : {
				header  : fd_string(p.body.header),	
				message : fd_string(p.body.message)
			} 
		}
	}); 
 }
 // finnaly return data :
 return 0;
}
	

// ctiKawani.setThreadAlarm[object]
ctiKawani.prototype.setThreadAlarm = function(p){ 
 var fds = p.fetch(); 
 if((fds.Action == 'Alarm')){
	 var fd_state = ctiClient.session.label.extenState;
	 if(fd_isobj(fd_state)){
		var error = sprintf("* PBX : %s -- INFO: %s",fds.ServerHost, fds.ServerError);
		fd_state.html(fd_string(error));
	}
 }
 return 0;
}

// ctiKawani.setThreadTimout[object]
ctiKawani.prototype.setThreadTimout = function(p){ 
 var fds = p.fetch(); 
 if((fds.Action == 'Expired')){
	 ctiClient.setClientSession('ctiClientTimeout', fds.SockId); 
	 var fd_state = ctiClient.session.label.extenState;
	 if(fd_isobj(fd_state)){
		writeloger(sprintf("Got MSG INFO = Timeout #ID= %s #PID= %s  MSG ERR = %s", fds.SockId, fds.SockPid, fds.SockErr))
		
	}
 }
 return 0;
}
// ctiKawani.setThreadTransferCall[object]
ctiKawani.prototype.setThreadTransferCall = function(p){ 
 var tParam = p.fetch();  
 if(fd_isobj(tParam) &&(!fd_undef(tParam.CallSessionId))){
	 if(fd_undef(tParam.EventState)) tParam.EventState = 1; // default 
	 ctiClient.setIntefaceThread({
		event : fd_string('transfercall'),
		body : {
			type : fd_sigint(tParam.EventState),
			data : tParam 
		}
	}); 
 }
 // finnaly return data :
 return 0;
};
 
// ctiKawani.setThreadMediaChannel[object]
// 1:IVR , 2:Call, 3:SMS, 4:Mail,5:Chat  
ctiKawani.prototype.setThreadMediaChannel = function(p){ 
 var tParam = p.fetch();  
 if(fd_isobj(tParam) &&(!fd_undef(tParam.UniqueId))){
	 var MediaChannel = ctiClient.getMediaType(tParam.MediaType);
	 tParam.EventState = 0;
	 if( typeof(MediaChannel) == 'object' ){
		tParam.EventState = fd_sigint(MediaChannel.id);
	 }
	 ctiClient.setIntefaceThread({
		event : fd_string('mediachannel'),
		body : {
			type : tParam.EventState,
			data : tParam 
		}
	}); 
 }
 // finnaly return data :
 return 0;
};


// ctiKawani.setThreadForwardCall[object]
ctiKawani.prototype.setThreadForwardCall = function(p){ 
 var tParam = p.fetch();  
 if(fd_isobj(tParam) &&(!fd_undef(tParam.CallSessionId))){
	 if(fd_undef(tParam.EventState)) tParam.EventState = 1; // default 
	 ctiClient.setIntefaceThread({
		event : fd_string('forwardcall'),
		body : {
			type : fd_sigint(tParam.EventState),
			data : tParam 
		}
	}); 
 }
 // finnaly return data :
 return 0;
} 

// ctiKawani.setThreadParkCall[object]
ctiKawani.prototype.setThreadParkCall = function(p){ 
 var tParam = p.fetch(); 
 if(fd_isobj(tParam) &&(!fd_undef(tParam.SessionId))){
	 // createDocumentFragment Interface :
	 ctiClient.setIntefaceThread({
		event : fd_string('parkcall'),
		body : {
			type : fd_sigint(tParam.EventState),
			data : tParam 
		}
	}); 
 }
 // finnaly return data :
 return 0;		  
};

// ctiKawani.setThreadCallSplitIVR[object]
ctiKawani.prototype.setThreadCallSplitIVR = function(p){ 
 var tParam = p.fetch(); 
 if(fd_isobj(tParam) &&(!fd_undef(tParam.SessionId))){
	 // push data on here Like this;
	 ctiClient.setClientSession('ctiCallSplitIVRData',tParam.CallData);  
	 ctiClient.setIntefaceThread({
		event : fd_string('callsplitivr'),
		body : {
			type : fd_sigint(tParam.EventState),
			data : tParam 
		}
	}); 
 }
 // finnaly return data :
 return 0;
};


// ctiKawani.setThreadVoiceMail[object]
ctiKawani.prototype.setThreadVoiceMail = function(p){ 
	var fds = p.fetch(); 
	if(fd_isobj(fds) &&(!fd_undef(fds.VMCallerId))){
		var msgVoiceMail = sprintf('You have Voice Mail Message from = %s\nWith MessageId [%s]', fds.VMCallerId, fds.VMMessageId);
		if(fd_length(msgVoiceMail)>0){
			console.log(msgVoiceMail);
		}
		return 0;
	}
};

/** 
 * --- TODO : --- 
 * @method 		ctiKawani.setThreadReport[object] berfungsi untuk menerima event.disposition == report 
 * @object 		Example [
 *				Action: "99"
 *				Calling: "0"
 *				Disposition: "Info"
 *				EventGetVar: "Report"
 *				Process: "5"
 *				Ready: "0"
 *				Total: "5"
 *				Type: "PDS"
 *				Utilize: "0"
 * ]				
*/	
ctiKawani.prototype.setThreadReport = function(p){
  var tParam = p.fetch(); tSkill = window.ctiClient.getAgentSkill(), tType = (!fd_undef(tParam.Type) ? tParam.Type : '');
  if(fd_isobj(tParam) &&(tType== 'PDS')){ 
	tParam.Skill = fd_sigint(tSkill); // add object
	ctiClient.setIntefaceThread({
		event : fd_string('report'),
		body : {
			type : fd_sigint(tParam.Action),
			data : tParam 
		}
	});
 }
 return 0;
}

// ctiKawani.prototype.setThreadCallLater 
ctiKawani.prototype.setThreadCallLater = function(p){
  if(!fd_isobj(p)) return 0;
  else if(fd_isobj(p)){
	var tParam = p.fetch(); 
	  if(fd_isobj(tParam) &&(fd_sigint(tParam.CallLaterId))){  
		tParam.Action = 8;
		ctiClient.setIntefaceThread({
			event : fd_string('callater'),
			body : {
				type : fd_sigint(tParam.Action),
				data : tParam 
			}
		});
	  }
  }
 return 0;
}

// ctiKawani.setThreadSkill[object]
ctiKawani.prototype.setThreadSkill = function(p){
	if(!fd_isobj(p)) return 0;
	var tParam = p.fetch(); 
	if(fd_isobj(tParam) &&(fd_sigint(tParam.Skill)>0)){
		 ctiClient.setClientSession('ctiAgentSkill', fd_sigint(tParam.Skill)); tParam.Action = 9;
		 ctiClient.setIntefaceThread({
			 event : fd_string('skill'),
			 body : {
				type : fd_sigint(),
				data : tParam 
			}
		 });
	}
	return 0;
} 

// ctiKawani.setThreadPing[object]
ctiKawani.prototype.setThreadPing = function(p){ 
	var fds = p.fetch(); 
	//	console.log(fds);
	// nanti ini di check bugs -nya :
	if(fd_undef(fds.clientId)){
		var timeStamp = fd_string(fd_unixtime(fds.SockTime)),
			response = fd_string(fds.Response),
			disposition = fd_string(fds.Disposition),
			sockid  = fd_string(fds.SockId),
			socktype = fd_string(fds.SockType),
			sockpid = fd_string(fds.SockPid),
			sockport = fd_string(fds.SockPort),
			sockhost = fd_string(fds.SockHost);
		
			writeloger(sprintf('Got MSG INFO = %s #PID = %s #MSG = %s',disposition, sockpid, response));
			writeloger(sprintf('Type= %s #Host = %s #Id= %s #Port= %s',socktype, sockhost, sockid, sockport));
			return 0;
	}

	if(fd_isobj(fds)&&fd_sigint(fds.clientId)>0){
		var timeStamp = fd_unixtime(fds.clientTime);
		if(typeof(window.interfaceIdleTimer) == 'function' ){
			window.interfaceIdleTimer.apply(this, new Array(ctiClient));	
		}
		// then me ... 
		writeloger(sprintf('Got MSG INFO = %s',fds.EventGetVar));
		writeloger(sprintf('Type= %s #Host = %s #Id= %s #Port= %s',fds.clientType, fds.clientHost, fds.clientId, fds.clientPort ));
		
		if(fd_isfunc(ctiClient.setWebSocketPing)){
			ctiClient.setWebSocketPing();
		}
		var fd = {};
			fd.p1 = ctiClient.session.label.serverState;
			if(fd_isobj(fd.p1)){
				fd.p1.html('Now Connected.');
			}
	}  
	return 0;
};

// ctiKawani.setIntefaceThread[object{[type: string ] [type: int ] [data: object ] }]
ctiKawani.prototype.setIntefaceThread = function(data){ 
	var fd = {};
		fd.p0 = ctiClient.session['interface'];
	if(!fd_undef(fd.p0)&&fd_length(fd.p0)>3){
		window[fd.p0].apply(this, new Array(data));
		return;
	}
	return;
};

// ctiKawani.setThreadPing[object]
ctiKawani.prototype.setThreadCall = function(param){ 
	var p = param.fetch();
	
	// console.log(p);
	
	if(!fd_isobj(p)){
		return 0;
	}
	// add to list data local browser :
	if(fd_isobj(p)&&(typeof(p.CallIVRData)!='undefined' && fd_length(p.CallIVRData)>3)){
	   ctiClient.setCallSession('CallIVRData', p.CallIVRData);
	   if(callDataIVR = p.CallIVRData.split(";")){
		   // masukan data ke Session IVR :
		   if(typeof(callDataIVR) == 'object') for( var i in callDataIVR){ 
			  ctiClient.setCallIvrData(i,callDataIVR[i]);
		   }
	   }
	}									 
	if(fd_isobj(p)&&fd_length(p.CallerID) > 3 ){
		ctiClient.setCallSession('CallCallerId', p.CallerID);
	}
	if(fd_isobj(p)&&fd_length(p.CallDirection)>0){
		ctiClient.setCallSession('CallDirection', p.CallDirection);
	}
	if(fd_isobj(p)&&fd_length(p.CallCalling)>0){
		ctiClient.setCallSession('CallCalling', p.CallCaller);
	}
	if(fd_isobj(p)&&fd_length(p.CallChannel)>0){
		ctiClient.setCallSession('CallChannel1', p.CallChannel);
	}
	if(fd_isobj(p)&&fd_length(p.CallChannel2)>0){
		ctiClient.setCallSession('CallChannel2', p.CallChannel2);
	}
	
	if(fd_isobj(p)&&fd_length(p.CallCreateId)>0){
		ctiClient.setCallSession('CallCreateId', p.CallCreateId);
	}
	
	if(fd_isobj(p)&&fd_length(p.CallSessionId)>3){
		ctiClient.setCallSession('CallSessionId', p.CallSessionId);
	}
	if(fd_isobj(p)&&fd_length(p.CallStatus)>3){
		ctiClient.setCallSession('CallStatus', p.CallStatus);
	}
	if(fd_isobj(p)&&fd_length(p.CallTrunk)>3){
		ctiClient.setCallSession('CallTrunk', p.CallTrunk);
	}
	if(fd_isobj(p)&&fd_length(p.CallVDN)>3){
		ctiClient.setCallSession('CallVDN', p.CallVDN);
	} 
	if(fd_isobj(p)&&fd_length(p.CallAssignId)>0){
		ctiClient.setCallSession('CallAssignId', p.CallAssignId);
	} 
	
	// -- INFO -- user deleagate function to aksess 
	// from public method :
	if(fd_isfunc(ctiClient.setIntefaceThread)){
	   ctiClient.setIntefaceThread({
		   event : fd_string('callstate'),
		   body  : {
			   type : fd_sigint(p.CallDirection),
			   data : p 
		   }
		});
	} 
	return 0;
};

// ctiKawani.setThreadAgent[object]
ctiKawani.prototype.setThreadAgent = function(param){ 
	var tParam = param.fetch();
	// -- OPEN PUBLIC -- thread method :
	if(fd_isfunc(ctiClient.setIntefaceThread)){
		if(fd_isobj(tParam)&&fd_length(tParam.Extens)>2){
			ctiClient.setIntefaceThread({
				   event : fd_string('agentstate'),
				   body : {
					   type : fd_sigint(tParam.Value),
					   data : tParam 
				   }
			});
		}
	}
	
	if(fd_isobj(tParam)&&fd_length(tParam.Extens)>2){
		var tTrack = tParam.Value
		// repsonse on agent register :
		if(tTrack == AST_AGENT_LOGOUT && (fd_isfunc(ctiClient.setAgentDisconected))){
			return 0;
		}
		// repsonse on agent register :
		if(tTrack == AST_AGENT_REGISTER && (fd_isfunc(ctiClient.setAgentLogin))){
			if(fd_isfunc(ctiClient.setThreadRegister)){
				ctiClient.setThreadRegister.apply(this, new Array(tParam));
			}
			return 0;
		}
		// user login 
		else if(tTrack == AST_AGENT_LOGIN && (fd_isfunc(ctiClient.setAgentReady))){
			if(fd_isfunc(ctiClient.setThreadLogin)){
				ctiClient.setThreadLogin.apply(this, new Array(tParam));
			}
			return 0;
		}
		// agent Ready:
		else if(tTrack == AST_AGENT_READY && (fd_isfunc(ctiClient.setAgentNotReady))){
			window.callOpen = 0;
			ctiClient.setLocalAgentStatus(tParam.AgentId,{
				skill  : tParam.Skill,
				status : tParam.Value,
				result : tParam.Cause
			});	
			
			return 0;
		}
		// not ready 
		else if(tTrack == AST_AGENT_NOT_READY  && (fd_isfunc(ctiClient.setAgentNotReady))){
			window.callOpen = 0;
			ctiClient.setLocalAgentStatus(tParam.AgentId,{
				skill  : tParam.Skill,
				status : tParam.Value, 
				result : tParam.Cause
			});	
			return 0;
		}
		// Busy  
		else if(tTrack == AST_AGENT_BUSY  && (fd_isfunc(ctiClient.setAgentNotReady))){
			return 0;
		}
		// Acw  
		else if(tTrack == AST_AGENT_ACW  && (fd_isfunc(ctiClient.setAgentLogin))){
			window.callOpen = 0;
			return 0;
		}
	}
	return 0;
};


// ctiKawani.setThreadServer[string]
ctiKawani.prototype.setThreadServer = function(message){
	// console.log(message);
	var p = ctiClient.setParseMessage(message); 
	if(!fd_isobj(p)){
		return 0;
	}
	// get data 
	var events = p.get('EventGetVar'), param = p.fetch(); 
	//  console.log(events);
	// Thread baru Untuk Utils  example utils server triger from socket manager 
	if(events == 'Utils'){ 
		// cti triger to reload page to clients browser 
		if(fd_isobj(param) &&(param.UtilProcess == 'Reload')){
			document.location = document.location.href;
		}
		// cti triger to logout from page to clients browser 
		if(fd_isobj(param) &&(param.UtilProcess == 'Logout')){
			var windowURL = Ext.EventUrl( new Array( 'Auth', 'Logout' )).Apply();
			ctiClient.setAgentLogout(AST_AGENT_LOGOUT, function(obj){
				window.localStorage.clear();
				document.location= windowURL; //Adapt to actual logout script
	        });
		} 
	} 
	// Thread baru Untuk ChatHearbeat:
	else if(events == 'ChatHearbeat'){
		if(fd_isfunc(ctiClient.setThreadPing)){
			ctiClient.setThreadPing.apply(this, new Array(p));
		}
	}
	// Thread baru Untuk VoiceMail:
	else if(events == 'VoiceMail'){
		if(fd_isfunc(ctiClient.setThreadVoiceMail)){
			ctiClient.setThreadVoiceMail.apply(this, new Array(p));
		}
	}
	
	// Thread.ctiClient.setThreadParkCall
	else if(events == 'ParkCall'){
		if(fd_isfunc(ctiClient.setThreadParkCall)){
			ctiClient.setThreadParkCall.apply(this, new Array(p));
		}
	}
	// Thread.ctiClient.setThreadTransferCall
	else if(events == 'CallTransfer'){
		if(fd_isfunc(ctiClient.setThreadTransferCall)){
			ctiClient.setThreadTransferCall.apply(this, new Array(p));
		}
	}
	// Thread.ctiClient.setThreadForwardCall
	else if(events == 'CallForward'){
		if(fd_isfunc(ctiClient.setThreadForwardCall)){
			ctiClient.setThreadForwardCall.apply(this, new Array(p));
		}
	}
	// Thread.ctiClient.setThreadCallSplitIVR
	else if(events == 'CallSplitIVR'){
		if(fd_isfunc(ctiClient.setThreadCallSplitIVR)){
			ctiClient.setThreadCallSplitIVR.apply(this, new Array(p));
		}
	}
	// track get Media Channel Like 'Mail, SMS, IVR, CALL, CHAT'
	else if(events == 'MediaChannel'){
	   if(fd_isfunc(ctiClient.setThreadMediaChannel)){
		   ctiClient.setThreadMediaChannel.apply(this, new Array(p));
	   }
	}
	
	// track get report 
	else if(events == 'Report'){
	  if(fd_isfunc(ctiClient.setThreadReport)){
	   ctiClient.setThreadReport.apply(this, new Array(p));
	 }
	}
	
	// track get informations 
	else if(events == 'Info'){
		// setThreadPing
		if(!fd_undef(param.Disposition) &&(param.Disposition=='Chathearbeat')){
			if(fd_isfunc(ctiClient.setThreadPing)){
				ctiClient.setThreadPing.apply(this, new Array(p));
			}
		}
		// setThreadAlarm
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='Alarm')){
			if(fd_isfunc(ctiClient.setThreadAlarm)){
				ctiClient.setThreadAlarm.apply(this, new Array(p));
			}
		}
		// setThreadSkill
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='Skill')){
			if(fd_isfunc(ctiClient.setThreadSkill)){
				ctiClient.setThreadSkill.apply(this, new Array(p));
			}
		}
		// setThreadReport
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='Report')){
			if(fd_isfunc(ctiClient.setThreadReport)){
				ctiClient.setThreadReport.apply(this, new Array(p));
			}
		}			   
		// setThreadCallLater
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='CallLater')){
			if(fd_isfunc(ctiClient.setThreadCallLater)){
				ctiClient.setThreadCallLater.apply(this, new Array(p));
			}
		}
		// setThreadTimout
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='Timeout')){
			if(fd_isfunc(ctiClient.setThreadTimout)){
				ctiClient.setThreadTimout.apply(this, new Array(p));
			}
		}
	}
	// track on agent_state 
	else if(events == 'AgentState'){
		if(fd_isfunc(ctiClient.setThreadAgent)){
			ctiClient.setThreadState.apply(this, new Array(p));
			ctiClient.setThreadAgent.apply(this, new Array(p));
		}
	} 
	// track on call_state 
	else if(events == 'CallState'){
		if(fd_isfunc(ctiClient.setThreadCall)){
			ctiClient.setThreadCallState.apply(this, new Array(param));
			ctiClient.setThreadCall.apply(this, new Array(p));
		}
	}
	// track PeerStatus 
	else if(events == 'PeerStatus'){
		if(fd_isfunc(ctiClient.setThreadPeers)){
			ctiClient.setThreadPeers.apply(this, new Array(p));
		}
	}
	return 0;
};

// ctiKawani.setComposeMessage[string]
ctiKawani.prototype.setComposeMessage = function(fd, callback){
	var ptr = 0, buff = "", bytes = []; 
	if(!fd_isobj(fd)){
		return 0;
	} 
  /**
   * INFO: 20200825
   * Convert To Singgle String for easy process on backend and less data to efiesiensi
   * Buffered data Socket   
   */ 
	for(var i in fd ){
		bytes.push(sprintf('%s:%s',i,fd[i]));
		ptr+=1; // ptr+= sprintf("%s:%s\r\n",i,fd[i]); 
	} 
	// check validation process if empty back to NULL;
	if(ptr<1) return 0;
	else if(ptr>0){ 
		/** INFO :if user not deleagate function / callback  **/
		buff = fd_string(sprintf("%s\r\n", bytes.join('|'))); bytes = []; 	
		if(!fd_isfunc(callback)){
			if(fd_length(buff) == 0 ){
				return 0;
			}
			return buff;
		}
		// if user deleagatefunction callback 
		else if(fd_isfunc(callback)){
			callback.apply(this, new Array(buff, ctiClient));
			return 0;
		}
	}
	// reset data to null byte : 
	return;
	
};

// ctiKawani.setWriteMessage[string]
ctiKawani.prototype.setWriteMessage = function(message){
	var message = this.setStringToBinary(message);
	if(fd_isobj(window.ctiClient.onSocket)){
		window.ctiClient.onSocket.send(message);
	}
	return 0;
};

// ctiKawani.setWebSocketPrepare[void]
ctiKawani.prototype.setWebSocketPrepare = function( callback ){
  
 /*!
  * handleEvent for UniqueId Tab bro indetification 
  * every first open tab browser  application
  *
  */
  var ctiNavUniqueID = null;
  if ( sessionStorage.getItem( 'ctiNavUniqueID' ) === null ){
	 window.sessionStorage.setItem( 'ctiNavUniqueID', window.strtotime());
  }
  
 /*! 
  * then sent signal to client process for next step 
  * check function callback from client.
  */
  if ( typeof callback == 'function' ){
	ctiNavUniqueID = window.sessionStorage.getItem( 'ctiNavUniqueID' );
	if ( ctiNavUniqueID ){
		this.setClientSession( 'ctiNavUniqueID', ctiNavUniqueID );
		callback.apply(this, [window.ctiClient, ctiNavUniqueID]);
	}
	// retval == (string)
	return ctiNavUniqueID;
  }
 // retval == (string)
  return ctiNavUniqueID
}

// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketConnect = function(){
	this.onType  = this.session.server.type;
	this.onHost  = this.session.server.host;
	this.onPort  = this.session.server.port;
	this.onInfo  = this.session.label.serverState;
	this.onCall  = this.session.label.callState;
	this.onAgent = this.session.label.agentState;
	this.onExten = this.session.label.extenState;
	
	// wait if loost connection try to connected again */
	this.onWait  = 10000;
	if ( fd_sigint(window.WebSocketTimeout) > 0 ){ 
		this.onWait = window.WebSocketTimeout;
	}
	
	// set to URL :
	this.onUrl = window.sprintf('%s://%s:%s', this.onType, this.onHost, this.onPort);
	writeloger(sprintf("Connecting to Server= %s", this.onUrl));
	if ( typeof(this.onUrl) != 'string'){
		return 0;
	}
	
	// open connection to webSocket server 
	try{
		if ( typeof MozWebSocket == 'function' ) this.onSocket = new MozWebSocket(this.onUrl);
		else {
		   this.onSocket = new WebSocket(this.onUrl);
		}
		
		// detected browser
		this.userAgent = navigator.userAgent;
		if ( fd_isfunc(window.detectBrowser) ){
			this.userAgent = window.detectBrowser();
		}
		
		// masukan variable Browser;
		if ( fd_isfunc(ctiClient.setClientSession) ){
			ctiClient.setClientSession('ctiNavigator', this.userAgent);
		}
		
		var websock = this; /*!< scope local variable for this session */
		
		// EVENT: onopen
		// interface[ctiClient.setConnectServer]
		this.onSocket.onopen = function(evt){  
			writeloger(sprintf('INFO: %s', 'Initialize open ws:socket'));
			writeloger(sprintf('INFO: Browser Type: %s', websock.userAgent));
			
			if ( fd_isobj(evt) &&( fd_isfunc(ctiClient.setConnectServer) )){
				if ( fd_isfunc(window.clearTimeout)){
					window.clearTimeout(window.WebSocketInterval);
					websock.onReady = true;
				}
				// then back connect && detected for Chrome Browser
				websock.evensTarget= {};
				ctiClient.setClientSession('ctiClientTimeout',0);
				if ( !websock.userAgent.localeCompare('CH') ){
					websock.evensTarget = evt.target;
				}
				
				// detected Browser Firefox 
				if ( !websock.userAgent.localeCompare('FF') ){
					websock.evensTarget = evt.originalTarget;
				}
				
				// sent to Interface 
				// console.log(websock.evensTarget);
				ctiClient.setConnectServer.apply(this, new Array(1, websock.evensTarget));
				return 0;
			}	
		}
		
		// EVENT: onmessage
		// interface[ctiClient.setThreadServer]
		this.onSocket.onmessage = function(evt){
			writeloger(sprintf('INFO: Initialize message ws:socket'));
			if ( fd_isfunc(ctiClient.setThreadServer) ){
				ctiClient.setThreadServer.apply(this, new Array(evt.data));
				return 0;
			}	
		}
		
		// EVENT: onclose
		this.onSocket.onclose = function(evt){ 
			writeloger('INFO: Initialize close ws:socket');
			
			// check if register rejected;
			websock.onReject = fd_sigint( websock.onReject );
			if ( websock.onReject > 0 ) {
				return 0;
			}
			
			// stop break for rejected state 
			if ( websock.onReject <1 ) {
				var p1 = websock.onInfo,
					p2 = websock.onCall,
					p3 = websock.onAgent;
					
			   /**	
			    * INFO: 20201229
				*
			    * jika User membuka banyak tab dalam satu screen Browser maka 
			    * socket yang di anggap active hanya pada "New tab" , dan yang lain akan di 
			    * matikan 
				* 
				*/ 
				var _timeout = ctiClient.getClientSession('ctiClientTimeout'); 
				if ( fd_sigint( _timeout ) > 0 ){
					p1.html('');
					p2.html('-- INFO: Expired Session');
					p3.html('-- WARNING: CTI Disconected');
					
					
					// set On Thread Error 
					if( fd_isfunc(ctiClient.setThreadError) ){
					   ctiClient.setThreadError({
						 event : 'ctierror',  
						 type : 0,
						 body : {
							header  : 'CTI Disconected',
							message : 'Expired Session'	
						 }
					   }); 
					}
					// finally return 
					return 0;
				}
				
				if ( fd_isobj(p1) ){
					writeloger('INFO: Can\'t establish a Connection to the CTI');
					p1.html('-- INFO -- Can\'t establish a Connection to the CTI ');
				}
				if ( fd_isobj(p2) ){
					writeloger('INFO: Can\'t establish a Connection to the CTI (10) s');
					p2.html('-- INFO -- Can\'t establish a Connection to the CTI (10) s');
				}
				if ( fd_isobj(p3) ){
					writeloger('WARNING: CTI Disconected');
					p3.html('-- WARNING -- CTI Disconected.');
				}
				
				
				// set On Thread Error 
				if ( fd_isfunc(websock.setThreadError) ){
					websock.setThreadError({
						event : 'ctierror',  
						type  : 1,
						body : {
							header  : 'CTI Disconected',
							message : 'Can\'t establish a Connection to the CTI'	
						}
					}); 
				}	
				
				// try connect to the server .
				window.WebSocketInterval = window.setTimeout( function(){
					websock.onReady = false;
					if ( !websock.onReady ) {
						writeloger('INFO: Wait timout 10s to reconnecting ...');
					}
					ctiClient.setWebSocketReconnect(websock);
				}, websock.onWait); /*!< 10 seconds */ 
				return 0;
			}
		}
		
		// EVENT: onerror
		this.onSocket.onerror = function(evt){
			writeloger('INFO: Initialize error ws:socket');  /*!< write loger actived  */
			var p0 = websock.onInfo;  /*!< Local scope variable  */
			if ( fd_isobj(p0) ){
				p0.html('CTI Disconected'); 
				if ( fd_isfunc(websock.setThreadError) ){
					websock.setThreadError({
						event : 'ctierror',  
						type : 2,
						body : {
							header  : 'CTI Disconected',
							message : 'Initialize Error WS:Socket'	
						}
					});
				}
			}
		}
	}
	
	/*!< catch Error not definitively  */
	catch( error ){
		writeloger(error);
		return 0
	}
	return 0;
};

// ctiKawani.setWriteMessage[string]
ctiKawani.prototype.setWebSocketReconnect = function(webSocket){
	// -- INFO : clean session Key if disconected with server  
	ctiClient.setClientSession('ctiAgentKey', '');
	if(fd_isobj(webSocket.onInfo)){
		writeloger('INFO: ReConnecting to the CTI ...');
		webSocket.onInfo.html('-- INFO -- Re Connecting to the CTI...');
	}
	if(fd_isobj(webSocket.onAgent)){
		writeloger('INFO: CTI Disconected.');
		webSocket.onAgent.html('-- WARNING -- CTI Disconected');
	}
	if(fd_isobj(webSocket.onCall)){
		writeloger('NOTICE: ReConnect to CTI...');
		webSocket.onCall.html('-- INFO -- Re Connect...');
	}	
	
	
	// Set Erro from socket CTI 
	if( typeof( ctiClient.setThreadError ) == 'function'){
		ctiClient.setThreadError({
			event : 'ctierror',  
			type : 3,
			body : {
				header  : 'CTI Disconected',
				message : 'Trying Connecting to the CTI'	
			}
		});
	}
	
	// detect socket :
	if(fd_isobj(ctiClient)&&fd_isfunc(ctiClient.setWebSocketConnect) ){
		ctiClient.setWebSocketConnect();
	}
	return 0;	
};

// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketDisconect = function(timeout){
	if( fd_undef(timeout)){
	    var timeout = 0;
	}
	ctiClient.setClientSession('ctiClientTimeout',timeout);
	if( window.ctiClient.onSocket !== null &&(fd_isobj(window.ctiClient.onSocket))){
		ctiClient.onSocket.close();
	}
	return 0;
};
 
// ctiKawani.setClientPing[string, string]
ctiKawani.prototype.setWebSocketPing = function(){ 
	var 
	  ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	  ctiAgentExten = ctiClient.getClientSession('ctiAgentExten'),
	  ctiAgentHost  = ctiClient.getClientSession('ctiAgentHost'),
	  ctiAgentEvent = 4;
	  
   // -- VALIDATION : no have data to call be terminate. 
	if(fd_undef(ctiAgentExten)){
		return 0;
	}
  
   // -- VALIDATION : cannot be empty dialing number 
	if(fd_length(ctiAgentHost)<3){
		return 0;
	}
	
	// set Tiem Over Internet client for check Network Quality :
	var ctiReqTime = window.microtime(false);
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = ctiAgentEvent;
		fd.p2 = ctiAgentExten;
		fd.p3 = ctiAgentHost;
		fd.p4 = ctiReqTime;
		
	var param ={
			Action  : fd_string(fd.p0),
			Event   : fd_string(fd.p1),
			Extens  : fd_string(fd.p2),
			Address : fd_string(fd.p3),
			Timing  : fd_string(fd.p4)
		};
	ctiClient.setComposeMessage(param, function( lenBuffer, ctiClient ){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	}); 
	return 0;
}
// ctiKawani.setAgentDisconected[void]
ctiKawani.prototype.setAgentDisconected = function(){ 
  ctiClient.setWriteMessage('quit');
  return 0;
};

// ctiKawani.setAgentRegister[void]
ctiKawani.prototype.setAgentRegister = function(){
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = 10;
		fd.p3 = ctiClient.getClientSession('ctiAgentExten');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentId');
		fd.p6 = ctiClient.getClientSession('ctiNavigator');
		fd.p7 = ctiClient.getClientSession('ctiNavUniqueID');
		
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action 	 : fd_string(fd.p1),
			Event 	 : fd_string(fd.p2),
			Extens   : fd_string(fd.p3),
			AgentId  : fd_string(fd.p5),
			Address  : fd_string(fd.p4),
			Navigator: fd_string(fd.p6),
			UniqueId : fd_string(fd.p7)
		});
		
		// console.log(lenBuffer);
		if(fd_length(lenBuffer)>3){
		   ctiClient.setWriteMessage(lenBuffer);
		}
		return 0;
};

// ctiKawani.setAgentLogout[void]
ctiKawani.prototype.setAgentLogout = function(agentLogout, callback){ 
	if(fd_undef(agentLogout)){
		agentLogout = 0;
	}
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = agentLogout;
		fd.p3 = ctiClient.getClientSession('ctiAgentKey');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentExten');
		fd.p6 = ctiClient.getClientSession('ctiAgentName');
		fd.p7 = ctiClient.getClientSession('ctiAgentPass');
		fd.p8 = ctiClient.getClientSession('ctiAgentId');
		fd.p9 = 'KCA'; 
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p1),
			Event : fd_string(fd.p2),
			Extens : fd_string(fd.p5),
			Address : fd_string(fd.p4),
			AgentName : fd_string(fd.p6), 
			AgentId : fd_string(fd.p8),
			AgentService: fd_string(fd.p9)
	});
	// -- VALIDATION -- process sent message ""
	if(fd_length(lenBuffer)>3){
	   ctiClient.setWriteMessage(lenBuffer);	
	}
	
	//-- INFO -- if have callback function:
	if(fd_isfunc(callback)){
		callback.apply(this, new Array(ctiClient));
	}
	return 0;
};

// ctiKawani.setAgentLogin[void]
ctiKawani.prototype.setAgentLogin = function(){
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = 3;
		fd.p3 = ctiClient.getClientSession('ctiAgentKey');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentExten');
		fd.p6 = ctiClient.getClientSession('ctiAgentName');
		fd.p7 = ctiClient.getClientSession('ctiAgentPass');
		fd.p8 = ctiClient.getClientSession('ctiAgentId');
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p1),
			Event : fd_string(fd.p2),
			Extens : fd_string(fd.p5),
			Address : fd_string(fd.p4),
			AgentName : fd_string(fd.p6),
			AgentPwd : fd_string(fd.p7),
			//AgentKey : fd_string(fd.p3),
			AgentId : fd_string(fd.p8)
	});
	if(fd_length(lenBuffer)>3){ 
		//console.log(lenBuffer);
		ctiClient.setWriteMessage(lenBuffer);	
	}
	return 0;
};
// ctiKawani.setAgentReady[void]
ctiKawani.prototype.setAgentReady = function(){
	var fd = {};
		fd.p0 = 'AgentState';
		fd.p1 = 1;
		fd.p2 = ctiClient.getClientSession('ctiAgentKey');
		fd.p3 = ctiClient.getClientSession('ctiAgentHost');
		fd.p4 = ctiClient.getClientSession('ctiAgentExten');
		fd.p5 = ctiClient.getClientSession('ctiAgentName');
		fd.p6 = ctiClient.getClientSession('ctiAgentPass');
		fd.p7 = ctiClient.getClientSession('ctiAgentId');
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action	  : fd_string(fd.p0),
			Event	  : fd_string(fd.p1),
			Extens	  : fd_string(fd.p4),
			Address	  : fd_string(fd.p3),
			AgentName : fd_string(fd.p5),
			AgentId	  : fd_string(fd.p7)
	});
	if(fd_length(lenBuffer)>3){
		ctiClient.setWriteMessage(lenBuffer);	
	}
	return 0;
	 
};
// ctiKawani.setAgentNotReady[int]
ctiKawani.prototype.setAgentNotReady = function( ctiReasonId ){ 
	var fd = {};
		if(fd_undef(ctiReasonId)){
			ctiReasonId = 0;
		}
		
		// set all parameter :
		fd.p0 = 'AgentState';
		fd.p1 = 2;
		fd.p2 = ctiClient.getClientSession('ctiAgentKey');
		fd.p3 = ctiClient.getClientSession('ctiAgentHost');
		fd.p4 = ctiClient.getClientSession('ctiAgentExten');
		fd.p5 = ctiClient.getClientSession('ctiAgentName');
		fd.p6 = ctiClient.getClientSession('ctiAgentPass');
		fd.p7 = ctiClient.getClientSession('ctiAgentId');
		fd.p8 = ctiReasonId;
		
	var param = {
							  
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		ReasonId : fd_string(fd.p8),	
		Extens : fd_string(fd.p4),
		Address : fd_string(fd.p3),
		AgentName : fd_string(fd.p5),
		AgentId : fd_string(fd.p7)
	}; 
	 
	// put on localStorage:
	ctiClient.setLocalAgentStatus(fd.p7,{skill:0, status: fd.p1, reason:fd.p8, result: ''}); 
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	}); 
	return 0;
};

// ctiKawani.setAgentSpying[int]
ctiKawani.prototype.setAgentSpying = function(destination){ 
	if(fd_undef(destination)){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 1;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0; 
};

// ctiKawani.setAgentCoaching[int]
ctiKawani.prototype.setAgentCoaching = function(destination){ 
	if(fd_undef(destination)){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 2;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0; 
}; 

// ctiKawani.prototype.setAgentBarge([ destination:string ]):integer
ctiKawani.prototype.setAgentBarge = function(destination){
	if(fd_undef(destination)){ return 0; }
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 10;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0; 
} 

// ctiKawani.setAgentSplitCall[int]
ctiKawani.prototype.setAgentSplitCall = function(){
// then if add 
	var fd = {};
	fd.p0 = 'CallSplitIVR';
	fd.p1 = ctiClient.getCallSessionId();
	fd.p2 = ctiClient.getClientSession('ctiAgentExten'); 
	
	if(fd_length(fd.p1)<2){
		return 0;
	}
	// send data ok 
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
	
};

// ctiKawani.setAgentSkill[int]
ctiKawani.prototype.setAgentSkill = function(Skill){
	if(fd_undef(Skill)){
		Skill = 1;
	}
	// jika kosong :
	if( Skill == '' ){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 9;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = ctiClient.getClientSession('ctiAgentId');
		fd.p4 = Skill;
		
	var param = {
			Action 	: fd_string(fd.p0),
			Event 	: fd_string(fd.p1),
			Extens 	: fd_string(fd.p2),
			AgentId : fd_string(fd.p3),
			Skill	: fd_string(fd.p4)
		}
		
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			// console.log(lenBuffer);
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;	
}

// ctiKawani.setCallSurvey[int]::CurrentCall 
ctiKawani.prototype.setCallSurvey = function(mCallbacks){  
											 
	/** 
		Action : CallSurvey,
		CallSessionId : 1
		Destination : SIP/4001 
	 */
	var fd = {};
		fd.p0 = 'CallSurvey';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		
													  
  
					  
							
													   
	// then set object parameter ""
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage( lenBuffer );	
			// if function callbacks exist will return; 
			if( typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(ctiClient, lenBuffer));
				return 0;
			}
		}
	});	
	return 0;
};

// ctiKawani.setRetriveCall  = Ambil Call Yang di Parking Untuk di Transfer 
// Untuk di simpan Sementara Waktu yang Kemudian Akan di Transfer Kembali . 
ctiKawani.prototype.setRetriveCall = function(mCallbacks){
	var fd = {};
		fd.p0 = 'RetriveCall';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2) 
	}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){ 
			ctiClient.setWriteMessage(lenBuffer);	
			if( typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(lenBuffer));
				return 0;
			}
		}
	});	
	return 0;
}

// ctiKawani.setParkingCall  = Tujuannya Adalah call di simpan terlebih dahulu Kebucket USer 
// Untuk di simpan Sementara Waktu yang Kemudian Akan di Transfer Kembali . 
ctiKawani.prototype.setParkingCall = function(mCallbacks){
	var fd = {};
		fd.p0 = 'ParkingCall';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2) 
	}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
			if( typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(lenBuffer));
				return 0;
			}
		}
	});	
	return 0;
}

// ctiKawani.setCallTransfer =  Blind Transfer Call Tapa Melakukan Verifikasi terelebih 
// danulu ke No tujuan yang akan di Transfer  
ctiKawani.prototype.setCallForward = function(destination, callgroup){
	if(typeof(callgroup)!= 'undefined' &&(callgroup.length<4)) callgroup =  '3000'; // default Group;
	else if(typeof(callgroup) == 'undefined'){
	   callgroup = '3000';
	}
	
	// check phone number 
	if( fd_length(destination)<1){
		console.log('invalid extension');
		return 0;
	}
	
	var fd = {};
		fd.p0 = 'CallForward';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		fd.p4 = callgroup;
		
	var param = {
			Action		  : fd_string(fd.p0),
			CallSessionId : fd_string(fd.p1),
			Originating   : fd_string(fd.p2),
			Destination   : fd_string(fd.p3),
			CallGroup     : fd_string(fd.p4)   
		} 
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
}; 

// ctiKawani.setCallTransfer =  Blind Transfer Call Tapa Melakukan Verifikasi terelebih 
// danulu ke No tujuan yang akan di Transfer 
ctiKawani.prototype.setCallTransfer = function(destination, callgroup){
	if(typeof(callgroup)!= 'undefined' &&(callgroup.length<4)) callgroup =  '3000'; // default Group;
	else if(typeof(callgroup) == 'undefined'){
	   callgroup = '3000';
	}
	
	// check phone number 
	if( fd_length(destination)<1){
		console.log('invalid extension');
		return 0;
	}
	
	var fd = {};
		fd.p0 = 'CallTransfer';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		fd.p4 = callgroup;
		
	var param = {
			Action		  : fd_string(fd.p0),
			CallSessionId : fd_string(fd.p1),
			Originating   : fd_string(fd.p2),
			Destination   : fd_string(fd.p3),
			CallGroup     : fd_string(fd.p4)   
		}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			
			// console.log(lenBuffer);
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
}; 

// ctiKawani.setCallSplitToIVR([agentTarget:string, [custTarget:string, [assignId:string, [callback:interface ]]]]])
ctiKawani.prototype.setCallSplitToIVR = function(agentTarget, custTarget, assignId, mCallback){
	// check validation: agentTarget
	if(fd_length(agentTarget)<1){
	   writeloger('INFO: Invalid agentTarget');
	   return 0;
	}
	
	// check validation: custTarget
	if(fd_length(custTarget)<1){
	   writeloger('INFO: Invalid custTarget');
	   return 0;
	}
	
	// check validation: assignId
	if(fd_length(assignId)<1){
	   writeloger('INFO: Invalid assignId');
	   return 0;
	}
	
	// check data porposional 
	var callAction = 'CallSplitIVR',
		callSessionId = ctiClient.getCallSessionId(),
		callExten = ctiClient.getClientSession('ctiAgentExten');
		
	// check fd_session 	
	var fd = {};
		fd.p0 = callAction;
		fd.p1 = callSessionId;
		fd.p2 = callExten;
		fd.p3 = agentTarget;
		fd.p4 = custTarget;
		fd.p5 = assignId
		
	// check param 	
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Extension : fd_string(fd.p2),	
		AgentTarget : fd_string(fd.p3),
		CustTarget : fd_string(fd.p4),
		AssignId : fd_string(fd.p5)
	}

	// check cti client <process> :	
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		console.log(lenBuffer);
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
			if( typeof(mCallback) == 'function'){
				mCallback.apply(this, new Array(ctiClient, lenBuffer));
				return 0;
			}
		}
	});	
	return 0;
};							
// ctiKawani.setRegisterIP[void]
ctiKawani.prototype.setRegisterIP = function(Address, Extens, PbxId, Type){
	if(fd_length(Address)<1) return 0;
	// default PBX ID 
	if(fd_undef(PbxId)) 
		PbxId = window.sprintf('%s', '1');
	
	// default Type 
	if(fd_undef(Type)) 
		Type = window.sprintf('%s', '0');
	
	// then set data event:
	var Event = window.sprintf('%s', '3'),
		param = {
			Action : 'AstTool',
			Event : Event,
			Extens : Extens,
			PbxId : PbxId,
			Type : Type,
			Address : Address
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
};
// ctiKawani.setCallHangup[callback]:: tambahan untuk triger di sisi client 
// jika ada request callback : 
ctiKawani.prototype.setCallHangup = function(mCallbacks){
	var fd = {};
		fd.p0 = 'CallHangup';
		fd.p1 = ctiClient.getClientSession('ctiAgentExten');
		fd.p2 = ctiClient.getCallerSession('CallSessionId');
	var param = {
		Action : fd_string(fd.p0),
		Extens : fd_string(fd.p1),
		CallSessionId : fd_string(fd.p2) 
	};
	ctiClient.setComposeMessage(param, function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
			if(typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(ctiClient, lenBuffer));
			}
		}
	});
	return 0;
}; 

// ctiKawani.setCallDial[string, string]
ctiKawani.prototype.setCallDial = function(ctiCallerId, ctiAssignId, ctiContext, mCallbacks){ 
  var ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	  ctiAgentExten = ctiClient.getClientSession('ctiAgentExten');
		
  // -- VALIDATION : no have data to call be terminate. 
    writeloger(sprintf("INFO: call destination= %s [ID:%s]",ctiCallerId, ctiAssignId));
	if(fd_undef(ctiContext)){
	   ctiContext = 0; 
	}
	// untuk context dari function lama jika tidak di definisikan isi selalu menajdi == 0 
	if(fd_isfunc(ctiContext)){
	   ctiContext = 0; 
	}
	if(fd_undef(ctiCallerId)){
		return 0;
	}
   // -- VALIDATION : assign object data ID 
	if(fd_undef(ctiAssignId)){
		ctiAssignId = 0;
	}
   // -- VALIDATION : cannot be empty dialing number 
	if(fd_length(ctiCallerId)<3){
		return 0;
	}
	var fd = {};
		fd.p0 = 'MakeCall';
		fd.p1 = ctiCallerId;
		fd.p2 = ctiAssignId;
		fd.p3 = ctiProjectId;
		fd.p4 = ctiAgentExten;
		fd.p5 = ctiContext;
		
	var param ={
			Action : fd_string(fd.p0),		// Action name 
			Extens : fd_string(fd.p4),		// Local channel  
			Context : fd_string(fd.p5),		// Dialing Context 	
			CustomerId : fd_string(fd.p2),  // CustomerId / AssignId 
			CallerId: fd_string(fd.p1),		// CallerID show on Softphone , uniqueID destination  
			ProjectId: fd_string(fd.p3)		// Call Project Separated	
		};
		
	ctiClient.setComposeMessage(param, function(lenBuffer, ctiClient ){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
			// tambahan baru untuk handler sembarangan call[20191022]
			if( typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(ctiClient, lenBuffer));
			}
		}
	}); 
	return 0;
}; 

// ctiKawani.setCallSessionId(sessionid) return 0;
ctiKawani.prototype.setCallSessionId = function(sessionid){ 
   if(fd_undef(sessionid )) {
	   var sessionid = "";
   }
   // set callSessionId = "" empty;
   ctiClient.setCallSession('CallSessionId', sessionid);
   return 0;
}; 

// ctiKawani.prototype.setLocalAgentStatus([id:integer, [data:object]]) 
ctiKawani.prototype.setLocalAgentStatus = function(id,data){
 var c = {}; c.id = id; c.s = {}; c.s = ctiClient.getLocalAgentStatus(c.id); 
 if ( !fd_isobj(c.s) &&(fd_isobj(data))){
	c.s = (!fd_isobj(c.s) ? {} : c.s);
	for( var i in data){
	  if ( fd_undef(c.s[i])) c.s[i] = data[i];
	  else if ( !fd_undef(c.s[i]) &&(fd_length(data[i])>0 )){
		c.s[i] = data[i];
	  }
	}
 } 
 else if ( fd_isobj(c.s) &&( fd_isobj(data) )){  
  for ( var i in data ){
	if ( fd_undef(c.s[i])) c.s[i] = data[i];
	else if ( !fd_undef(c.s[i]) &&(fd_length(data[i])>0 )){
	  c.s[i] = data[i];
	}
  } 
 }
 // ubah data dan check apkah process ini termasuk Session Object :
 c.k = ( c.s === null ? '' : JSON.stringify(c.s));
 if ( fd_length(c.k)>0 &&( typeof(window.localStorage) == 'object' )){
	window.localStorage.setItem(c.id, c.k);
 }	 
 return 0;
} 

// ctiKawani.prototype.getLocalAgentStatus([id:integer]) 
ctiKawani.prototype.getLocalAgentStatus = function(id){
 var session = window.localStorage.getItem(id);
 if ( session === null ) return 0;
 else if ( fd_undef(session) ) return 0;
 else if ( !fd_undef(session) ) {
	var data = JSON.parse(session);
	if ( fd_isobj(data) ){
		return data;
	}
 }
 return 0; 
}

// ctiKawani.setCallDial[integer, integer] return (string)
ctiKawani.prototype.getCtiLabel = function(type, kode){
 return fd_string(fd_libs[type][kode]);
}
// ctiKawani.getAgentStatus[null] return (integer)
ctiKawani.prototype.getAgentStatus = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentStatus'));
};
// ctiKawani.getAgentStatus[null] return (integer)
ctiKawani.prototype.getAgentSkill = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentSkill'));
};
// ctiKawani.getCallStatus[null] return (integer)
ctiKawani.prototype.getCallStatus = function(){ 
 return fd_sigint(ctiClient.getCallerSession('CallStatus'));
};
// ctiKawani.getCallSessionId[null] return (string)
ctiKawani.prototype.getCallSessionId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallSessionId'));
};
// ctiKawani.getCallerId[null] return (string)
ctiKawani.prototype.getCallerId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallCallerId'));
};
// ctiKawani.getCallChannels[null] return (string)
ctiKawani.prototype.getCallChannels = function(){ 
 return fd_string(ctiClient.getCallerSession('CallChannel1'));
};
// ctiKawani.getCallDirection[null] return (string)
ctiKawani.prototype.getCallDirection = function(){ 
 return fd_string(ctiClient.getCallerSession('CallDirection'));	
};
// ctiKawani.getCallDirection[null] return (string)
ctiKawani.prototype.getCallIvrdata = function(index){ 
if( typeof(index) == 'undefined') { // secara default yang di ambil index pertama :
	index = 0;
}
 return fd_string(ctiClient.getIvrSession(index));	
}; 	
// ctiKawani.getCallIvrdata[null] return (string)
ctiKawani.prototype.getCallSplitIvrData = function(){ 
  return fd_string(ctiClient.getCallerSession('ctiCallSplitIVRData'));	
};	 
// ctiKawani.getCallIvrdata[null] return (string)
ctiKawani.prototype.getCallAssignId = function(){ 
  return fd_string(ctiClient.getCallerSession('CallAssignId'));	
};	 
// ctiKawani.getCtiMediaData[integer, integer] return (string)
ctiKawani.prototype.getMediaType = function(type){
 return fd_Media[type];
} 

/** 
 * https://ccm.net/forum/affich-57878-firefox-connection-interrupted
 * Type [firefox] About:config in the address bar.
 * search ipv6
 * make it FALSE If TRUE.
 * 
 */	 
 try{
	$(window).on('beforeunload', function(){
		if( typeof(ctiClient) != 'undefined' ){
			if(fd_isobj(ctiClient) && fd_isfunc(ctiClient.setWebSocketDisconect)){
				ctiClient.setWebSocketDisconect();
				return undefined;
			}
		}
	});
 }
catch(err){
	console.log(err)
}