/** 
 * ============================================================================== 
 * $.kawani.websocket.js v 4.5.4 <REVISION PREDITIVE MODUL>
 * ==============================================================================
 * 
 * Copyright (c) 2019  omens[jombi_par@yahoo.com]
 *  
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
var AST_AGENT_LOGOUT = 0;
var AST_AGENT_READY = 1;
var AST_AGENT_NOT_READY = 2;
var AST_AGENT_LOGIN = 3;
var AST_AGENT_BUSY = 4;
var AST_AGENT_ACW = 5;
var AST_AGENT_RESERVED = 6;
var AST_AGENT_TALKING = 7;
var AST_AGENT_REJECTED = 9;
var AST_AGENT_REGISTER = 10;
var AST_AGENT_CALLASSIGN = 11;
var AST_AGENT_IDLE = 25;
// MASTER REF
window.WebSocketInterval = 0;
var fd_libs = {
	
  // INBOUND:
	1 : {
		999:'Call Incoming from ',  
		100:'Call Ringing from', 
		101:'Call Incoming with',  
		102:'Call Incoming from',  
		201:'Call Incoming from',  
		202:'Call Connected with',  
		300:'Call Ended Session', 
		301:'Call Ignore with',
		302:'Call Congestion with',
		303:'Call Rejected with'
	},
// OUTBOUND:	
   2 : {
		999:'Call Iniate', 
		100:'Call Offered', 
		101:'Call Originating', 
		102:'Call Trunksize', 
		201:'Call Dialing', 
		202:'Call Connected', 		 
		300:'Call Answered', 
		301:'Call Busy',
		302:'No User Response',
		303:'No Answered',
		304:'Call Rejected',
		305:'Change Number',
		306:'Destination Out Of Order',
		307:'Invalid Number Format',
		308:'Facility Rejected',
		309:'Response To Status Enquiry',
		311:'Normal Unspecified',
		312:'Normal Circuit Congestion',
		313:'Network Out Of Order',
		314:'Chan Not Implemented'
	},
	// PREDICTIVE:
	3 : {
		999:'Predictive from ',  
		100:'Predictive Ringing', 
		101:'Predictive with',  
		102:'Predictive from',  
		201:'Predictive from',  
		202:'Predictive Connected',  
		300:'Predictive Disconect', 
		301:'Predictive Ignore',
		302:'Predictive Congestion',
		303:'Predictive Rejected'
	}
};

if( typeof(window.sprintf) != 'function' ){   
window.sprintf = function( format ) {
  for( var i=1; i < arguments.length; i++ ) {
	format = format.replace( /%s/, arguments[i] );
  }
  return format;
};
}
if( typeof(window.strcmp) != 'function' ){  
	window.strcmp = function( key, val ) {
		if( key!== null && key.localeCompare(val) == 0 ){
			return true;
		} 
		return false;
	};
}
if( typeof(window.fd_isfunc) != 'function' ){
function fd_isfunc(data){
	if( typeof(data) == 'function' ){
		return true;
	}
	return false;
 } 
}
if( typeof(window.fd_isobj) != 'function' ){
function fd_isobj(data){
	if( typeof(data) == 'object' ){
		return true;
	}
	return false;
 } 
}
if( typeof(window.fd_undef) != 'function' ){
function fd_undef(data){
	if( typeof(data) == 'undefined' ){
		return true;
	}
	return false;
 } 
}
if( typeof(window.fd_sigint) != 'function' ){
function fd_sigint(data){
	return parseInt(data);
 } 
}
if( typeof(window.fd_string) != 'function' ){
function fd_string(data){
	if( fd_undef(data) ){
		return "";
	}
	return data.toString();
 } 
}
if( typeof(window.fd_length) != 'function' ){
function fd_length(data){
	return fd_string(data).length;
 } 
}

if( typeof(window.fd_msg) != 'function' ){
function fd_msg(data){
	window.alert(data);
 } 
}
if( typeof(window.fd_unixtime) != 'function' ){
function fd_unixtime(time){
	var time_to_show = fd_sigint(time); // unix timestamp in seconds
	var t = new Date(time_to_show * 1000);
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
}
}
if( typeof(window.timestamp) != 'function' ){
function timestamp(){
	var t = new Date();
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
}
}

// helper 
if( typeof(window.writeloger) != 'function' ){
function writeloger(message){
	var verbose='', logWrite = sprintf('%s WS >> %s',timestamp(), message);
	if(fd_isobj(message)){
		verbose = sprintf('VERBOSE:%s\n','WS');
		console.log(verbose);
		console.log(message);
		console.log('\n');
	}
	// not object:
	else if(!fd_isobj(message) &&(fd_length(logWrite)>0)){
		console.log(logWrite);
	}
	return;
 } 
}
// MASTER ATTRIBUTE :
// window.ctiKawani['onready', 'onConnect', 'onThread', 'onClose']		
function ctiKawani( p1, p2, p3, p4, p5){ 
	this.onType 	= 'ws';
	this.onSocket 	= null;
	this.onHost 	= null;
	this.onPort 	= null;
	this.onReady 	= p1;
	this.onConnect 	= p2;
	this.onThread 	= p3;
	this.onClose 	= p4; 
	this.onError 	= p5; 
	this.onUrl		= null;
	this.onReject	= 0;
	return this;
};
// ctiKawani.setConfigServer[object] 
ctiKawani.prototype.setConfigServer = function(param){
	if(fd_isobj(param)){
		this.session = param;
		if(fd_undef(this.session.caller)){
			this.session.caller = {};
		}
	}
};
// ctiKawani.setConnectServer[boolean, object] 
ctiKawani.prototype.setConnectServer = function(sockStatus, server){
	if(sockStatus > 0){
		var p = ctiClient.session.label.serverState;
		if( fd_isobj(p)){
			writeloger('INFO: Connecting ...');
			p.html('Connecting ...');
		}
		ctiClient.setAgentRegister();
	}
};
// ctiKawani.getClientSession[string] 
ctiKawani.prototype.getClientSession = function(key){
	var param = this.session.client,
		result = '';
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};
// ctiKawani.getCallerSession[string] 
ctiKawani.prototype.getCallerSession = function(key){
	var param = this.session.caller,
		result = "";
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};
// ctiKawani.setParseMessage[string] 
ctiKawani.prototype.setParseMessage = function( message ){
	var param = message.split('|'),
		buffer = {};
	for( i in param  ){
		var ptr = param[i].split(':');
		if(fd_isobj(ptr)){
			buffer[ptr[0]] = ptr[1];
		}
	}	
	var tParam = {
		get : function( key ){
			if(!fd_undef(buffer[key])){
				return buffer[key];
			}
			return "";
		},
		fetch : function(){
			return (fd_isobj(buffer) ? buffer : false);
		}
	}
	return tParam;
};
// ctiKawani.setThreadCallState[object] 
ctiKawani.prototype.setThreadCallState = function(param){
	if(!fd_isobj(param)){
		return 0;
	}
	// typeof inbound 	
	var fd = {};
		fd.p1 = param.CallDirection;
		fd.p2 = param.CallStatus;
		fd.p4 = param.CallerID;
		fd.p7 = ctiClient.session.label.callState;
		//console.log(fd.p7);
	
	// inbound Call 
	if(fd_isobj(param) && fd.p1==1){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// outbound call 
	else if(fd_isobj(param) && fd.p1==2){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}	
	// Predictve Call
	else if(fd_isobj(param) && fd.p1==3){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// end md:
	return 0;
};
// ctiKawani.setThreadPeers[object] 
ctiKawani.prototype.setThreadPeers = function(param){
	if(!fd_isobj(param)){
		return 0;
	}
	// -- WARNING INFO :
	var fd = param.fetch();
		fd.p0 = ctiClient.session.label.extenState;
		
	//console.log(fd);
	// SIP:UNREGISTERED
	if(strcmp(fd.ReasonId,'101')){	
		if(!fd_isobj(fd.p0)){
			fd_msg(sprintf('-- WARNING --\nExtension: SIP/%s Cannot Be Used. Because : %s',fd.Extens, fd.ReasonVal));
			return 0;
		} 
		// then if object data :
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		return 0;
	}
	// SIP:UKNOWN REASON
	else if(strcmp(fd.ReasonId,'102')){
		if(!fd_isobj(fd.p0)){
			fd_msg(sprintf('-- WARNING --\nExtension: SIP/%s Cannot Be Used. Because : %s',fd.Extens, fd.ReasonVal));
			return 0;
		}
		// then if object data :
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		return 0;
	}
	// SIP:REGISTER 
	else if(strcmp(fd.ReasonId,'103')){
		if(!fd_isobj(fd.p0)){
			fd_msg(sprintf('-- WARNING --\nExtension: SIP/%s Ok Because: %s',fd.Extens, fd.ReasonVal));
			return 0;
		}
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		return 0;
	}
	// SIP:REACHABLE
	else if(strcmp(fd.ReasonId,'104')){
		if(!fd_isobj(fd.p0)){
			fd_msg(sprintf('-- WARNING --\nExtension: SIP/%s Ok Because: %s',fd.Extens, fd.ReasonVal));
			return 0;
		}
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		return 0;
	}
	// SIP:RESERVED
	else if(strcmp(fd.ReasonId,105)){
		if(!fd_isobj(fd.p0)){
			fd_msg(sprintf('-- WARNING --\nExtension: SIP/%s Ok Because: %s',fd.Extens, fd.ReasonVal));
			return 0;
		}
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		return 0;
	}
	return 0;
};
// ctiKawani.setThreadLogin[object] 
ctiKawani.prototype.setThreadLogin = function(fd){ 
	if(!fd_isobj(fd)){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will__data 
	if(fd_length(fd.ReasonVal)<0){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will set _key 
	if(fd.ReasonVal =='success'){
		ctiClient.setClientSession('ctiAgentKey', fd.AgentKey);
		window.setTimeout(function(){
			ctiClient.setAgentReady();
		}, 1000);
		return 0;
	}
	else {
		// console.log(fd);
		fd_msg('-- WARNING -- Invalid Login');
		var p0 = ctiClient.session.label.agentState
		if( fd_isobj(p0)){
			p0.html(fd.Message);
		}
		return 0;
	}
	return 0;
};
// ctiKawani.setThreadRegister[object] 
ctiKawani.prototype.setThreadRegister = function(param){ 
	var fd = {}
		fd.p0 = param.ReasonVal;
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.extenState;
		if(fd_length(fd.p0)<1) return 0;
		// -- INFO -- Success next login :
		if(fd.p0 == 'success' ){
			window.setTimeout(function(){
				ctiClient.setAgentLogin();
			}, 1000);
			return 0;
		}
		else {
			// then  console.log(param);
			writeloger(sprintf('WARNING: Invalid Register host=%s exts= %s used by other user.',param.Address, param.Extens));
			if(fd_isobj(fd.p1)){
			   fd.p1.html('Register');
			}
			if(fd_isobj(fd.p2)) {
			  fd.p2.html('-- INFO: Register rejected');
			}
			ctiClient.onReject++;
			return 0;
		}
		return 0;
};
// ctiKawani.setThreadState[object] 
ctiKawani.prototype.setThreadState = function(param){ 
	var tParam = param.fetch(),
		tTrack = tParam.Value;
		
		//$('#cmdAgentStatus').html("HELLO");
	//	alert('hello');
	var fd = {};
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.callState; 
		fd.p3 = ctiClient.session.label.serverState; 
		
	//	console.log(fd);
	// on track agents:	
	if(fd_sigint(tTrack) == AST_AGENT_LOGOUT){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(fd_isobj(fd.p1)){
		   fd.p1.html('logout');
		   fd.p3.html('');
		}
		if(fd_isobj(fd.p2)){
		   fd.p2.html('');
		}
		return 0;
	}
	
	// on track agents:	
	if(fd_sigint(tTrack) == AST_AGENT_REGISTER){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(fd_isobj(fd.p1)){
		   fd.p1.html('Register');
		}
		if(fd_isobj(fd.p2)){
		   fd.p2.html('idle');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_LOGIN){
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.text('Login');
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_READY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.html('Ready');
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		} 
	}
	else if(fd_sigint(tTrack) == AST_AGENT_NOT_READY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		var p1 = tParam.ReasonVal, p2 = 'Not Ready';
		if(fd_length(p1)> 2){
			p2 = sprintf('%s ( %s )', p2, p1);
		}
		if(fd_isobj(fd.p1)){
			fd.p1.html(p2);
		}
		if(fd_isobj(fd.p2)){
			fd.p2.html('idle');
		} 
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_BUSY){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.html('Busy');
		}
		return 0;
	}
	else if(fd_sigint(tTrack) == AST_AGENT_ACW){
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if(fd_isobj(fd.p1)){
			fd.p1.html('Acw');
		}
		return 0;
	}
	return 0;
};
// ctiKawani.setCallSession[string, string] 
ctiKawani.prototype.setCallSession = function(key, val){ 
	ctiClient.session.caller[key] = val; 
	return 0;
};
// ctiKawani.setClientSession[string, string] 
ctiKawani.prototype.setClientSession = function(key, val){ 
	ctiClient.session.client[key] = val; 
	return 0;
};
// ctiKawani.setThreadAlarm[object]
ctiKawani.prototype.setThreadAlarm = function(p){ 
 var fds = p.fetch(); 
 if((fds.Action == 'Alarm')){
	 var fd_state = ctiClient.session.label.extenState;
	 if(fd_isobj(fd_state)){
		var error = sprintf("* PBX : %s -- INFO: %s",fds.ServerHost, fds.ServerError);
		fd_state.html(fd_string(error));
	}
 }
 return 0;
}
// ctiKawani.setThreadVoiceMail[object]
ctiKawani.prototype.setThreadVoiceMail = function(p){ 
	var fds = p.fetch(); 
	if(fd_isobj(fds) &&(!fd_undef(fds.VMCallerId))){
		var msgVoiceMail = sprintf('You have Voice Mail Message from = %s\nWith MessageId [%s]', fds.VMCallerId, fds.VMMessageId);
		if(fd_length(msgVoiceMail)>0){
			console.log(msgVoiceMail);
		}
		return 0;
	}
}

// ctiKawani.setThreadPing[object]
ctiKawani.prototype.setThreadPing = function(p){ 
	var fds = p.fetch(); 
	if(fd_undef(fds.clientId)){
		var timeStamp = fd_string(fd_unixtime(fds.SockTime)),
			response = fd_string(fds.Response),
			disposition = fd_string(fds.Disposition),
			sockid  = fd_string(fds.SockId),
			socktype = fd_string(fds.SockType),
			sockpid = fd_string(fds.SockPid),
			sockport = fd_string(fds.SockPort),
			sockhost = fd_string(fds.SockHost);
			
		console.log(sprintf('%s WS >> Got MSG INFO = %s #PID = %s #MSG = %s',timeStamp, disposition, sockpid, response));
		console.log(sprintf('%s WS >> Type= %s #Host = %s #Id= %s #Port= %s',timeStamp, socktype, sockhost, sockid, sockport));
		return 0;
	}

	if(fd_isobj(fds)&&fd_sigint(fds.clientId)>0){
		var timeStamp = fd_unixtime(fds.clientTime);
		if(typeof(window.interfaceIdleTimer) == 'function' ){
			window.interfaceIdleTimer.apply(this, new Array(ctiClient));	
		}
		// then me ... 
		console.log(sprintf('%s WS >> Got MSG INFO = %s',timeStamp, fds.EventGetVar));
		console.log(sprintf('%s WS >> Type= %s #Host = %s #Id= %s #Port= %s ', timeStamp, fds.clientType, fds.clientHost, fds.clientId, fds.clientPort ));
		if(fd_isfunc(ctiClient.setWebSocketPing)){
			ctiClient.setWebSocketPing();
		}
		var fd = {};
			fd.p1 = ctiClient.session.label.serverState;
			if(fd_isobj(fd.p1)){
				fd.p1.html('Now Connected.');
			}
	}  
	return 0;
};
// ctiKawani.setIntefaceThread[object{[type: string ] [type: int ] [data: object ] }]
ctiKawani.prototype.setIntefaceThread = function(data){ 
	var fd = {};
		fd.p0 = ctiClient.session['interface'];
	if(!fd_undef(fd.p0)&&fd_length(fd.p0)>3){
		window[fd.p0].apply(this, new Array(data));
		return;
	}
	return;
};
// ctiKawani.setThreadPing[object]
ctiKawani.prototype.setThreadCall = function(param){ 
	var p = param.fetch();
	if(!fd_isobj(p)){
		return 0;
	}
	// add to list data local browser :
	if(fd_isobj(p)&&fd_length(p.CallerID) > 3 ){
		ctiClient.setCallSession('CallCallerId', p.CallerID);
	}
	if(fd_isobj(p)&&fd_length(p.CallDirection)>0){
		ctiClient.setCallSession('CallDirection', p.CallDirection);
	}
	if(fd_isobj(p)&&fd_length(p.CallCalling)>0){
		ctiClient.setCallSession('CallCalling', p.CallCaller);
	}
	if(fd_isobj(p)&&fd_length(p.CallChannel)>0){
		ctiClient.setCallSession('CallChannel1', p.CallChannel);
	}
	if(fd_isobj(p)&&fd_length(p.CallChannel2)>0){
		ctiClient.setCallSession('CallChannel2', p.CallChannel2);
	}
	if(fd_isobj(p)&&fd_length(p.CallCreateId)>0){
		ctiClient.setCallSession('CallCreateId', p.CallCreateId);
	}
	if(fd_isobj(p)&&fd_length(p.CallSessionId)>3){
		ctiClient.setCallSession('CallSessionId', p.CallSessionId);
	}
	if(fd_isobj(p)&&fd_length(p.CallStatus)>3){
		ctiClient.setCallSession('CallStatus', p.CallStatus);
	}
	if(fd_isobj(p)&&fd_length(p.CallTrunk)>3){
		ctiClient.setCallSession('CallTrunk', p.CallTrunk);
	}
	if(fd_isobj(p)&&fd_length(p.CallVDN)>3){
		ctiClient.setCallSession('CallVDN', p.CallVDN);
	} 
	
	// -- INFO -- user deleagate function to aksess 
	// from public method :
	if(fd_isfunc(ctiClient.setIntefaceThread)){
	   ctiClient.setIntefaceThread({
		   event : fd_string('callstate'),
		   body  : {
			   type : fd_sigint(p.CallDirection),
			   data : p 
		   }
		});
	} 
	return 0;
};
// ctiKawani.setThreadAgent[object]
ctiKawani.prototype.setThreadAgent = function(param){ 
	var tParam = param.fetch();
	//console.log(tParam);
	// -- OPEN PUBLIC -- thread method :
	if(fd_isfunc(ctiClient.setIntefaceThread)){
		if(fd_isobj(tParam)&&fd_length(tParam.Extens)>2){
			ctiClient.setIntefaceThread({
				   event : fd_string('agentstate'),
				   body : {
					   type : fd_sigint(tParam.Value),
					   data : tParam 
				   }
			});
		}
	}
	
	if(fd_isobj(tParam)&&fd_length(tParam.Extens)>2){
		var tTrack = tParam.Value
		// repsonse on agent register :
		if(tTrack == AST_AGENT_LOGOUT 
		&& (fd_isfunc(ctiClient.setAgentDisconected))){
			return 0;
		}
		// repsonse on agent register :
		if(tTrack == AST_AGENT_REGISTER 
		&& (fd_isfunc(ctiClient.setAgentLogin))){
			if(fd_isfunc(ctiClient.setThreadRegister)){
				ctiClient.setThreadRegister.apply(this, new Array(tParam));
			}
			return 0;
		}
		// user login 
		else if(tTrack == AST_AGENT_LOGIN
		&& (fd_isfunc(ctiClient.setAgentReady))){
			if(fd_isfunc(ctiClient.setThreadLogin)){
				ctiClient.setThreadLogin.apply(this, new Array(tParam));
			}
			return 0;
		}
		// agent Ready:
		else if(tTrack == AST_AGENT_READY
		&& (fd_isfunc(ctiClient.setAgentNotReady))){
			return 0;
		}
		// not ready 
		else if(tTrack == AST_AGENT_NOT_READY 
		&& (fd_isfunc(ctiClient.setAgentNotReady))){
			return 0;
		}
		// Busy  
		else if(tTrack == AST_AGENT_BUSY 
		&& (fd_isfunc(ctiClient.setAgentNotReady))){
			return 0;
		}
		// Acw  
		else if(tTrack == AST_AGENT_ACW 
		&& (fd_isfunc(ctiClient.setAgentLogin))){
			return 0;
		}
	}
	return 0;
};
// ctiKawani.setThreadServer[string]
ctiKawani.prototype.setThreadServer = function(message){
	var p = ctiClient.setParseMessage(message);
	
	if(!fd_isobj(p)){
		return 0;
	}
	// get data 
	var events = p.get('EventGetVar'),
		param = p.fetch();
		
	//console.log(param);	
	if(events == 'ChatHearbeat'){
		if(fd_isfunc(ctiClient.setThreadPing)){
			ctiClient.setThreadPing.apply(this, new Array(p));
		}
	}
	
	// Thread baru Untuk VoiceMail:
	if(events == 'VoiceMail'){
		if(fd_isfunc(ctiClient.setThreadVoiceMail)){
			ctiClient.setThreadVoiceMail.apply(this, new Array(p));
		}
	}
	// track get informations 
	else if(events == 'Info'){
		// setThreadPing
		if(!fd_undef(param.Disposition) &&(param.Disposition=='Chathearbeat')){
			if(fd_isfunc(ctiClient.setThreadPing)){
				ctiClient.setThreadPing.apply(this, new Array(p));
			}
		}
		// setThreadAlarm
		else if(!fd_undef(param.Disposition) &&(param.Disposition=='Alarm')){
			if(fd_isfunc(ctiClient.setThreadAlarm)){
				ctiClient.setThreadAlarm.apply(this, new Array(p));
			}
		}
		
	}
	// track on agent_state 
	else if(events == 'AgentState'){
		// console.log(message);
		if(fd_isfunc(ctiClient.setThreadAgent)){
			ctiClient.setThreadState.apply(this, new Array(p));
			ctiClient.setThreadAgent.apply(this, new Array(p));
		}
	} 
	// track on call_state 
	else if(events == 'CallState'){
		if(fd_isfunc(ctiClient.setThreadCall)){
			ctiClient.setThreadCallState.apply(this, new Array(param));
			ctiClient.setThreadCall.apply(this, new Array(p));
		}
	}
	// track PeerStatus 
	else if(events == 'PeerStatus'){
		// console.log(message);
		if(fd_isfunc(ctiClient.setThreadPeers)){
			ctiClient.setThreadPeers.apply(this, new Array(p));
		}
	}
	return 0;
};
// ctiKawani.setComposeMessage[string]
ctiKawani.prototype.setComposeMessage = function(fd, callback){
	var ptr = "", buffer = "";
	if(!fd_isobj(fd)){
		return 0;
	} 
	for( var i in fd ){
		ptr+= sprintf("%s:%s\r\n", i, fd[i]); 
	}
	buffer = fd_string(ptr);
	// -- INFO-- if user not deleagate function / callback 
	// will back return data 	
	if(!fd_isfunc(callback)){
		if(fd_length(buffer) == 0 ){
			return 0;
		}
		return buffer;
	}
	// if user deleagatefunction callback 
	else if(fd_isfunc(callback)){
		callback.apply(this, new Array(buffer, ctiClient));
		return 0;
	}
	return;
};
// ctiKawani.setWriteMessage[string]
ctiKawani.prototype.setWriteMessage = function(message){
	if( fd_isobj(window.ctiClient.onSocket)){
		window.ctiClient.onSocket.send(message);
	}
	return 0;
};
// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketConnect = function(){
	this.onType  = this.session.server.type;
	this.onHost  = this.session.server.host;
	this.onPort  = this.session.server.port;
	this.onInfo  = this.session.label.serverState;
	this.onCall  = this.session.label.callState;
	this.onAgent = this.session.label.agentState;
	this.onExten = this.session.label.extenState;
	// set to URL :
	this.onUrl = window.sprintf('%s://%s:%s', this.onType, this.onHost, this.onPort);
	console.log(this.onUrl);
	if(typeof(this.onUrl) != 'string'){
		return 0;
	}
	// open connection to webSocket server 
	try{
		if(typeof(MozWebSocket)=='function'){ 
			this.onSocket = new MozWebSocket(this.onUrl);
		}
		else {
			this.onSocket = new WebSocket(this.onUrl);
		} 
		var websock = this;
		// interface[ctiClient.setConnectServer]
		this.onSocket.onopen = function(evt){ 
			//console.log(evt);
			writeloger('INFO: Initialize open ws:socket');
			if(fd_isobj(evt)&&fd_isfunc(ctiClient.setConnectServer)){
				if(fd_isfunc(window.clearTimeout)){
					window.clearTimeout(window.WebSocketInterval);
					websock.onReady = true;
				}
				// then back connect:
				ctiClient.setConnectServer.apply(this, new Array(1, evt.originalTarget));
				return 0;
			}	
		}
		// interface[ctiClient.setThreadServer]
		this.onSocket.onmessage = function(evt){
			writeloger(sprintf('INFO: Initialize message ws:socket'));
			if(fd_isfunc(ctiClient.setThreadServer)){
				ctiClient.setThreadServer.apply(this, new Array(evt.data));
				return 0;
			}	
		}
		// -- EVENT : onclose
		this.onSocket.onclose = function(evt){ 
			writeloger('INFO: Initialize close ws:socket');
			
			if((websock.onReject>0)) return 0;
			else if((websock.onReject<1)){
				var p1 = websock.onInfo,
					p2 = websock.onCall,
					p3 = websock.onAgent;
				if(fd_isobj(p1)){
					writeloger('INFO: Can\'t establish a Connection to the CTI');
					p1.html('-- INFO -- Can\'t establish a Connection to the CTI ');
				}
				if(fd_isobj(p2)){
					writeloger('INFO: Can\'t establish a Connection to the CTI (10) s');
					p2.html('-- INFO -- Can\'t establish a Connection to the CTI (10) s');
				}
				if(fd_isobj(p3)){
					writeloger('WARNING: CTI Disconected');
					p3.html('-- WARNING -- CTI Disconected.');
				}
				// try connect to the server .
					window.WebSocketInterval = window.setTimeout(function(){
						writeloger('INFO: Wait timout 10s to reconnecting ...');
						websock.onReady = false;
						ctiClient.setWebSocketReconnect(websock);
					},10000); // 10 seconds
				return 0;
			}
			
		}
		// -- EVENT : onerror
		this.onSocket.onerror = function(evt){
			writeloger('INFO: Initialize error ws:socket');
			var p0 = websock.onInfo; if(fd_isobj(p0) ){
				p0.html('CTI Disconected');
			}
		}
	}
	// -- EVENT -- error
	catch(wsError){
		//console.log(wsError);
		return 0
	}
	return 0;
};
// ctiKawani.setWriteMessage[string]
ctiKawani.prototype.setWebSocketReconnect = function(webSocket){
	// -- INFO : clean session Key if disconected with server  
	ctiClient.setClientSession('ctiAgentKey', '');
	if(fd_isobj(webSocket.onInfo)){
		writeloger('INFO: ReConnecting to the CTI ...');
		webSocket.onInfo.html('-- INFO -- Re Connecting to the CTI...');
	}
	if(fd_isobj(webSocket.onAgent)){
		writeloger('INFO: CTI Disconected.');
		webSocket.onAgent.html('-- WARNING -- CTI Disconected');
	}
	if(fd_isobj(webSocket.onCall)){
		writeloger('NOTICE: ReConnect to CTI...');
		webSocket.onCall.html('-- INFO -- Re Connect...');
	}	
	// detect socket :
	if(fd_isobj(ctiClient)&&fd_isfunc(ctiClient.setWebSocketConnect) ){
		ctiClient.setWebSocketConnect();
	}
	return 0;	
};
// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketDisconect = function(){
	if(fd_isobj(window.ctiClient.onSocket)){
		ctiClient.onSocket.close();
	}
	return 0;
};

// ctiKawani.setClientPing[string, string]
ctiKawani.prototype.setWebSocketPing = function(){ 
	var 
	  ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	  ctiAgentExten = ctiClient.getClientSession('ctiAgentExten'),
	  ctiAgentHost  = ctiClient.getClientSession('ctiAgentHost'),
	  ctiAgentEvent = 4;
	  
   // -- VALIDATION : no have data to call be terminate. 
	if(fd_undef(ctiAgentExten)){
		return 0;
	}
  
   // -- VALIDATION : cannot be empty dialing number 
	if(fd_length(ctiAgentHost)<3){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = ctiAgentEvent;
		fd.p2 = ctiAgentExten;
		fd.p3 = ctiAgentHost;
		
	var param ={
			Action : fd_string(fd.p0),
			Event : fd_string(fd.p1),
			Extens : fd_string(fd.p2),
			Address: fd_string(fd.p3) 
		};
	ctiClient.setComposeMessage(param, function( lenBuffer, ctiClient ){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	}); 
	return 0;
}
// ctiKawani.setAgentDisconected[void]
ctiKawani.prototype.setAgentDisconected = function(){ 
  ctiClient.setWriteMessage('quit');
  return 0;
};


// ctiKawani.setAgentRegister[void]
ctiKawani.prototype.setAgentRegister = function(){
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = 10;
		fd.p3 = ctiClient.getClientSession('ctiAgentExten');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentId');
		
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action 	: fd_string(fd.p1),
			Event 	: fd_string(fd.p2),
			Extens  : fd_string(fd.p3),
			AgentId : fd_string(fd.p5),
			Address : fd_string(fd.p4)
		});
	
		if(fd_length(lenBuffer)>3){
		   ctiClient.setWriteMessage(lenBuffer);
		}
		return 0;
};
// ctiKawani.setAgentLogout[void]
ctiKawani.prototype.setAgentLogout = function(agentLogout, callback){ 
	if(fd_undef(agentLogout)){
		agentLogout = 0;
	}
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = agentLogout;
		fd.p3 = ctiClient.getClientSession('ctiAgentKey');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentExten');
		fd.p6 = ctiClient.getClientSession('ctiAgentName');
		fd.p7 = ctiClient.getClientSession('ctiAgentPass');
		fd.p8 = ctiClient.getClientSession('ctiAgentId');
		fd.p9 = 'KCA'; 
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p1),
			Event : fd_string(fd.p2),
			Extens : fd_string(fd.p5),
			Address : fd_string(fd.p4),
			AgentName : fd_string(fd.p6), 
			AgentId : fd_string(fd.p8),
			AgentService: fd_string(fd.p9)
	});
	// -- VALIDATION -- process sent message ""
	if(fd_length(lenBuffer)>3){
	   ctiClient.setWriteMessage(lenBuffer);	
	}
	
	//-- INFO -- if have callback function:
	if(fd_isfunc(callback)){
		callback.apply(this, new Array(ctiClient));
	}
	return 0;
};
// ctiKawani.setAgentLogin[void]
ctiKawani.prototype.setAgentLogin = function(){
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = 3;
		fd.p3 = ctiClient.getClientSession('ctiAgentKey');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentExten');
		fd.p6 = ctiClient.getClientSession('ctiAgentName');
		fd.p7 = ctiClient.getClientSession('ctiAgentPass');
		fd.p8 = ctiClient.getClientSession('ctiAgentId');
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p1),
			Event : fd_string(fd.p2),
			Extens : fd_string(fd.p5),
			Address : fd_string(fd.p4),
			AgentName : fd_string(fd.p6),
			AgentPwd : fd_string(fd.p7),
			//AgentKey : fd_string(fd.p3),
			AgentId : fd_string(fd.p8)
	});
	if(fd_length(lenBuffer)>3){ 
		//console.log(lenBuffer);
		ctiClient.setWriteMessage(lenBuffer);	
	}
	return 0;
};
// ctiKawani.setAgentReady[void]
ctiKawani.prototype.setAgentReady = function(){
	var fd = {};
		fd.p0 = 'AgentState';
		fd.p1 = 1;
		fd.p2 = ctiClient.getClientSession('ctiAgentKey');
		fd.p3 = ctiClient.getClientSession('ctiAgentHost');
		fd.p4 = ctiClient.getClientSession('ctiAgentExten');
		fd.p5 = ctiClient.getClientSession('ctiAgentName');
		fd.p6 = ctiClient.getClientSession('ctiAgentPass');
		fd.p7 = ctiClient.getClientSession('ctiAgentId');
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action	  : fd_string(fd.p0),
			Event	  : fd_string(fd.p1),
			Extens	  : fd_string(fd.p4),
			Address	  : fd_string(fd.p3),
			AgentName : fd_string(fd.p5),
			AgentId	  : fd_string(fd.p7)
	});
	if(fd_length(lenBuffer)>3){
		ctiClient.setWriteMessage(lenBuffer);	
	}
	return 0;
	 
};
// ctiKawani.setAgentNotReady[int]
ctiKawani.prototype.setAgentNotReady = function( ctiReasonId ){
	var fd = {};
		if(fd_undef(ctiReasonId)){
			ctiReasonId = 0;
		}
		
		// set all parameter :
		fd.p0 = 'AgentState';
		fd.p1 = 2;
		fd.p2 = ctiClient.getClientSession('ctiAgentKey');
		fd.p3 = ctiClient.getClientSession('ctiAgentHost');
		fd.p4 = ctiClient.getClientSession('ctiAgentExten');
		fd.p5 = ctiClient.getClientSession('ctiAgentName');
		fd.p6 = ctiClient.getClientSession('ctiAgentPass');
		fd.p7 = ctiClient.getClientSession('ctiAgentId');
		fd.p8 = ctiReasonId;
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p0),
			Event : fd_string(fd.p1),
			ReasonId : fd_string(fd.p8),	
			Extens : fd_string(fd.p4),
			Address : fd_string(fd.p3),
			AgentName : fd_string(fd.p5),
			AgentId : fd_string(fd.p7)
	});
	if(fd_length(lenBuffer)>3){
		ctiClient.setWriteMessage(lenBuffer);	
	}
	return 0;
};
// ctiKawani.setAgentSpying[int]
ctiKawani.prototype.setAgentSpying = function(destination){ 
	if(fd_undef(destination)){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 1;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0; 
};

// ctiKawani.setAgentCoaching[int]
ctiKawani.prototype.setAgentCoaching = function(destination){ 
	if(fd_undef(destination)){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		pd.p1 = 2;
		pd.p2 = ctiClient.getClientSession('ctiAgentExten');
		pd.p3 = destination;
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0; 
};
// ctiKawani.setAgentSplitCall[int]
ctiKawani.prototype.setAgentSplitCall = function(){
// then if add 
	var fd = {};
	fd.p0 = 'CallSplitIVR';
	fd.p1 = ctiClient.getCallSessionId();
	fd.p2 = ctiClient.getClientSession('ctiAgentExten'); 
	
	if(fd_length(fd.p1)<2){
		return 0;
	}
	// send data ok 
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
	
};
// ctiKawani.setAgentSkill[int]
ctiKawani.prototype.setAgentSkill = function(Skill){
	if(fd_undef(Skill)){
		Skill = 1;
	}
	// jika kosong :
	if( Skill == '' ){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 9;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = ctiClient.getClientSession('ctiAgentId');
		fd.p4 = Skill;
		
	var param = {
			Action 	: fd_string(fd.p0),
			Event 	: fd_string(fd.p1),
			Extens 	: fd_string(fd.p2),
			AgentId : fd_string(fd.p3),
			Skill	: fd_string(fd.p4)
		}
		
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			// console.log(lenBuffer);
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;	
}

// ctiKawani.setCallSurvey[int]::CurrentCall 
ctiKawani.prototype.setCallSurvey = function(){
	// check phone number 
	/** 
		Action : CallSurvey,
		CallSessionId : 1
		Destination : SIP/4001 
	 */
	var fd = {};
		fd.p0 = 'CallSurvey';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		
	// check callSession 
	// if(fd_length(fd.p1)<1 ){
		// console.log('WARNING: Invalid Call Session ID !');
		// return 0;
	// }	
	// then set object parameter ""
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
};

// ctiKawani.setCallTransfer[int]
ctiKawani.prototype.setCallTransfer = function(destination){
	// check phone number 
	if( fd_length(destination)<1){
		console.log('invalid extension ');
		return 0;
	}
	var fd = {};
		fd.p0 = 'CallTransfer';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
	var param = {
			Action : fd_string(fd.p0),
			CallSessionId : fd_string(fd.p1),
			Originating : fd_string(fd.p2),
			Destination : fd_string(fd.p3)
		}
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			console.log(lenBuffer);
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
};

// ctiKawani.setRegisterIP[void]
ctiKawani.prototype.setRegisterIP = function(Address, Extens, PbxId, Type){
	if(fd_length(Address)<1) return 0;
	// default PBX ID 
	if(fd_undef(PbxId)) 
		PbxId = window.sprintf('%s', '1');
	
	// default Type 
	if(fd_undef(Type)) 
		Type = window.sprintf('%s', '0');
	
	// then set data event:
	var Event = window.sprintf('%s', '3'),
		param = {
			Action : 'AstTool',
			Event : Event,
			Extens : Extens,
			PbxId : PbxId,
			Type : Type,
			Address : Address
	};
	ctiClient.setComposeMessage(param,function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
};
// ctiKawani.setCallHangup[void]
ctiKawani.prototype.setCallHangup = function(){
	var fd = {};
		fd.p0 = 'CallHangup';
		fd.p1 = ctiClient.getClientSession('ctiAgentExten');
		fd.p2 = ctiClient.getCallerSession('CallSessionId');
	var param = {
		Action : fd_string(fd.p0),
		Extens : fd_string(fd.p1),
		CallSessionId : fd_string(fd.p2) 
	};
	ctiClient.setComposeMessage(param, function(lenBuffer, ctiClient){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});
	return 0;
}; 

// ctiKawani.setCallDial[string, string]
ctiKawani.prototype.setCallDial = function(ctiCallerId, ctiAssignId){ 
  var ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	  ctiAgentExten = ctiClient.getClientSession('ctiAgentExten');
	
   // -- VALIDATION : no have data to call be terminate. 
   // console.log(ctiCallerId);
   // console.log(ctiAssignId);
   
	if(fd_undef(ctiCallerId)){
		return 0;
	}
   // -- VALIDATION : assign object data ID 
	if(fd_undef(ctiAssignId)){
		ctiAssignId = 0;
	}
   // -- VALIDATION : cannot be empty dialing number 
	if(fd_length(ctiCallerId)<3){
		return 0;
	}
	var fd = {};
		fd.p0 = 'MakeCall';
		fd.p1 = ctiCallerId;
		fd.p2 = ctiAssignId;
		fd.p3 = ctiProjectId;
		fd.p4 = ctiAgentExten;
		
	var param ={
			Action : fd_string(fd.p0),
			Extens : fd_string(fd.p4),
			CustomerId : fd_string(fd.p2),
			CallerId: fd_string(fd.p1),
			ProjectId: fd_string(fd.p3)	
		};
	ctiClient.setComposeMessage(param, function( lenBuffer, ctiClient ){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	}); 
	return 0;
};
// ctiKawani.setCallDial[integer, integer] return (string)
ctiKawani.prototype.getCtiLabel = function(type, kode){
 return fd_string(fd_libs[type][kode]);
}
// ctiKawani.getAgentStatus[null] return (integer)
ctiKawani.prototype.getAgentStatus = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentStatus'));
};
// ctiKawani.getCallStatus[null] return (integer)
ctiKawani.prototype.getCallStatus = function(){ 
 return fd_sigint(ctiClient.getCallerSession('CallStatus'));
};
// ctiKawani.getCallSessionId[null] return (string)
ctiKawani.prototype.getCallSessionId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallSessionId'));
};
// ctiKawani.getCallerId[null] return (string)
ctiKawani.prototype.getCallerId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallCallerId'));
};
// ctiKawani.getCallChannels[null] return (string)
ctiKawani.prototype.getCallChannels = function(){ 
 return fd_string(ctiClient.getCallerSession('CallChannel1'));
};
// ctiKawani.getCallDirection[null] return (string)
ctiKawani.prototype.getCallDirection = function(){ 
 return fd_string(ctiClient.getCallerSession('CallDirection'));	
};
/** 
 * https://ccm.net/forum/affich-57878-firefox-connection-interrupted
 * Type [firefox] About:config in the address bar.
 * search ipv6
 * make it FALSE If TRUE.
 * 
 */	 
try{
	$(window).on('beforeunload', function(){
		try{
			if(fd_isobj(ctiClient) 
			&& fd_isfunc(ctiClient.setWebSocketDisconect)){
				ctiClient.setWebSocketDisconect();
				return undefined;
			}
		}
		// error handing ctiClient:
		catch(error){
		  console.log(error);
		  return undefined;
		}
	});
 }
catch(error){
	console.log(error);
}
