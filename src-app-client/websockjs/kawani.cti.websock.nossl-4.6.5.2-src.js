/*!
 * Kawani -- An open source Kawani Websocket Client toolkit.
 *
 * Copyright (C) 2019 - 2021, (PDS) Peranti Digital Solusindo, PT.
 *
 * omen <jombi_par@yahoo.com>
 *
 * See http://perantidigital.co.id for more information about
 * the Asterisk project. Please do not directly contact
 * any of the maintainers of this project for assistance;
 * the project provides a web site, mailing lists and IRC
 * channels for your use.
 *
 * This program is free software, distributed under the terms of
 * the GNU General Public License Version 2. See the LICENSE file
 * at the top of the source tree.
 *
 * <BEGIN>
 *
 * @version 		4.6.5 
 * @revision 		4.6.4 
 * @date 			2021/07/21
 * @revdate 		2022/07/05
 * </BEGIN>
 *
 * Add New Hold Thread 
 */ 
const AST_AGENT_LOGOUT 					 = 0;	  
const AST_AGENT_READY 					 = 1;     
const AST_AGENT_NOT_READY 				 = 2;     
const AST_AGENT_AUX 				 	 = 2;  
const AST_AGENT_LOGIN 					 = 3;     
const AST_AGENT_BUSY 					 = 4;     
const AST_AGENT_ACW 					 = 5;     
const AST_AGENT_RESERVED 				 = 6;     
const AST_AGENT_TALKING 				 = 7;     
const AST_AGENT_REJECTED 				 = 9;     
const AST_AGENT_REGISTER 				 = 10;    
const AST_AGENT_CALLASSIGN 				 = 11;    
const AST_AGENT_IDLE					 = 25;   
const AST_CALL_PREPARING			 	 = 905;
const AST_CALL_INIATED			 		 = 999;
const AST_CALL_OFFER		 		 	 = 101;
const AST_CALL_ORIGNATING		 		 = 101;
const AST_CALL_TRUNKSIZE		 		 = 102;
const AST_CALL_DIALING			 		 = 201;
const AST_CALL_CONNECTED		 		 = 202;
const AST_CALL_ANSWERED			 	 	 = 300;
const AST_CALL_BUSY				 	 	 = 301;
const AST_CALL_NORESPONSE		 		 = 302;
const AST_CALL_NO_ANSWER		 		 = 303;
const AST_CALL_RESERVED			 		 = 200;
const AST_CALL_REJECTED			 	 	 = 304;
const AST_CALL_NUMBER_CHANGED	 		 = 305;
const AST_CALL_DESTINATION_OUT_OF_ORDER  = 306;
const AST_CALL_INVALID_NUMBER_FORMAT	 = 307;
const AST_CALL_FACILITY_REJECTED 		 = 308;
const AST_CALL_STATUS_ENQUIRY 	 		 = 309;
const AST_CALL_NORMAL_UNSPECIFIED 		 = 311;
const AST_CALL_NORMAL_CIRCUIT_CONGESTION = 312;
const AST_CALL_NETWORK_OUT_OF_ORDER	 	 = 313;
const AST_CALL_CHAN_NOT_IMPLEMENTED	 	 = 314;
const AST_CALL_CHAN_SUBSCRIBER_ABSENT 	 = 315;
const AST_CALL_ROUTED_TERMINATE		 	 = 310;
const AST_CALL_ANSWERING_MACHINE	  	 = 317;
const AST_CALL_MISSING_CALL			 	 = 318; 
const AST_AGENT_NO_ANSWER				 = 319;
const AST_SKILL_INBOUND 				 = 1;    
const AST_SKILL_OUTBOUND 				 = 2;    
const AST_SKILL_PREDICTIVE 				 = 3;    
const AST_CALL_PREDICTIVE 				 = 3;    
const AST_CALL_OUTBOUND 				 = 2;    
const AST_CALL_INBOUND 					 = 1;    
const AST_CALL_DEVELOPER 				 = 1; 	/*!< matikan log di sisi clients */  
const AST_CALL_MASKING 					 = 0; 	/*!< matikan masking di sisi clients */ 
const AST_MUTE_STATE_ON					 = 1;    
const AST_MUTE_STATE_OFF				 = 0;
const AST_MUTE_TYPE_ALL					 = 0;
const AST_MUTE_TYPE_IN				 	 = 1;
const AST_MUTE_TYPE_OUT				 	 = 2;

/*! Window default configuration setups */
window.WebSocketVersion  = "4.6.5";
window.WebSocketInterval = 0;
window.WebSocketTimeout  = 10000;
window.WebSocketChnnels  = {};
window.WebSocketSACAgent = 0; 
window.WebSocketSACManager = 0; 

var fd_libs = { 
  // INBOUND:
	1 : {
		999:'Call Incoming Ringing',  
		100:'Call Incoming Ringing', 
		101:'Call Incoming Ringing',  
		102:'Call Incoming Ringing',  
		201:'Call Incoming Ringing',  
		202:'Call Incoming Connected',  
		300:'Call Incoming Answered', 
		301:'Call Incoming Ignore',
		302:'Call Incoming Congestion',
		303:'Call Incoming Rejected',
		318:'Call Incoming Misscall',
		319:'Call Incoming Decline'
	},
// OUTBOUND:	
   2 : {
		999:'Call Iniate', 
		100:'Call Offered', 
		101:'Call Dialing', 
		102:'Call Ring', 
		201:'Call Ringing', 
		202:'Call Connected', 		 
		300:'Call Answered', 
		301:'Call Cancel',
		302:'No User Response',
		303:'No Answered',
		304:'Call Rejected',
		305:'Change Number',
		306:'Destination Out Of Order',
		307:'Invalid Number Format',
		308:'Facility Rejected',
		309:'Response To Status Enquiry',
		311:'Normal Unspecified',
		312:'Normal Circuit Congestion',
		313:'Network Out Of Order',
		314:'Chan Not Implemented',
		315:'Chan Subcriber Absent'
	},
	// PREDICTIVE:
	3 : {
		999:'Predictive from ',  
		100:'Predictive Ringing', 
		101:'Predictive with',  
		102:'Predictive from',  
		201:'Predictive from',  
		202:'Predictive Connected',  
		300:'Predictive Answered', 
		301:'Predictive Ignore',
		302:'Predictive Congestion',
		303:'Predictive Rejected',
		310:'Predictive Routed',
		318:'Predictive Misscall',
		319:'Call Incoming Decline'
	}
};

// New On MediaChannel source
if ( typeof fd_Media != 'object' ){
 var fd_Media = {
	'IVR'   : { id: 1, name: 'IVR'  }, 	/*!< media channel event IVR blaster 	*/
    'Call'  : { id: 2, name: 'IVR'  }, 	/*!< media channel event Call directly 	*/	
	'Sms'   : { id: 3, name: 'SMS'  },	/*!< media channel event SMS directly	*/
	'Mail'  : { id: 4, name: 'Mail' },	/*!< media channel event Mail directly  */
	'Chat'  : { id: 5, name: 'Chat' }	/*!< media channel event Chat directly	*/
 }; 
}
// https://www.codegrepper.com/code-examples/typescript/how+to+check+browser+type+with+javascript
window.strtotime = function( now ){
	var retval = ''; if ( typeof now == 'undefined' ){
		var now = Date.now();
	}
	// finally "string"
	retval = fd_string(now);
	return retval;
} 
// https://www.codegrepper.com/code-examples/typescript/how+to+check+browser+type+with+javascript
window.detectBrowser = function(){ 
   /*!< OP= Opera , CH= Chrome, SF= Safari, FF= Firefox, IE= MSIE, DF= Unknown */
    if((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1 ) {
        return 'OP';
    } else if(navigator.userAgent.indexOf("Chrome") != -1 ) {
        return 'CH';
    } else if(navigator.userAgent.indexOf("Safari") != -1) {
        return 'SF';
    } else if(navigator.userAgent.indexOf("Firefox") != -1 ){
        return 'FF';
    } else if((navigator.userAgent.indexOf("MSIE") != -1 ) || (!!document.documentMode == true )) {
        return 'IE';//crap
    } else {
        return 'DF';
    }
	return "";
}  
// http://www.navioo.com/javascript/tutorials/Javascript_microtime_1583.html
window.microtime = function(get_as_float) {  
    // Returns either a string or a float containing the current time in seconds and microseconds    
    //   
    // version: 812.316  
    // discuss at: http://phpjs.org/functions/microtime  
    // +   original by: Paulo Ricardo F. Santos  
    // *     example 1: timeStamp = microtime(true);  
    // *     results 1: timeStamp > 1000000000 && timeStamp < 2000000000  
    var now = new Date().getTime() / 1000;  
    var s = parseInt(now);  
    return (get_as_float) ? now : (((now - s) * 1000)/1000).toFixed(8) + ' ' + s;  
}  

if ( typeof(window.sprintf) != 'function' ){   
window.sprintf = function( format ) {
  for( var i=1; i < arguments.length; i++ ) {
	format = format.replace( /%s/, arguments[i] );
  }
  return format;
};
}
if ( typeof(window.strcmp) != 'function' ){  
	window.strcmp = function( key, val ) {
		if( key!== null && key.localeCompare(val) == 0 ){
			return true;
		} 
		return false;
	};
}
if ( typeof(window.fd_isfunc) != 'function' ){
function fd_isfunc(data){
	if( typeof(data) == 'function' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_isobj) != 'function' ){
function fd_isobj(data){
	if( typeof(data) == 'object' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_undef) != 'function' ){
function fd_undef(data){
	if( typeof(data) == 'undefined' ){
		return true;
	}
	return false;
 } 
}
if ( typeof(window.fd_sigint) != 'function' ){
function fd_sigint(data){
	return parseInt(data);
 } 
}
if( typeof(window.fd_string) != 'function' ){
function fd_string(data){
	if( fd_undef(data) ){
		return "";
	}
	return data.toString();
 } 
}
if( typeof(window.fd_length) != 'function' ){
function fd_length(data){
	return fd_string(data).length;
 } 
}

if( typeof(window.fd_msg) != 'function' ){
function fd_msg(data){
	window.alert(data);
 } 
}
if( typeof(window.fd_unixtime) != 'function' ){
function fd_unixtime(time){
	var time_to_show = fd_sigint(time); // unix timestamp in seconds
	var t = new Date(time_to_show * 1000);
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
}
}
if( typeof(window.timestamp) != 'function' ){
 function timestamp(){
	var t = new Date();
	var formatted = ('0' + t.getHours()).slice(-2) + ':' + ('0' + t.getMinutes()).slice(-2) + ':' + ('0' + t.getSeconds()).slice(-2);
	return formatted;
 }
} 
if( typeof(window.zeropad) != 'function' ){
 window.zeropad = function(nr,base){
  var  len = (String(base).length - String(nr).length)+1;
  return len > 0? new Array(len).join('0')+nr : nr;
 }
}
 
if( typeof(window.duration) != 'function' ){
window.duration = function(second){
	var sec = 0, min = 0, hour= 0; 
	var retval = ""; 
	// next process calculation :
	sec = second%60; 
    second = Math.floor(second/60);  
	if(second){
		min  = second%60;
        hour = Math.floor(second/60);
    }
	if(second== 0 && sec == 0) return "";
    else {
	  retval = window.sprintf("%s:%s:%s", zeropad(hour,10), zeropad(min, 10), zeropad(sec,10)); 
	}
	return retval;
 }
} 

/*! \remark find array Or InAarray*/
if (window.search !== "function")
{
	window.search = function(s, populates)
	{
		var retval = 0; 
		populates.forEach((value, index, populasi) => {
			if (s == value){
				retval = value;
			}
		});
		return retval;
	}
} 

/**
 * --TODO: 
 *  @method		 :  object class [writeloger]
 *	@description :	window.writeloger['onready', 'onConnect', 'onThread', 'onClose']
 */
if( typeof(window.writeloger) != 'function' ){
window.writeloger = function(message){
	var verbose='', logWrite = sprintf('%s WS >> %s',timestamp(), message);
	if (fd_isobj(message)){
		verbose = sprintf('VERBOSE:%s\n','WS');
		console.log(verbose);
		console.log(message);
		console.log('\n');
	}
	// not object:
	else if(!fd_isobj(message) &&(fd_length(logWrite)>0)){
		if(parseInt(AST_CALL_DEVELOPER)>0){
			console.log(logWrite);
		}
	}
	return;
 } 
}
/**
 * --TODO: 
 *  @method		 :  object class [ctimasking]
 *	@description :	window.ctimasking[string]
 */
if( typeof(window.ctimasking) != 'function' ){
window.ctimasking = function(vl){
 if(fd_sigint(AST_CALL_MASKING)<1) return fd_string(vl);
 else if(fd_sigint(AST_CALL_MASKING)>0) {
	var lt = 3, rs = [], retval = '';
	if(fd_length(vl)<1) return retval;
	else if(fd_length(vl)>2){
		var fs = fd_string(vl).split(''), ar= [], dl= Math.round((fs.length/lt)), ts = 0, sl = null;
		for(var i = 0; i<dl; i++){
			ts = (lt*i);
			if(!ts){
				sl = fs.slice(ts,lt); sv = sl.join('');
				if(fd_length(sv)>0){ 
					ar.push(fd_string(sv));
				}
			}
			// jika ts lebih besar untuk data process 
			if(ts>0){
				sl = fs.slice(ts,lt+ts); sv = sl.join('');
				if(fd_length(sv)>0){ 
				  ar.push(fd_string(sv));
				} 
			}
		}
		
		// INFO: check setiap block  
		if(!fd_isobj(ar)) return retval;
		else if(fd_isobj(ar)) {
			for(var j in ar){
				if(j%2!=0) {
					var rv = ar[j].split(''); for( var ri in rv ){
						rs.push('x');
					}
				}
				if(j%2==0) rs.push(ar[j]);  
			}
		}
		// INFO : nilai balikan data process OK 
		retval = (fd_isobj(rs)?rs.join('') : '');
     }
	 return fd_string(retval);
   }
 } 
}
 
/**
 * --TODO: 
 *  @method		 :  object class [ctiKawani]
 *	@description :	window.ctiKawani['onready', 'onConnect', 'onThread', 'onClose']
 */
function ctiKawani( p1, p2, p3, p4, p5)
{ 
	this.onType 	= 'ws';
	this.onSocket 	= null;
	this.onHost 	= null;
	this.onPort 	= null;
	this.onReady 	= p1;
	this.onConnect 	= p2;
	this.onThread 	= p3;
	this.onClose 	= p4; 
	this.onError 	= p5; 
	this.onUrl		= null;
	this.onReject	= 0;
	return this;
};

// ctiKawani.setConfigServer[object] 
 ctiKawani.prototype.setConfigServer = function(param)
{
	if (fd_isobj(param))
	{
		/*! \remark tambahan untuk session IVR khusus Inbound */
		this.session = param; 
		if (fd_undef(this.session.ivr))
		{
			this.session.ivr = {};
		}							
		
		if (fd_undef(this.session.caller))
		{
			this.session.caller = {};
		}
		
		/*! \add new storage linef for Multiple channel handleEvent */
		if (fd_undef(this.session.contact))
		{
			this.session.contact = {};
			ws_storage_contact = window.localStorage.getItem('cti_ws_contact'); 
			if (typeof ws_storage_contact === "string"){
				ws_storage_contact = JSON.parse(ws_storage_contact); 
				if (typeof ws_storage_contact === "object"){
					this.session.contact = ws_storage_contact;
				}
			} 
		}
		/*! \remark tamabahan untuk versi 4.6.3 */
		if (fd_undef(this.session.callbacks))
		{
			this.session.callbacks = {};
		}
	}
};
/*! \brief ctiKawani.setConnectServer[boolean, object] */
 ctiKawani.prototype.setConnectServer = function(sockStatus, server)
{
	if (sockStatus > 0 &&(fd_sigint(server.readyState)>0))
	{
		var p = ctiClient.session.label.serverState;
		if (fd_isobj(p))
		{
			writeloger('INFO: Connecting ...');
			p.html('Connecting ...');
		}
		
		ctiClient.setAgentRegister();
	}
};
// ctiKawani.getClientSession[string] 
ctiKawani.prototype.getClientSession = function(key){
	try 
	{
		var param = this.session.client, result = ''; 
		if(!fd_isobj(param)){
			return false;
		} if(!fd_undef(param[key])){
			result = param[key].toString();
		}
		return result;
	}
	catch(error){
		console.log( "Error 'ctiKawani.prototype.getClientSession()'")
		return 0;
	}		
};
// ctiKawani.getCallerSession[string] 
ctiKawani.prototype.getCallerSession = function(key){
	var param = this.session.caller,
		result = "";
	if(!fd_isobj(param)){
		return false;
	}
	if(!fd_undef(param[key])){
		result = param[key].toString();
	}
	return result;
};

// ctiKawani.getIvrSession[string] 
ctiKawani.prototype.getIvrSession = function(key)
{
	var param = this.session.ivr, result = "";
	if(!fd_isobj(param)){
		return false;
	}
	
	/*! \remark jika tidak ada key yang di cari maka tampilakn semua data */
	if (typeof key === "undefined")
	{
		var output = ctiClient.getCallerSession('CallIVRData');
		if (typeof output === "undefined"){
			return "";
		}
		/*! \retval process */
		return fd_string(output);
	}
	
	if (!fd_undef(param[key]))
	{
		result = param[key].toString();
	}
	return result;
};		

// ctiKawani.setThreadDisplayTimer
 ctiKawani.prototype.setThreadDisplayTimer = function(display, status)
{	var ast_pool= {}; ast_pool.ticker = 1000; ast_pool.status = status; ast_pool.timer = 1;
	window.ThreadDisplayTimerId = window.setInterval(() => {
		ast_pool.value = ctiClient.getClientSession('ctiAgentStatus');
		if (ast_pool.status != ast_pool.value ){
			window.clearInterval(window.ThreadDisplayTimerId);	
		} else { 
			display.html(sprintf(" ( %s \s )", ast_pool.timer));
			display.css({"font-size": "11px"});
		}
		ast_pool.timer += 1; 
	}, ast_pool.ticker); 
	return 0;
}

// ctiKawani.setStringToBinary[string] 
ctiKawani.prototype.setStringToBinary = function(buff){
 var buffs = buff.split(''), byteASCII = new Array(), byteString = '';
 for(var i = 0; i<buffs.length; i++){
	byteASCII.push(buff.charCodeAt(i)); 
 } 
 // set to data string;
 if(byteASCII.length>1){
	byteString = byteASCII.join('x');
 }
 return fd_string(byteString);
 
}
	
// ctiKawani.setBinaryToString[string] 
ctiKawani.prototype.setBinaryToString = function(buff)
{
	var chr = buff.split('x'), message = '';
	for (var i in chr)
	{
		if(chr[i] == '13') continue;
		else if(chr[i] == '10') continue;
		else if(chr[i] != '') {
			message+= String.fromCharCode(chr[i]);
		}
	}
	
	//return to string:
	return fd_string(message);
}
	
// ctiKawani.setParseMessage[string] 
 ctiKawani.prototype.setParseMessage = function( message )
{
	var message = this.setBinaryToString(message);
	var param = message.split('|'),
		buffer = {};
	for( i in param  ){
		var ptr = param[i].split(':');
		if(fd_isobj(ptr)){
			buffer[ptr[0]] = ptr[1];
		}
	}	
	var tParam = {
		get : function( key ){
			if(!fd_undef(buffer[key])){
				return buffer[key];
			}
			return "";
		},
		fetch : function(){
			return (fd_isobj(buffer) ? buffer : false);
		}
	}
	return tParam;
};

 
/**!
 * \brief   ctiKawani.setThreadCallState
 *  
 * \remark  Event Handler for Call State Data 
 * \param   {object:param} object stream from socket 	
 *
 * \retval  void(0)
 *
 */  
 ctiKawani.prototype.setThreadCallState = function(param)
{
	if(!fd_isobj(param)){
		return 0;
	}
	// typeof inbound 	
	var fd = {};
		fd.p1 = param.CallDirection;
		fd.p2 = param.CallStatus;
		fd.p4 = window.ctimasking(param.CallerID);
		fd.p7 = ctiClient.session.label.callState;
		//console.log(fd.p7);
	
	// inbound Call 
	if(fd_isobj(param) && fd.p1==1){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// outbound call 
	else if(fd_isobj(param) && fd.p1==2){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}	
	// Predictve Call
	else if(fd_isobj(param) && fd.p1==3){
		fd.p3 = ctiClient.getCtiLabel(fd.p1,fd.p2);
		if(fd_length(fd.p3)>0){
			fd.p6 = sprintf('%s ( %s )', fd.p3, fd.p4);
			if(fd_isobj(fd.p7)){
				fd.p7.html(fd.p6);
			}
		}
	}
	// end md:
	return 0;
};
 
/**!
 * \brief   ctiKawani.setThreadPeers
 *  
 * \remark  Event Handler for data SIP status 
 * \param   {object:param} object stream from socket 	
 *
 * \retval  void(0)
 *
 */  
 ctiKawani.prototype.setThreadPeers = function(param)
{
	// console.log(param);
	if( !fd_isobj(param) ){
		return 0;
	}
	// -- WARNING INFO :
	var fd = param.fetch();
		fd.p0 = ctiClient.session.label.extenState;
		
	// console.log(fd);
	// SIP:UNREGISTERED
	if(strcmp(fd.ReasonId,'101')){	
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Not Registered', fd.Extens)
			}
		});
		
		return 0;
	}
	// SIP:UKNOWN REASON
	else if(strcmp(fd.ReasonId,'102')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Not Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Not Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	// sip on data process Registered
	else if(strcmp(fd.ReasonId,'103')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	// sip data process on her look like 
	else if(strcmp(fd.ReasonId,'104')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		// finally return data 
		return 0;
	}
	
	// sip on register on following data 
	else if(strcmp(fd.ReasonId,'105')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s Registered', fd.Extens)
			}
		});
		return 0;
	}
	
	// SIP:Broken ACD Terjadi kesalahan pada saat distribute call 
	else if(strcmp(fd.ReasonId,'106')){
		// sent data to browser client 
		fd.p0.html(sprintf('SIP: %s -- INFO: Broken ACD', fd.Extens));
		ctiClient.setThreadError({
			event : 'ctierror',  
			type  : 4,
			body  : {
				header  : 'SIP Information',
				message : sprintf('SIP: %s -- INFO: Broken Network', fd.Extens)
			}
		});
		
		// next to Timeout 
		window.setTimeout( () => {
		   fd.p0.html(sprintf('SIP: %s -- INFO: Registered', fd.Extens));	
		},5000);
		return 0;
	}
	return 0;
};

/**!
 * \brief   ctiKawani.setThreadLogin
 *  
 * \remark  Event Handler On Agent Register 
 * \param   {object:fd} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadLogin = function(fd)
{ 
	if(!fd_isobj(fd)){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will__data 
	if(fd_length(fd.ReasonVal)<0){
		fd_msg('-- WARNING -- Wrong Object Login');
		return 0;
	}
	// then will set _key 
	if(fd.ReasonVal =='success'){
		ctiClient.setClientSession('ctiAgentKey', fd.AgentKey);
		window.setTimeout(() => {
			var c = ctiClient.getLocalAgentStatus(fd.AgentId);
			if(c != null &&(c.status == AST_AGENT_NOT_READY)){
			  ctiClient.setAgentNotReady(c.reason);
			}else {
			  ctiClient.setAgentReady();
			}
		}, 1000);
		return 0;
	}
	else {
		// console.log(fd);
		fd_msg( '-- WARNING -- Invalid Login' );
		var p0 = ctiClient.session.label.agentState
		if( fd_isobj(p0)){
			p0.html(fd.Message);
		}
		// stop don't  try to connected . close(this);
		ctiClient.onReject ++;
		return 0;
	}
	return 0;
};

/**!
 * \brief   ctiKawani.setThreadRegister
 *  
 * \remark  Event Handler On Agent Register 
 * \param   {object:param} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadRegister = function(param)
{ 

	var fd = {}, s = '';
	
	fd.p0 = param.ReasonVal;
	fd.p1 = ctiClient.session.label.agentState;
	fd.p2 = ctiClient.session.label.extenState;
	
	// if length invalid 
	if (fd_length(fd.p0)<1)
	{
		return 0;
	}
	
	/*! \remark if response CTI "success" */
	if (fd.p0 == 'success'  )
	{
		s = ctiClient.getAgentStatus(); 
		if (s != AST_AGENT_BUSY){ 
			window.setTimeout(() => {	
				ctiClient.setAgentLogin();
			}, 1000); 
		}
		if (s == AST_AGENT_BUSY){ 
			window.setTimeout(() => {
				ctiClient.setAgentLogin();
			},100); 
		}
		return 0;
	}
	
	/*! \remark if response CTI "failed" */
	else 
	{
		writeloger( sprintf('WARNING: Invalid Register host=%s exts= %s used by other user.',param.Address, param.Extens) );
		if (fd_isobj(fd.p1))
		{
		   fd.p1.html('Register');
		}
		
		if (fd_isobj(fd.p2) ) 
		{
		  fd.p2.html('-- INFO: Rejected');
		}
		
		/*! \remark Add Incremnet if Reject don't try connected again .! */
		ctiClient.onReject ++;
		return 0;
	}
	
	return 0;
};

/**!
 * \brief   ctiKawani.setThreadState
 *  
 * \remark  Event Handler On Agent Register 
 * \param   {object:param} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadState = function(param)
{ 
	var tParam = param.fetch(),
		tTrack = tParam.Value
		
	var fd = {};
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.callState; 
		fd.p3 = ctiClient.session.label.serverState;
		 
	/*! \remark AST_AGENT_LOGOUT */
	var s = null; 
	if (fd_sigint(tTrack) == AST_AGENT_LOGOUT)
	{
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if (fd_isobj(fd.p1))
		{
		   fd.p1.html('logout');
		   fd.p3.html('');
		}
		
		if (fd_isobj(fd.p2))
		{
		   fd.p2.html('');
		}
		
		/*! \remark disconnected */
		ctiClient.setWebSocketDisconect(1);
		return 0;
	} 
	
   /*! \remark AST_AGENT_REGISTER  
    *  Handler Untuk Ambil Last status di sisi client Jika terjadi Reload page 
	*  Atau rfresh Browser yang Berulang2 
	*/	
	else if (fd_sigint(tTrack) == AST_AGENT_REGISTER)
	{
		s = ctiClient.getAgentStatus();
		if (s == AST_AGENT_BUSY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else if (s == AST_AGENT_NOT_READY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else if (s == AST_AGENT_READY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else {
			ctiClient.setClientSession('ctiAgentStatus', tTrack);
		}
		
		/*! \remark for label state */
		if (fd_isobj(fd.p1) &&(s = ctiClient.getAgentStatus()))
		{
			if (s == AST_AGENT_BUSY) fd.p1.html('Busy');
			else if (s == AST_AGENT_NOT_READY) fd.p1.html('Not Ready');
			else if (s == AST_AGENT_READY) fd.p1.html('Ready'); 
			else {
				fd.p1.html('Register');
			}
		}
		
		if (fd_isobj(fd.p2))
		{
		   fd.p2.html('idle');
		}
		return 0;
		
	} 
	/*! \remark AST_AGENT_LOGIN */		
	else if (fd_sigint(tTrack) == AST_AGENT_LOGIN)
	{
		s = ctiClient.getAgentStatus();
		if (s == AST_AGENT_BUSY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else if (s == AST_AGENT_NOT_READY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else if (ctiClient.getAgentStatus() == AST_AGENT_READY){ 
			ctiClient.setClientSession( 'ctiAgentStatus', 
				ctiClient.getAgentStatus()
			);
		}
		else {
			ctiClient.setClientSession('ctiAgentStatus', tTrack);
		}
		
		/*! \remark for skill data */
		if (!fd_undef(tParam.Skill) &&( fd_isfunc(ctiClient.setThreadSkill) )) 
		{
			ctiClient.setThreadSkill.apply(this, [param]); 
		}
		
		/*! \remark for label state */
		if (fd_isobj(fd.p1) &&(s = ctiClient.getAgentStatus()))
		{ 
			if (s == AST_AGENT_BUSY) fd.p1.html('Busy');
			else if (s == AST_AGENT_NOT_READY) fd.p1.html('Not Ready');
			else if (s == AST_AGENT_READY) fd.p1.html('Ready');
			else {
				fd.p1.html('Login');
			}
		}
		
		if (fd_isobj(fd.p2))
		{
			fd.p2.html('idle');
		}
	}
	/*! \remark AST_AGENT_READY */	
	else if (fd_sigint(tTrack) == AST_AGENT_READY) 
	{
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if (!fd_undef(tParam.Skill) &&(fd_isfunc(ctiClient.setThreadSkill)))
		{
			ctiClient.setThreadSkill.apply(this, [param]); 
		}
		
		if (fd_isobj(fd.p1))
		{
			fd.p1.html('Ready');
		}
		
		if (fd_isobj(fd.p2))
		{
			fd.p2.html('idle');
		}
	} 
	/*! \remark AST_AGENT_NOT_READY */		
	else if (fd_sigint(tTrack) == AST_AGENT_NOT_READY) 
	{
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if (!fd_undef(tParam.Skill) &&( fd_isfunc(ctiClient.setThreadSkill)))
		{
			ctiClient.setThreadSkill.apply(this, [param]); 
		}
		 
		var p1 = tParam.ReasonVal, p2 = 'Not Ready';
		if (fd_length(p1)> 2)
		{
			p2 = sprintf('%s ( %s )', p2, p1);
		}
		
		if (fd_isobj(fd.p1))
		{
			fd.p1.html(p2);
		}
		
		if (fd_isobj(fd.p2))
		{
			fd.p2.html('idle');
		}  
	} 
	/*! \remark AST_AGENT_BUSY */	
	else if (fd_sigint(tTrack) == AST_AGENT_BUSY) 
	{
		ctiClient.setClientSession('ctiAgentStatus',tTrack);
		if (fd_isobj(fd.p1))
		{
			fd.p1.html('Busy');
		} 
	}
	/*! \remark AST_AGENT_ACW */
	else if (fd_sigint(tTrack) == AST_AGENT_ACW) 
	{	
		ctiClient.setClientSession('ctiAgentStatus', tTrack);
		if (fd_isobj(fd.p1)){
			fd.p1.html('Acw <span class="acwtimer"></span>');
			if (fd_isfunc(ctiClient.setThreadDisplayTimer)){ 
				/* Check if have timer for display time */
				var display = $('.acwtimer'); if (fd_isobj(display)){
					ctiClient.setThreadDisplayTimer.apply(this, [display, AST_AGENT_ACW]);
				}
			}
		} 
	} 
	return 0;
};

/**!
 * \brief   ctiKawani.setThreadHold
 *  
 * \remark  Event Handler On Agent Register 
 * \param   {object:param} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadHold = function(param)
{
	var tParam = param.fetch();
	var fd = {};
		fd.p1 = ctiClient.session.label.agentState;
		fd.p2 = ctiClient.session.label.callState; 
		fd.p3 = ctiClient.session.label.serverState; 
		
	var Event = tParam.Event,
		CallerId = tParam.CallerId,
		Direction = tParam.Direction,
		CallSessionId = tParam.SessionId;
		
	
	
	if (fd_sigint(Event) == 1){
		fd.p2.html(sprintf("Call On Hold ( %s )", CallerId));
		return 0;
	}
	// stop Hold Triger By Server 
	if (fd_sigint(Event) == 2){
		var CallState = ctiClient.getCtiLabel(Direction, 202),
			CallLabel = window.sprintf("%s ( %s )", CallState, CallerId);
			
		// ketika Call Active Maka masukan CallSessionId Untuk Menghindari 
		// Salah ketika Hangup 
		
		if(fd_length(CallSessionId)>0){
			ctiClient.setCallSessionId(CallSessionId);
		}	
		
		if (fd_isobj(fd.p2)){ 
			fd.p2.html(CallLabel);
		}	
	}
	return 0;
}



/**!
 * \brief   ctiKawani.setThreadCallCtxfer
 *  
 * \remark  Handler for Call Again for confirm 
 * \param   {param:object} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadCallCtxfer = function(param)
{
	var s = param.fetch(), o = {};
	
   /*! \remark add new object */ 
	o.Exten     = s.Exten;
	o.Called 	= s.Called;  
	o.Message   = s.Message;
	o.Response  = s.Response;
	
	/*! \remark  user delegate function to aksess */
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
	   ctiClient.setIntefaceThread
	   ({
			event : fd_string('callctxfer'),
			body  : {
			   type : 1,
			   data : o 
			}
		});
	} 
	
	return 0;
}


/**!
 * \brief   ctiKawani.setThreadCallBridge
 *  
 * \remark  Handler for CallBridge Message 
 * \param   {param:object} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadCallBridge = function(param)
{
	var s = param.fetch(), o = {};
	
   /*! \remark add new object */ 
	o.Exten     = s.Exten;
	o.SessionId = s.SessionId; 
	o.Channel1  = s.Channel1;
	o.Channel2  = s.Channel2;
	o.Message   = s.Message;
	o.Response  = s.Response;
	
	/*! \remark  user delegate function to aksess */
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
	   ctiClient.setIntefaceThread
	   ({
			event : fd_string('callbridge'),
			body  : {
			   type : 2,
			   data : o 
			}
		});
	} 
	
	return 0;
}
/**!
 * \brief   ctiKawani.setThreadAcwTimeout
 *  
 * \remark  Handler for Set ACW Timeout Message 
 * \param   {param:object} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadAcwTimeout = function(param)
{
	var s = param.fetch(), o = {};
		o.AgentId 	= s.AgentId;
		o.Exten 	= s.Exten;
		o.AgentName = s.AgentName;
		o.Timeout 	= s.Timeout;
		o.Message 	= s.Message;
		o.Response 	= s.Response;
		
	/*! \remark  user delegate function to aksess */
	ctiClient.setClientSession('ctiAcwTimeout', o.Timeout); 
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
	   ctiClient.setIntefaceThread
	   ({
			event : fd_string('acwtimeout'),
			body  : {
			   type : 1,
			   data : o 
			}
		});
	}  
	return 0; 
}

/**!
 * \brief   ctiKawani.setThreadRecording
 *  
 * \remark  Handler for Recording Message 
 * \param   {param:object} object stream from socket 	
 *
 * \retval  void(0)
 *
 */
 ctiKawani.prototype.setThreadRecording = function(param)
{
	var s = param.fetch(), o = {};
	
   /*! \remark add new object */
	o.RecordingId = s.RecordingId;
	o.SessionId   = s.CallSessionId;
	o.Direction   = s.CallDirection;
	o.AssignId 	  = s.CallAssignId;
	o.CallerId 	  = s.CallerId;
	o.StartTime   = s.StartTime;
	o.AnswerTime  = s.AnswerTime;
	o.EndTime 	  = s.EndTime;
	o.Duration 	  = s.Duration;
	o.FileType 	  = s.FileType;
	o.FileName 	  = s.FileName;
	o.FileSize 	  = s.FileSize;
	o.Exten 	  = s.Extension;
	o.AgentId 	  = s.AgentId;
	o.AgentName   = s.AgentName;
	
	/*! \remark  user delegate function to aksess */
	ctiClient.setCallSession('RecordingId', s.RecordingId); 
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
	   ctiClient.setIntefaceThread
	   ({
			event : fd_string('recording'),
			body  : {
			   type : fd_sigint(s.CallDirection),
			   data : o 
			}
		});
	} 
	
	return 0;
}

// ctiKawani.setCallIvrData[string, string] 
 ctiKawani.prototype.setCallIvrData = function(key, val)
{ 
	ctiClient.session.ivr[key] = val; 
	return 0;
};	

// ctiKawani.setCallSession[string, string] 
 ctiKawani.prototype.setCallSession = function(key, val)
{ 
	ctiClient.session.caller[key] = val; 
	return 0;
};

// ctiKawani.setClientAddContact[string, string] 
 ctiKawani.prototype.setClientAddContact = function(sessionid, key, val)
{ 
	try { 
		if (typeof ctiClient.session.contact[sessionid] === "undefined"){
			ctiClient.session.contact[sessionid] = {};
		}
		
		if (typeof key !== "object"){ 
			ctiClient.session.contact[sessionid][key]= val;
		}
		
		if (typeof key === "object"){ 
			for(var s in key){
				ctiClient.session.contact[sessionid][s]= key[s];
			}
		}
		/*! \remark simpan ke localStorage */  
		window.localStorage.setItem('cti_ws_contact', 
			JSON.stringify(ctiClient.session.contact)
		); 
	}
	catch(error) { 
		console.log("Error 'ctiKawani.prototype.setClientAddContact()'"); 
	}
	return 0;
};

// ctiKawani.setClientDeleteContact[string, string] 
 ctiKawani.prototype.setClientDeleteContact = function(sessionid)
{ 
	try { 
		if (typeof ctiClient.session.contact[sessionid]  === "object"){
			delete ctiClient.session.contact[sessionid];
			window.localStorage.setItem('cti_ws_contact', 
				JSON.stringify(ctiClient.session.contact)
			);
		}
	}
	catch(error) { 
		console.log("Error 'ctiKawani.prototype.setClientDeleteContact()'"); 
	}
	return 0;
};

// ctiKawani.setClientSession[string, string] 
 ctiKawani.prototype.setClientSession = function(key, val)
{ 
	try { ctiClient.session.client[key] = val; }
	catch(error) { console.log("Error 'ctiKawani.prototype.setClientSession()'"); }
	return 0;
};

// ctiKawani.setThreadError[string]
 ctiKawani.prototype.setThreadError = function(p)
{ 
	var tParam = (typeof(p) == 'object' ? p: false);
	if (fd_isobj(tParam))
	{
		ctiClient.setIntefaceThread({
			event : fd_string(p.event),
			body  : {
				type : fd_string(p.type),
				data : {
					header  : fd_string(p.body.header),	
					message : fd_string(p.body.message)
				} 
			}
		}); 
	} 
	 
	return 0;
}
	

// ctiKawani.setThreadAlarm[object]
 ctiKawani.prototype.setThreadAlarm = function(p)
{ 
	var f = p.fetch(), s = null; 
	if ((f.Action == 'Alarm'))
	{
		s = ctiClient.session.label.extenState;
		if (fd_isobj(s))
		{
			var e = sprintf("* PBX : %s -- INFO: %s", f.ServerHost, f.ServerError);
			s.html( fd_string(e) );
		}
	}
	
	return 0;
}

// ctiKawani.setThreadTimout[object]
 ctiKawani.prototype.setThreadTimout = function(p)
{ 
	var f = p.fetch(), s = false; 
	if ((f.Action == 'Expired'))
	{
		ctiClient.setClientSession('ctiClientTimeout', f.SockId); 
		s = ctiClient.session.label.extenState;
		if (fd_isobj(s))
		{
			writeloger(sprintf("Got MSG INFO = Timeout #ID= %s #PID= %s  MSG ERR = %s", f.SockId, f.SockPid, f.SockErr));
		}
	}
	return 0;
}

// ctiKawani.setThreadTransferCall[object]
 ctiKawani.prototype.setThreadTransferCall = function(p)
{ 
	var tParam = p.fetch();  
	if (fd_isobj(tParam) &&(!fd_undef(tParam.CallSessionId))) 
	{
		if(fd_undef(tParam.EventState)) tParam.EventState = 1; // default 
		ctiClient.setIntefaceThread
		({
			event : fd_string('transfercall'),
			body : {
				type : fd_sigint(tParam.EventState),
				data : tParam 
			}
		}); 
	}
	
	return 0;
};

 
// ctiKawani.setThreadMediaChannel[object]
// 1:IVR , 2:Call, 3:SMS, 4:Mail,5:Chat  

 ctiKawani.prototype.setThreadMediaChannel = function(p)
{ 
	var tParam = p.fetch();  
	if(fd_isobj(tParam) &&(!fd_undef(tParam.UniqueId)))
	{
		var MediaChannel = ctiClient.getMediaType(tParam.MediaType); tParam.EventState = 0;
		if (typeof(MediaChannel) == 'object' )
		{
			tParam.EventState = fd_sigint(MediaChannel.id);
		}
		
		ctiClient.setIntefaceThread
		({
			event : fd_string('mediachannel'),
			body : {
				type : tParam.EventState,
				data : tParam 
			}
		}); 
		
	} 
	
	return 0;
};


// ctiKawani.setThreadForwardCall[object]
 ctiKawani.prototype.setThreadForwardCall = function(p)
{ 
	var tParam = p.fetch();  
	if(fd_isobj(tParam) &&(!fd_undef(tParam.CallSessionId)))
	{
		if (fd_undef(tParam.EventState)) tParam.EventState = 1; // default 
		ctiClient.setIntefaceThread
		({
			event : fd_string('forwardcall'),
			body  : {
				type : fd_sigint(tParam.EventState),
				data : tParam 
			}
		}); 
	} 
	
	return 0;
} 

// ctiKawani.setThreadParkCall[object]
 ctiKawani.prototype.setThreadParkCall = function(p)
{ 
	var tParam = p.fetch(); 
	if (fd_isobj(tParam) &&(!fd_undef(tParam.SessionId)))
	{
		// createDocumentFragment Interface :
		ctiClient.setIntefaceThread
		({
			event : fd_string('parkcall'),
			body  : {
				type : fd_sigint(tParam.EventState),
				data : tParam 
			}
		}); 
	}
	 
	return 0;		  
};

// ctiKawani.setThreadCallSplitIVR[object]
 ctiKawani.prototype.setThreadCallSplitIVR = function(p)
{ 
	var tParam = p.fetch(); 
	if (fd_isobj(tParam) &&(!fd_undef(tParam.SessionId)))
	{
		ctiClient.setClientSession('ctiCallSplitIVRData', tParam.CallData);  
		ctiClient.setCallSession('ctiCallSplitIVRData', tParam.CallData);  
		ctiClient.setIntefaceThread
		({
			event : fd_string('callsplitivr'),
			body  : {
				type : fd_sigint(tParam.EventState),
				data : tParam 
			}
		}); 
	}
	
	return 0;
};
 
// ctiKawani.setThreadVoiceMail[object]
 ctiKawani.prototype.setThreadVoiceMail = function(p)
{ 
	var fds = p.fetch(); 
	if ( fd_isobj(fds) && (!fd_undef(fds.VMCallerId)) )
	{
		var msgVoiceMail = sprintf('You have Voice Mail Message from = %s\nWith MessageId [%s]', fds.VMCallerId, fds.VMMessageId);
		if (fd_length(msgVoiceMail)>0)
		{
			console.log(msgVoiceMail);
		}
	}
	
	return 0;
};

/*!
 * \brief  	ctiKawani.setThreadReport
 * \remark 
 * get Event handler from report method 
 *
 * \param 	{object:p} stream data from socket 
 * \retval  {
 *				Action: "99"
 *				Calling: "0"
 *				Disposition: "Info"
 *				EventGetVar: "Report"
 *				Process: "5"
 *				Ready: "0"
 *				Total: "5"
 *				Type: "PDS"
 *				Utilize: "0"
 *			}
 * 				
 */	 
 ctiKawani.prototype.setThreadReport = function(p)
{
	var tParam = p.fetch(); tSkill = window.ctiClient.getAgentSkill(), 
	tType = (!fd_undef(tParam.Type) ? tParam.Type : '');
	
	if (fd_isobj(tParam) &&(tType=== "PDS")) { 
		tParam.Skill = fd_sigint(tSkill); /** add object */
		ctiClient.setIntefaceThread ({
			event : fd_string("report"),
			body  : {
				type : fd_sigint(tParam.Action),
				data : tParam 
			}
		});
	} /* Auto Dialer Agent Outbound: New feature */
	else if (fd_isobj(tParam) &&(tType === "SAC")) { 
		tParam.Skill = fd_sigint(tSkill); /** add object */
		ctiClient.setIntefaceThread ({
			event : fd_string("report"),
			body  : {
				type : fd_sigint(tParam.Action),
				data : tParam 
			}
		});
	} return 0;
}

// ctiKawani.prototype.setThreadCallLater 
 ctiKawani.prototype.setThreadCallLater = function(p)
{
	if ( !fd_isobj(p)) return 0;
	else if (fd_isobj(p))
	{
		var tParam = p.fetch();  
		if (fd_isobj(tParam) &&(fd_sigint(tParam.CallLaterId)))
		{  
			tParam.Action = 8;
			ctiClient.setIntefaceThread
			({
				event : fd_string('callater'),
				body : {
					type : fd_sigint(tParam.Action),
					data : tParam 
				}
			});
		}
	}
	
	return 0;
}

// ctiKawani.setThreadSkill[object]
 ctiKawani.prototype.setThreadSkill = function(p)
{
	if (!fd_isobj(p)) 
	{
		return 0;
	}
	
	var tParam = p.fetch(), out = {};
	if (fd_isobj(tParam) &&(fd_sigint(tParam.Skill)>0))
	{
		if (tParam.EventGetVar == 'Info')
		{
			out.EventGetVar = tParam.EventGetVar;
			out.Disposition = tParam.Disposition;
			out.Response	= tParam.Response;
			out.AgentId		= tParam.AgentId;
			out.Extens		= tParam.Extens;	
			out.Skill 	  	= tParam.Skill;	
		}
		else if (tParam.EventGetVar == 'AgentState')
		{
			out.EventGetVar = 'Info';
			out.Disposition = 'Skill';
			out.Response	= 'succces';
			out.AgentId		= tParam.AgentId;
			out.Extens 	  	= tParam.Extens;	
			out.Skill 	  	= tParam.Skill;
		}
		
		ctiClient.setClientSession('ctiAgentSkill', fd_sigint(out.Skill));  out.Action = 9;  
		ctiClient.setIntefaceThread
		({
			event : fd_string('skill'),
			body : {
				type : fd_sigint(out.Action),
				data : out 
			}
		});
	}
	
	return 0;
} 

// ctiKawani.setThreadPing[object]
ctiKawani.prototype.setThreadPing = function(p)
{ 
	var fds = p.fetch();   
	if ( fd_undef(fds.clientId) )
	{
		var timeStamp 	= fd_string(fd_unixtime(fds.SockTime)),
			response 	= fd_string(fds.Response),
			disposition = fd_string(fds.Disposition),
			sockid  	= fd_string(fds.SockId),
			socktype 	= fd_string(fds.SockType),
			sockpid 	= fd_string(fds.SockPid),
			sockport 	= fd_string(fds.SockPort),
			sockhost 	= fd_string(fds.SockHost);
		
			writeloger(sprintf('Got MSG INFO = %s #PID = %s #MSG = %s',disposition, sockpid, response));
			writeloger(sprintf('Type= %s #Host = %s #Id= %s #Port= %s',socktype, sockhost, sockid, sockport));
			return 0;
	}

	if (fd_isobj(fds)&&fd_sigint(fds.clientId)>0)
	{
		var timeStamp = fd_unixtime(fds.clientTime), fd = {};
		if (typeof(window.interfaceIdleTimer) == 'function' )
		{
			window.interfaceIdleTimer.apply(this, [ctiClient] );	
		}
		
		writeloger(sprintf('Got MSG INFO = %s',fds.EventGetVar));
		writeloger(sprintf('Type= %s #Host = %s #Id= %s #Port= %s',fds.clientType, fds.clientHost, fds.clientId, fds.clientPort ));
		
		/*! \note replay sent Ping to the server */
		if ( fd_isfunc(ctiClient.setWebSocketPing)) 
		{
			ctiClient.setWebSocketPing();
		}
		
		fd.p1 = ctiClient.session.label.serverState;
		if (fd_isobj(fd.p1))
		{
			fd.p1.html('Now Connected.');
		}
	}  
	
	return 0;
};

/*! \brief ctiKawani.setIntefaceThread[object{[type: string ] [type: int ] [data: object ] }] */
 ctiKawani.prototype.setIntefaceThread = function(data)
{ 
	var fd = {}; fd.p0 = ctiClient.session['interface'];
	if (!fd_undef(fd.p0)&&fd_length(fd.p0)>3)
	{
		window[fd.p0].apply(this, [data] );
		return;
	}
	return;
};

/*! \brief ctiKawani.setThreadPing[object] */
 ctiKawani.prototype.setThreadCall = function(param)
{ 
	var p = param.fetch();
	if ( !fd_isobj(p) ) {
		return 0;
	} 
	/*! \remark new Revision for Handler Multiple Line Of Call */
	if ( fd_length(p.CallSessionId) )
	{
		if (typeof ctiClient.setClientAddContact === "function")
		{
			var cti_attr = {};
			for (var cti_var in p)
			{
				/*! \for 'CallCreateId' */
				if (cti_var === "CallCreateId") {
					cti_attr['CallCreateId'] = p.CallCreateId;
				}
					
				/*! \for 'CallSessionId' */
				if (cti_var === "CallSessionId") {
					cti_attr['CallSessionId'] = p.CallSessionId; 
				}
				
				/*! \for 'CallDirection' */
				if (cti_var === "CallDirection") {
					cti_attr['CallDirection'] = p.CallDirection;  
				}
				
				/*! \for 'CallerID' */
				if (cti_var === "CallerID") {
					cti_attr['CallerID'] = p.CallerID;   
				}
					
				/*! \for 'CallChannel' */
				
				if (cti_var === "CallChannel") {
					cti_attr['CallChannel1'] = p.CallChannel;
				}
				
				/*! \for 'CallerID' */
				if (cti_var === "CallChannel2") { 	 
					cti_attr['CallChannel2'] = p.CallChannel2;
				}
			}  
			ctiClient.setClientAddContact(p.CallSessionId, cti_attr);
			delete cti_attr;
		}
	}
	
	// add to list data local browser :
	if (fd_isobj(p)&&(typeof(p.CallIVRData)!='undefined' && fd_length(p.CallIVRData)>3))
	{
		ctiClient.setCallSession('CallIVRData', p.CallIVRData);
		if (callDataIVR = p.CallIVRData.split(";")) {
			if (typeof(callDataIVR) == 'object') for( var i in callDataIVR){ 
			  ctiClient.setCallIvrData(i,callDataIVR[i]);
			}
		}
	}	
	 
	if (fd_isobj(p)&&fd_length(p.CallStatus) > 0 ){
		ctiClient.setCallSession('CallStatus', p.CallStatus);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallerID) > 3 ){
		ctiClient.setCallSession('CallCallerId', p.CallerID);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallDirection)>0){
		ctiClient.setCallSession('CallDirection', p.CallDirection);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallCalling)>0){
		ctiClient.setCallSession('CallCalling', p.CallCaller);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallChannel)>0){
		ctiClient.setCallSession('CallChannel1', p.CallChannel);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallChannel2)>0){
		ctiClient.setCallSession('CallChannel2', p.CallChannel2);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallCreateId)>0){
		ctiClient.setCallSession('CallCreateId', p.CallCreateId);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallSessionId)>3){
		ctiClient.setCallSession('CallSessionId', p.CallSessionId);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallStatus)>3){
		ctiClient.setCallSession('CallStatus', p.CallStatus);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallTrunk)>3){
		ctiClient.setCallSession('CallTrunk', p.CallTrunk);
	}
	
	if (fd_isobj(p)&&fd_length(p.CallVDN)>3){
		ctiClient.setCallSession('CallVDN', p.CallVDN);
	} 
	
	if (fd_isobj(p)&&fd_length(p.CallAssignId)>0){
		ctiClient.setCallSession('CallAssignId', p.CallAssignId);
	} 
	
	/*! \remark  user delegate function to aksess */
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
	   ctiClient.setIntefaceThread
	   ({
			event : fd_string('callstate'),
			body  : {
			   type : fd_sigint(p.CallDirection),
			   data : p 
			}
		});
	} 
	return 0;
};



// ctiKawani.setThreadAgent[object]
ctiKawani.prototype.setThreadAgent = function(param)
{ 
	var s = param.fetch(), h;
	if (fd_isfunc(ctiClient.setIntefaceThread))
	{
		if (fd_isobj(s) && fd_length(s.Extens)>2)
		{
			ctiClient.setIntefaceThread
			({
				event : fd_string('agentstate'),
				body  : {
					type : fd_sigint(s.Value),
					data : s 
				}
			});
		}
	}
	
	/*! \remark get Paramater from Event Agent Handler */
	if (fd_isobj(s) && fd_length(s.Extens)>2 )
	{
		/*! \remark repsonse on agent register: */
		h = s.Value 
		if ((h == AST_AGENT_LOGOUT) &&(fd_isfunc(ctiClient.setAgentDisconected))){
			return 0;
		}
		/*! \remark repsonse on agent register: */ 
		else if ((h == AST_AGENT_REGISTER) &&(fd_isfunc(ctiClient.setAgentLogin)))
		{
			if (fd_isfunc(ctiClient.setThreadRegister)){
				ctiClient.setThreadRegister.apply(this, new Array(s));
			}
			return 0;
		}
		
		/*! \remark user login */
		else if((h == AST_AGENT_LOGIN) &&(fd_isfunc(ctiClient.setAgentReady)))
		{
			if(fd_isfunc(ctiClient.setThreadLogin)){
				ctiClient.setThreadLogin.apply(this, new Array(s));
			}
			return 0;
		}
		
		/*! \remark agent Ready: */
		else if((h == AST_AGENT_READY) &&(fd_isfunc(ctiClient.setAgentNotReady)))
		{
			ctiClient.setLocalPopup(0);
			ctiClient.setLocalAgentStatus(s.AgentId,{
				skill  : s.Skill,
				status : s.Value,
				result : s.Cause
			});	
			return 0;
		}
		
		/*! \remark Agent Notready / Aux : */
		else if((h == AST_AGENT_NOT_READY) &&(fd_isfunc(ctiClient.setAgentNotReady)))
		{
			ctiClient.setLocalPopup(0);
			ctiClient.setLocalAgentStatus(s.AgentId,{
				skill  : s.Skill,
				status : s.Value, 
				result : s.Cause
			});	
			return 0;
		}
		
		/*! \remark "Busy" */
		else if((h == AST_AGENT_BUSY) &&(fd_isfunc(ctiClient.setAgentNotReady)))
		{
			return 0;
		}
		
		/*! \remark "ACW" */
		else if((h == AST_AGENT_ACW) &&(fd_isfunc(ctiClient.setAgentLogin)))
		{
			ctiClient.setLocalPopup(0);
			return 0;
		}
	}
	
	return 0;
};


// ctiKawani.setThreadServer[string]
 ctiKawani.prototype.setThreadServer = function(message)
{
	/*! \remark default process on here */
	var p = ctiClient.setParseMessage(message), events = null, param = null, url = "";
	if (!fd_isobj(p)) 
	{
		return 0;
	} 
	/*! \remark Thread baru Untuk Utils  example utils server triger 
	from socket manager */
	events = p.get('EventGetVar'); param = p.fetch(); 
	// console.log(events);
	// console.log(param);
	if (events == 'Utils')
	{ 
		/*! \remark Event Handler on "Reload" from socket */
		if (fd_isobj(param) &&(param.UtilProcess == 'Reload')){
			document.location = document.location.href;	
			return 0;
		}
		
		/*! \remark Logout Handler If true Handler Then Call Url Logout */
		if (fd_isobj(param) &&(param.UtilProcess == 'Logout'))
		{
			url = Ext.EventUrl(['Auth', 'Logout']).Apply(); 
			ctiClient.setAgentLogout(AST_AGENT_LOGOUT, (s) => {
				window.localStorage.clear();
				document.location = url;  
	        });
		} 
		return 0;
		
	} 
	/*! \remark Thread baru Untuk ChatHearbeat: */
	else if (events == 'ChatHearbeat')
	{
		if (fd_isfunc(ctiClient.setThreadPing)){
			ctiClient.setThreadPing.apply(this, [p]);
		}
	}
	/*! \remark Thread baru Untuk VoiceMail: */
	else if (events == 'VoiceMail')
	{
		if (fd_isfunc(ctiClient.setThreadVoiceMail)){
			ctiClient.setThreadVoiceMail.apply(this, [p]);
		}
	}
	
	/*! \remark Thread.ctiClient.setThreadParkCall */
	else if (events == 'ParkCall')
	{
		if (fd_isfunc(ctiClient.setThreadParkCall)){
			ctiClient.setThreadParkCall.apply(this, [p]);
		}
	}
	
	/*! \remark  Thread.ctiClient.setThreadTransferCall */
	else if(events == 'CallTransfer')
	{
		if (fd_isfunc(ctiClient.setThreadTransferCall)){
			ctiClient.setThreadTransferCall.apply(this, [p]);
		}
	}
	/*! \remark  Thread.ctiClient.setThreadForwardCall */
	else if (events == 'CallForward')
	{
		if (fd_isfunc(ctiClient.setThreadForwardCall)){
			ctiClient.setThreadForwardCall.apply(this, [p]);
		}
	}
	/*! \remark  Thread.ctiClient.setThreadCallSplitIVR */
	else if (events == 'CallSplitIVR')
	{
		if (fd_isfunc(ctiClient.setThreadCallSplitIVR)){
			ctiClient.setThreadCallSplitIVR.apply(this, [p]);
		}
	}
	/*! \remark  track get Media Channel Like 'Mail, SMS, IVR, CALL, CHAT' */
	else if (events == 'MediaChannel')
	{
		if (fd_isfunc(ctiClient.setThreadMediaChannel)){
		   ctiClient.setThreadMediaChannel.apply(this, [p]);
		}
	}
	
	/*! \remark track get report */
	else if (events == 'Report')
	{
		if (fd_isfunc(ctiClient.setThreadReport)){
			ctiClient.setThreadReport.apply(this, [p]);
		}
	}
	/*! \remark track get informations */
	else if (events == 'Info')
	{
		/*! \remarksetThreadPing */
		if (!fd_undef(param.Disposition) &&(param.Disposition=='Chathearbeat'))
		{
			if (fd_isfunc(ctiClient.setThreadPing)){
				ctiClient.setThreadPing.apply(this, [p]);
			}
		}
		/*! \remark setThreadAlarm */
		else if (!fd_undef(param.Disposition) &&(param.Disposition=='Alarm'))
		{
			if (fd_isfunc(ctiClient.setThreadAlarm)){
				ctiClient.setThreadAlarm.apply(this,[p]);
			}
		}
		/*! \remark setThreadSkill */
		else if (!fd_undef(param.Disposition) &&(param.Disposition=='Skill'))
		{
			if (fd_isfunc(ctiClient.setThreadSkill)){
				ctiClient.setThreadSkill.apply(this, [p]);
			}
		}
		/*! \remark setThreadReport */
		else if (!fd_undef(param.Disposition) &&(param.Disposition=='Report'))
		{
			if (fd_isfunc(ctiClient.setThreadReport)){
				ctiClient.setThreadReport.apply(this, [p]);
			}
		}			   
		/*! \remark setThreadCallLater */
		else if (!fd_undef(param.Disposition) &&(param.Disposition=='CallLater'))
		{
			if (fd_isfunc(ctiClient.setThreadCallLater)){
				ctiClient.setThreadCallLater.apply(this, [p]);
			}
		}
		/*! \remark setThreadTimout */
		else if (!fd_undef(param.Disposition) &&(param.Disposition=='Timeout'))
		{
			if (fd_isfunc(ctiClient.setThreadTimout)){
				ctiClient.setThreadTimout.apply(this, [p]);
			}
		}
	}
	/*! \remark track on agent_state */
	else if (events == 'AgentState')
	{
		if (fd_isfunc(ctiClient.setThreadAgent)){
			ctiClient.setThreadState.apply(this, [p]);
			ctiClient.setThreadAgent.apply(this, [p]);
		}
	} 
	/*! \remark track on call_state */
	else if (events == 'CallState')
	{
		if (fd_isfunc(ctiClient.setThreadCall)){
			ctiClient.setThreadCallState.apply(this, new Array(param));
			ctiClient.setThreadCall.apply(this, [p]);
		}
	}
	/*! \remark track PeerStatus */
	else if (events == 'PeerStatus')
	{
		if (fd_isfunc(ctiClient.setThreadPeers)){
			ctiClient.setThreadPeers.apply(this, [p]);
		}
	}
	/*! \remark track "AgentHold" */
	else if (events == 'AgentHold')
	{ 
		if (fd_isfunc(ctiClient.setThreadHold)){
			ctiClient.setThreadHold.apply(this, [p]);
		}
	}
	/*! \remark track Bridge Call */
	else if (events == 'CallBridge'){
		if (fd_isfunc(ctiClient.setThreadCallBridge)){ 
			ctiClient.setThreadCallBridge.apply(this, [p]);
		}
	}
	
	/*! \remark track Ctxfer Call */
	else if (events == 'CallCtxfer'){
		if (fd_isfunc(ctiClient.setThreadCallCtxfer)){ 
			ctiClient.setThreadCallCtxfer.apply(this, [p]);
		}
	} 
	/*! \remark track "Recording" */
	else if (events == 'Recording')
	{ 
		if (fd_isfunc(ctiClient.setThreadRecording)){
			ctiClient.setThreadRecording.apply(this, [p]);
		}
	}
	/*! \remark track "SetAcwTimeout" */
	else if (events == 'SetAcwTimeout')
	{ 
		if (fd_isfunc(ctiClient.setThreadAcwTimeout)){
			ctiClient.setThreadAcwTimeout.apply(this, [p]);
		}
	}
	return 0;
};

// ctiKawani.setComposeMessage[string]
ctiKawani.prototype.setComposeMessage = function(fd, callback){
	var ptr = 0, buff = "", bytes = []; 
	if(!fd_isobj(fd)){
		return 0;
	} 
  /**
   * INFO: 20200825
   * Convert To Singgle String for easy process on backend and less data to efiesiensi
   * Buffered data Socket   
   */ 
	for(var i in fd ){
		bytes.push(sprintf('%s:%s',i,fd[i]));
		ptr+=1; // ptr+= sprintf("%s:%s\r\n",i,fd[i]); 
	} 
	// check validation process if empty back to NULL;
	if(ptr<1) return 0;
	else if(ptr>0){ 
		/** INFO :if user not deleagate function / callback  **/
		buff = fd_string(sprintf("%s\r\n", bytes.join('|'))); bytes = []; 	
		if(!fd_isfunc(callback)){
			if(fd_length(buff) == 0 ){
				return 0;
			}
			return buff;
		}
		// if user deleagatefunction callback 
		else if(fd_isfunc(callback)){
			callback.apply(this, new Array(buff, ctiClient));
			return 0;
		}
	}
	// reset data to null byte : 
	return;
	
};

// ctiKawani.setWriteMessage[string]
ctiKawani.prototype.setWriteMessage = function(message){
	var message = this.setStringToBinary(message);
	if (fd_isobj(window.ctiClient.onSocket)){
		window.ctiClient.onSocket.send(message);
	} return 0;
};

// ctiKawani.setWebSocketPrepare[void]
ctiKawani.prototype.setWebSocketPrepare = function( callback ){
  
 /*!
  * handleEvent for UniqueId Tab bro indetification 
  * every first open tab browser  application
  *
  */
  var ctiNavUniqueID = null;
  if (sessionStorage.getItem( 'ctiNavUniqueID' ) === null ){
	 window.sessionStorage.setItem( 'ctiNavUniqueID', window.strtotime());
  }
  
 /*! 
  * then sent signal to client process for next step 
  * check function callback from client.
  */
  if (typeof callback == 'function' ){
	ctiNavUniqueID = window.sessionStorage.getItem( 'ctiNavUniqueID' );
	if (ctiNavUniqueID){
		this.setClientSession( 'ctiNavUniqueID', ctiNavUniqueID );
		callback.apply(this, [window.ctiClient, ctiNavUniqueID]);
	}
	// retval == (string)
	return ctiNavUniqueID;
  }
 // retval == (string)
  return ctiNavUniqueID
}

// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketConnect = function(){
	this.onType  = this.session.server.type;
	this.onHost  = this.session.server.host;
	this.onPort  = this.session.server.port;
	this.onProxy = this.session.server.reverseProxy;
	this.onInfo  = this.session.label.serverState;
	this.onCall  = this.session.label.callState;
	this.onAgent = this.session.label.agentState;
	this.onExten = this.session.label.extenState;
	// wait if loost connection try to connected again */
	this.onWait  = 10000;
	if ( fd_sigint(window.WebSocketTimeout) > 0 ){ 
		this.onWait = window.WebSocketTimeout;
	}
	/*! Periksa Apakah ini lewat port atau bagaimana */
	if (window.location.hostname == this.onHost){
		writeloger("DOMExeptions(1)");
		/*! Ini mengindikasikan Client terkoneksi di server yang sama */
		this.onUrl = window.sprintf('%s://%s:%s/cti-wss', this.onType, this.onHost, this.onPort);
	} else if (window.location.hostname != this.onHost){
		writeloger("DOMExeptions(2)");
		/*! Ini mengindikasikan client connect ke Server A -> forward -> server B */
		this.onHost = window.location.hostname; /*! change of hostname */ 
		this.onUrl  = window.sprintf('%s://%s/cti-wss', this.onType, this.onHost);
		/*! If Not use service Reverese Proxy */
		if (!this.onProxy){
			writeloger("DOMExeptions(3)");
			this.onUrl  = window.sprintf('%s://%s:%s/cti-wss', this.onType, this.onHost, this.onPort);	
		}  
	} /*! dump process request to websocket */
	writeloger(sprintf("Connecting to Server= %s", this.onUrl));
	if ( typeof(this.onUrl) != 'string'){
		return 0;
	} /* open connection to webSocket server */
	try{
		if (typeof MozWebSocket === "function") this.onSocket = new MozWebSocket(this.onUrl);
		else {
		   this.onSocket = new WebSocket(this.onUrl);
		} /* detected for get browser type */
		this.userAgent = navigator.userAgent;
		if ( fd_isfunc(window.detectBrowser) ){
			this.userAgent = window.detectBrowser();
		} /* masukan variable Browser */
		if ( fd_isfunc(ctiClient.setClientSession) ){
			ctiClient.setClientSession('ctiNavigator', this.userAgent);
		} 
	   /*! \brief EVENT: onopen
	    *  \details interface[ctiClient.setConnectServer]
		*  scope local variable for this session
		*/
		let websock = this; /*__constructor */
		this.onSocket.onopen = function(evt){  
			writeloger(sprintf('INFO: %s', 'Initialize open ws:socket'));
			writeloger(sprintf('INFO: Browser Type: %s', websock.userAgent));
			
			if ( fd_isobj(evt) &&( fd_isfunc(ctiClient.setConnectServer) )){
				if ( fd_isfunc(window.clearTimeout)){
					window.clearTimeout(window.WebSocketInterval);
					websock.onReady = true;
				} /* then back connect && detected for Chrome Browser */
				websock.evensTarget= {};
				ctiClient.setClientSession('ctiClientTimeout',0);
				if ( !websock.userAgent.localeCompare('CH') ){
					websock.evensTarget = evt.target;
				} /* detected Browser Firefox */
				if ( !websock.userAgent.localeCompare('FF') ){
					websock.evensTarget = evt.originalTarget;
				} /* sent to Interface */
				ctiClient.setConnectServer.apply(this, new Array(1, websock.evensTarget));
				return 0;
			}	
		}
		
		// EVENT: onmessage
		// interface[ctiClient.setThreadServer]
		this.onSocket.onmessage = function(evt){
			writeloger(sprintf('INFO: Initialize message ws:socket'));
			if ( fd_isfunc(ctiClient.setThreadServer) ){
				ctiClient.setThreadServer.apply(this, new Array(evt.data));
				return 0;
			}	
		}
		
		// EVENT: onclose
		this.onSocket.onclose = function(evt){ 
			writeloger('INFO: Initialize close ws:socket');
			
			// check if register rejected;
			websock.onReject = fd_sigint( websock.onReject );
			if ( websock.onReject > 0 ) {
				return 0;
			}
			
			// stop break for rejected state 
			if ( websock.onReject <1 ) {
				var p1 = websock.onInfo,
					p2 = websock.onCall,
					p3 = websock.onAgent;
					
			   /**	
			    * INFO: 20201229
				*
			    * jika User membuka banyak tab dalam satu screen Browser maka 
			    * socket yang di anggap active hanya pada "New tab" , dan yang lain akan di 
			    * matikan 
				* 
				*/ 
				let _timeout = ctiClient.getClientSession('ctiClientTimeout'); 
				if ( fd_sigint( _timeout ) > 0 ){
					p1.html('');
					p2.html('-- INFO: Expired Session');
					p3.html('-- WARNING: CTI Disconected');
					
					
					// set On Thread Error 
					if( fd_isfunc(ctiClient.setThreadError) ){
					   ctiClient.setThreadError({
						 event : 'ctierror',  
						 type : 0,
						 body : {
							header  : 'CTI Disconected',
							message : 'Expired Session'	
						 }
					   }); 
					}
					// finally return 
					return 0;
				}
				
				if ( fd_isobj(p1) ){
					writeloger('INFO: Can\'t establish a Connection to the CTI');
					p1.html('-- INFO -- Can\'t establish a Connection to the CTI ');
				}
				if ( fd_isobj(p2) ){
					writeloger('INFO: Can\'t establish a Connection to the CTI (10) s');
					p2.html('-- INFO -- Can\'t establish a Connection to the CTI (10) s');
				}
				if ( fd_isobj(p3) ){
					writeloger('WARNING: CTI Disconected');
					p3.html('-- WARNING -- CTI Disconected.');
				}
				
				
				// set On Thread Error 
				if ( fd_isfunc(websock.setThreadError) ){
					websock.setThreadError({
						event : 'ctierror',  
						type  : 1,
						body : {
							header  : 'CTI Disconected',
							message : 'Can\'t establish a Connection to the CTI'	
						}
					}); 
				}	
				
				// try connect to the server .
				window.WebSocketInterval = window.setTimeout( function(){
					websock.onReady = false;
					if ( !websock.onReady ) {
						writeloger('INFO: Wait timout 10s to reconnecting ...');
					}
					ctiClient.setWebSocketReconnect(websock);
				}, websock.onWait); /*!< 10 seconds */ 
				return 0;
			}
		}
		
		// EVENT: onerror
		this.onSocket.onerror = function(evt){
			writeloger('INFO: Initialize error ws:socket');  /*!< write loger actived  */
			let p0 = websock.onInfo;  /*!< Local scope variable  */
			if ( fd_isobj(p0) ){
				p0.html('CTI Disconected'); 
				if ( fd_isfunc(websock.setThreadError) ){
					websock.setThreadError({
						event : 'ctierror',  
						type : 2,
						body : {
							header  : 'CTI Disconected',
							message : 'Initialize Error WS:Socket'	
						}
					});
				}
			}
		}
	}
	
	/*!< catch Error not definitively  */
	catch( error ){
		writeloger(error);
		return 0
	}
	return 0;
};

/*! \brief ctiKawani.setWriteMessage[string] */
ctiKawani.prototype.setWebSocketReconnect = function(webSocket){
	/*! INFO : clean session Key if disconected with server  */
	ctiClient.setClientSession("ctiAgentKey", "");
	if (fd_isobj(webSocket.onInfo)){
		writeloger("INFO: ReConnecting to the CTI ...");
		webSocket.onInfo.html("-- INFO -- Re Connecting to the CTI...");
		
	} if (fd_isobj(webSocket.onAgent)){
		writeloger("INFO: CTI Disconected.");
		webSocket.onAgent.html("-- WARNING -- CTI Disconected");
		
	} if (fd_isobj(webSocket.onCall)){
		writeloger("NOTICE: ReConnect to CTI...");
		webSocket.onCall.html("-- INFO -- Re Connect...");
		
	} /* set error from socket cti */
	if (typeof ctiClient.setThreadError === "function"){
		ctiClient.setThreadError({
			event : "ctierror",  
			type : 3,
			body : {
				header  : "CTI Disconected",
				message : "Trying Connecting to the CTI"	
			}
		});
	} /* detect socket */
	if (fd_isobj(ctiClient)&&fd_isfunc(ctiClient.setWebSocketConnect)){
		ctiClient.setWebSocketConnect();
	} return 0;	
};

// ctiKawani.setConnect[void]
ctiKawani.prototype.setWebSocketDisconect = function(timeout){
	if( fd_undef(timeout)){
	    var timeout = 0;
	}
	ctiClient.setClientSession('ctiClientTimeout',timeout);
	if( window.ctiClient.onSocket !== null &&(fd_isobj(window.ctiClient.onSocket))){
		ctiClient.onSocket.close();
	}
	return 0;
};
 
// ctiKawani.setClientPing[string, string]
ctiKawani.prototype.setWebSocketPing = function(){ 
	var 
	  ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	  ctiAgentExten = ctiClient.getClientSession('ctiAgentExten'),
	  ctiAgentHost  = ctiClient.getClientSession('ctiAgentHost'),
	  ctiAgentEvent = 4;
	  
   // -- VALIDATION : no have data to call be terminate. 
	if(fd_undef(ctiAgentExten)){
		return 0;
	}
  
   // -- VALIDATION : cannot be empty dialing number 
	if(fd_length(ctiAgentHost)<3){
		return 0;
	}
	
	// set Tiem Over Internet client for check Network Quality :
	var ctiReqTime = window.microtime(false);
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = ctiAgentEvent;
		fd.p2 = ctiAgentExten;
		fd.p3 = ctiAgentHost;
		fd.p4 = ctiReqTime;
		
	var param ={
			Action  : fd_string(fd.p0),
			Event   : fd_string(fd.p1),
			Extens  : fd_string(fd.p2),
			Address : fd_string(fd.p3),
			Timing  : fd_string(fd.p4)
		};
	ctiClient.setComposeMessage(param, function( lenBuffer, ctiClient ){
		if(fd_length(lenBuffer)>3){
			ctiClient.setWriteMessage(lenBuffer);	
		}
	}); 
	return 0;
}
// ctiKawani.setAgentDisconected[void]
ctiKawani.prototype.setAgentDisconected = function(){ 
  ctiClient.setWriteMessage('quit');
  return 0;
};

/*! \remark ctiKawani.setAgentRegister[void] */
 ctiKawani.prototype.setAgentRegister = function(callback)
{
	var fd = {}, param = {}; // defined header 
	
	/*! \remark get from session */
	fd.p1 = 'AgentState';
	fd.p2 = AST_AGENT_REGISTER;
	fd.p3 = ctiClient.getClientSession('ctiAgentExten');
	fd.p4 = ctiClient.getClientSession('ctiAgentHost');
	fd.p5 = ctiClient.getClientSession('ctiAgentId');
	fd.p6 = ctiClient.getClientSession('ctiNavigator');
	fd.p7 = ctiClient.getClientSession('ctiNavUniqueID');
	
	/*! \remark add new param 	to post data */
	param.Action = fd_string(fd.p1);
	param.Event = fd_string(fd.p2);
	param.Extens = fd_string(fd.p3);
	param.AgentId = fd_string(fd.p5);
	param.Address = fd_string(fd.p4);
	param.Navigator = fd_string(fd.p6);
	param.UniqueId = fd_string(fd.p7);
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		try{
			if (fd_length(buf)>3){
				s.setWriteMessage(buf);	 
				/*! \remark check callbac method */
				if (typeof callback === "function" ){
					callback.apply(this, [s, buf]);
					return 0;
				}
			}
		}
		/*! \remark catch error reverse */
		catch(e){
			if (typeof callback === "function" ){
				callback.apply(this, [e, false]);
				return 0;
			}
		}
	}); 
	return 0;
};

// ctiKawani.setAgentLogout[void]
ctiKawani.prototype.setAgentLogout = function(agentLogout, callback){ 
	if(fd_undef(agentLogout)){
		agentLogout = 0;
	}
	var fd = {};
		fd.p1 = 'AgentState';
		fd.p2 = agentLogout;
		fd.p3 = ctiClient.getClientSession('ctiAgentKey');
		fd.p4 = ctiClient.getClientSession('ctiAgentHost');
		fd.p5 = ctiClient.getClientSession('ctiAgentExten');
		fd.p6 = ctiClient.getClientSession('ctiAgentName');
		fd.p7 = ctiClient.getClientSession('ctiAgentPass');
		fd.p8 = ctiClient.getClientSession('ctiAgentId');
		fd.p9 = 'KCA'; 
	var 
		lenBuffer = ctiClient.setComposeMessage({
			Action : fd_string(fd.p1),
			Event : fd_string(fd.p2),
			Extens : fd_string(fd.p5),
			Address : fd_string(fd.p4),
			AgentName : fd_string(fd.p6), 
			AgentId : fd_string(fd.p8),
			AgentService: fd_string(fd.p9)
	});
	
	
	// -- {VALIDATION} -- process sent message ''
	if (fd_length(lenBuffer) >3 )
	{
		try { ctiClient.setWriteMessage(lenBuffer);	 }
		catch(error){
			console.log("Error 'ctiKawani.prototype.setAgentLogout()'");
		}
	}
	
	if (fd_isfunc(callback))
	{
		callback.apply(this, [ctiClient]);
	}
	
	return 0;
};

/*! \remark ctiKawani.setAgentLogin[void] */
 ctiKawani.prototype.setAgentLogin = function(callback)
{
	var fd = {}, param = {};
	
	fd.p1 = 'AgentState';
	fd.p2 = AST_AGENT_LOGIN;
	fd.p3 = ctiClient.getClientSession('ctiAgentKey');
	fd.p4 = ctiClient.getClientSession('ctiAgentHost');
	fd.p5 = ctiClient.getClientSession('ctiAgentExten');
	fd.p6 = ctiClient.getClientSession('ctiAgentName');
	fd.p7 = ctiClient.getClientSession('ctiAgentPass');
	fd.p8 = ctiClient.getClientSession('ctiAgentId');
	
	
	/*! \remark create object to sent  */
	param = {
		Action : fd_string(fd.p1),
		Event : fd_string(fd.p2),
		Extens : fd_string(fd.p5),
		Address : fd_string(fd.p4),
		AgentName : fd_string(fd.p6),
		AgentPwd : fd_string(fd.p7),
		//AgentKey : fd_string(fd.p3),
		AgentId : fd_string(fd.p8)
	}; 
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		try 
		{
			if (fd_length(buf)>3) {
				s.setWriteMessage(buf);	
				if (typeof callback === "function"){
					callback.apply(this, [s, buf]);	
					return 0;
				}
			}
		}
		/*! \remark track error callback */
		catch(e){
			if (typeof callback === "function"){
				callback.apply(this, [e]);	
			}
		}
	});  
	return 0;
};

// ctiKawani.setAgentReady[void]
 ctiKawani.prototype.setAgentReady = function(callback)
{
	var fd = {}, param = {}; 
	
	fd.p0 = 'AgentState';
	fd.p1 = AST_AGENT_READY;
	fd.p2 = ctiClient.getClientSession('ctiAgentKey');
	fd.p3 = ctiClient.getClientSession('ctiAgentHost');
	fd.p4 = ctiClient.getClientSession('ctiAgentExten');
	fd.p5 = ctiClient.getClientSession('ctiAgentName');
	fd.p6 = ctiClient.getClientSession('ctiAgentPass');
	fd.p7 = ctiClient.getClientSession('ctiAgentId');
	
	param = {
		Action	  : fd_string(fd.p0),
		Event	  : fd_string(fd.p1),
		Extens	  : fd_string(fd.p4),
		Address	  : fd_string(fd.p3),
		AgentName : fd_string(fd.p5),
		AgentId	  : fd_string(fd.p7)
	};
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3){
			s.setWriteMessage(buf);	
			if (typeof callback === "function"){
				callback.apply(this, [s, buf]);	
			}
		}
	}); 
	
	return 0;
	 
};
// ctiKawani.setAgentNotReady[int]
ctiKawani.prototype.setAgentNotReady = function( ctiReasonId ){ 
	var fd = {};
		if (fd_undef(ctiReasonId)){
			ctiReasonId = 0;
		}
		/*! \remark jika reason is empty */
		if (ctiReasonId == '')
		{
			ctiReasonId = 0;
		}
		
		// set all parameter :
		fd.p0 = 'AgentState';
		fd.p1 = AST_AGENT_NOT_READY;
		fd.p2 = ctiClient.getClientSession('ctiAgentKey');
		fd.p3 = ctiClient.getClientSession('ctiAgentHost');
		fd.p4 = ctiClient.getClientSession('ctiAgentExten');
		fd.p5 = ctiClient.getClientSession('ctiAgentName');
		fd.p6 = ctiClient.getClientSession('ctiAgentPass');
		fd.p7 = ctiClient.getClientSession('ctiAgentId');
		fd.p8 = ctiReasonId;
		
	var param = {		  
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		ReasonId : fd_string(fd.p8),	
		Extens : fd_string(fd.p4),
		Address : fd_string(fd.p3),
		AgentName : fd_string(fd.p5),
		AgentId : fd_string(fd.p7)
	}; 
	 
	
	// put on localStorage:
	ctiClient.setLocalAgentStatus(fd.p7, {skill:0, status: fd.p1, reason:fd.p8, result: '' }); 
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3) {
			s.setWriteMessage(buf);	
		}
	}); 
	return 0;
};

/*! \brief window.ctiClient.setAgentAbsoluteTimeout 
 *  \param integer [direction] call type inbound, outbound, predictive
 *  \param integer [timeout] to setting request 
 *  \param integer [active] 0 = Unactive 1= active 
 *  \param string  [extension] agent login on extension 
 *  \retval mixed callback function 
 *  
 */
ctiKawani.prototype.setAgentAbsoluteTimeout = function(objects, callback){
	var param = {}; /* new object */
	/* set default for direction */
	if (fd_undef(objects.direction)){
		objects.direction = 0;
	} /* set default for timeout */
	if (fd_undef(objects.timeout)){
		objects.timeout = 0;
	} /* Set Or Remove */
	if (fd_undef(objects.active)){
		objects.active = 0; /* not active */
	} /* set default for exten */
	if (fd_undef(objects.extension)){
		objects.extension = ctiClient.getClientSession('ctiAgentExten');
	} /* check length of extension */
	if (!fd_length(objects.extension)){
		objects.extension = ctiClient.getClientSession('ctiAgentExten');
	} /* set all parameter  */
	param.Action = "SetAbsTimeout";
	param.Exten = fd_string(objects.extension);
	param.Direction = fd_sigint(objects.direction);
	param.Timeout = fd_sigint(objects.timeout);
	param.Active = fd_sigint(objects.active); 
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3) {
			s.setWriteMessage(buf); /* then if succces sent message */
			if (typeof callback === "function"){
				callback.apply(this,[s, param]);
			}
		}
	}); return 0;
}


// window.ctiClient.setAgentAcwTime
ctiKawani.prototype.setAgentAcwTime = function(ctiMaxTimeout, callback){
	var fd = {};
		if (fd_undef(ctiMaxTimeout)){
			ctiMaxTimeout = 0;
		}
		/*! \remark jika reason is empty */
		if (ctiMaxTimeout == '')
		{
			ctiMaxTimeout = 0;
		}
		
		// set all parameter :
		fd.p0 = 'SetAcwTimeout';
		fd.p1 = ctiClient.getClientSession('ctiAgentExten');
		fd.p2 = ctiClient.getClientSession('ctiAgentName'); 
		fd.p3 = ctiClient.getClientSession('ctiAgentId');
		fd.p4 = ctiMaxTimeout;
		
	var param = {		  
		Action 	  : fd_string(fd.p0), 
		Exten 	  : fd_string(fd.p1),
		AgentName : fd_string(fd.p2),
		AgentId   : fd_string(fd.p3),
		Timeout   : fd_string(fd.p4)
	}; 
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3) {
			s.setWriteMessage(buf);	
			/* then if succces sent message */
			if (typeof callback === "function"){
				callback.apply(this,[s, param]);
			}
		}
	}); 
	return 0;
}

/*! \brief ctiKawani.setAgentSpying
 *  \details  listen and whisper te spesific channel 
 *  \param from extension to forward  
 *  \param to extension to listener 
 */ 
ctiKawani.prototype.setAgentSpying = function(from, to){ 

	/* set Global parameter from request of agents */
	if (fd_undef(to)){
		to = ctiClient.getClientSession('ctiAgentExten');
	} /* No channel to listen */ 
	if (fd_undef(from)){
		return 0;
	} /* make lokal objects */
	var fd = {};
		fd.p0 = 'AstTool'; /* name of service */
		fd.p1 = 1; /* evensTarget */
		fd.p2 = to; /* local channel: yang akan menerima spy */
		fd.p3 = from; /* remote channel: yang akan di spy  */ 
		
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,(buf, s) => {
		if (fd_length(buf)>3){
			s.setWriteMessage(buf);	
		}
	});
	return 0; 
};


/*! \brief ctiKawani.setAgentCoaching
 *  \details  listen, whisper, talk to spesific channel 
 *  \param from extension to forward  
 *  \param to extension to listener 
 */ 
ctiKawani.prototype.setAgentCoaching = function(from, to){ 

	/* set Global parameter from request of agents */
	if (fd_undef(to)){
		to = ctiClient.getClientSession('ctiAgentExten');
	} /* No channel to listen */ 
	if (fd_undef(from)){
		return 0;
	} /* make lokal objects */
	var fd = {};
		fd.p0 = 'AstTool'; /* name of service */
		fd.p1 = 2; /* evensTarget */
		fd.p2 = to; /* local channel: yang akan menerima spy */
		fd.p3 = from; /* remote channel: yang akan di spy  */ 
		
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,(buf, s) => {
		if (fd_length(buf)>3){
			s.setWriteMessage(buf);	
		}
	});
	return 0; 
}; 

/*! \brief ctiKawani.setAgentBarge
 *  \details  listen, whisper, talk to spesific channel 
 *  \param from extension to forward  
 *  \param to extension to listener 
 */ 
 ctiKawani.prototype.setAgentBarge = function(from, to)
{
	/* set Global parameter from request of agents */
	if (fd_undef(to)){
		to = ctiClient.getClientSession('ctiAgentExten');
	} /* No channel to listen */ 
	if (fd_undef(from)){
		return 0;
	} /* make lokal objects */
	var fd = {};
		fd.p0 = 'AstTool'; /* name of service */
		fd.p1 = 10; /* evensTarget */
		fd.p2 = to; /* local channel: yang akan menerima spy */
		fd.p3 = from; /* remote channel: yang akan di spy  */ 
		
	var param = {
		Action : fd_string(fd.p0),
		Event : fd_string(fd.p1),
		Extens : fd_string(fd.p2),
		Destination : fd_string(fd.p3)
	};
	ctiClient.setComposeMessage(param,(buf, s) => {
		if (fd_length(buf)>3){
			s.setWriteMessage(buf);	
		}
	});
	return 0;  
} 

// ctiKawani.setAgentSplitCall[int]
ctiKawani.prototype.setAgentSplitCall = function(){
    // then if add 
	var fd = {};
	fd.p0 = 'CallSplitIVR';
	fd.p1 = ctiClient.getCallSessionId();
	fd.p2 = ctiClient.getClientSession('ctiAgentExten'); 
	
	if(fd_length(fd.p1)<2){
		return 0;
	}
	// send data ok 
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf) > 3){
			s.setWriteMessage(lenBuffer);	
		}
	});	
	return 0;
	
};

// ctiKawani.setAgentSkill[int]
ctiKawani.prototype.setAgentSkill = function(Skill){
	if(fd_undef(Skill)){
		Skill = 1;
	}
	// jika kosong :
	if( Skill == '' ){
		return 0;
	}
	var fd = {};
		fd.p0 = 'AstTool';
		fd.p1 = 9;
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = ctiClient.getClientSession('ctiAgentId');
		fd.p4 = Skill;
		
	var param = {
			Action 	: fd_string(fd.p0),
			Event 	: fd_string(fd.p1),
			Extens 	: fd_string(fd.p2),
			AgentId : fd_string(fd.p3),
			Skill	: fd_string(fd.p4)
		}
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf) > 3){ 
			s.setWriteMessage(buf);	
		}
	});	
	return 0;	
}

// ctiKawani.setCallSurvey[int]::CurrentCall 
ctiKawani.prototype.setCallSurvey = function(mCallbacks){  
											 
	/** 
		Action : CallSurvey,
		CallSessionId : 1
		Destination : SIP/4001 
	 */
	var fd = {};
		fd.p0 = 'CallSurvey';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
							
													   
	// then set object parameter ""
	var param = {
		Action : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating : fd_string(fd.p2)
	}
	
	ctiClient.setComposeMessage(param, function(buf, s)
	{
		if (fd_length(buf)>3)
		{
			s.setWriteMessage( buf );	
			// if function callbacks exist will return; 
			if( typeof(mCallbacks) == 'function'){
				mCallbacks.apply(this, new Array(s, buf));
				return 0;
			}
		}
	});	
	return 0;
};

// ctiKawani.setRetriveCall  = Ambil Call Yang di Parking Untuk di Transfer 
// Untuk di simpan Sementara Waktu yang Kemudian Akan di Transfer Kembali . 
 ctiKawani.prototype.setRetriveCall = function(mCallbacks)
{
	var fd = {};
		fd.p0 = 'RetriveCall';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		
	var param = 
	{
		Action 		  : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating   : fd_string(fd.p2) 
	};
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf) > 3) { 
			s.setWriteMessage(buf);	
			if (typeof mCallbacks === "function") {
				mCallbacks.apply(this, [buf]);
				return 0;
			}
		}
	});	
	
	return 0;
}

// ctiKawani.setParkingCall  = Tujuannya Adalah call di simpan terlebih dahulu Kebucket USer 
// Untuk di simpan Sementara Waktu yang Kemudian Akan di Transfer Kembali . 

 ctiKawani.prototype.setParkingCall = function(mCallbacks)
{
	var fd = {};
		fd.p0 = 'ParkingCall';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		
	var param = 
	{
		Action 		  : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating   : fd_string(fd.p2) 
	};
	
	ctiClient.setComposeMessage(param, (lenBuffer, ctiClient) => {
		if (fd_length(lenBuffer)>3)
		{
			ctiClient.setWriteMessage(lenBuffer);	
			if (fd_isfunc(mCallbacks))
			{
				mCallbacks.apply(this,[lenBuffer]);
				return 0;
			}
		}
	});	
	
	return 0;
}

// ctiKawani.setCallTransfer =  Blind Transfer Call Tapa Melakukan Verifikasi terelebih  
// danulu ke No tujuan yang akan di Transfer  

 ctiKawani.prototype.setCallForward = function(destination, callgroup)
{
	if (typeof(callgroup)!= 'undefined' &&(callgroup.length<4)) callgroup =  '3000'; // default Group;
	else if (typeof(callgroup) == 'undefined')
	{
	   callgroup = '3000';
	}
	
	// check phone number 
	if ( fd_length(destination)<1)
	{
		console.log('invalid extension');
		return 0;
	}
	
	var fd = {};
		fd.p0 = 'CallForward';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		fd.p4 = callgroup;
		
	var param = 
	{
		Action		  : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating   : fd_string(fd.p2),
		Destination   : fd_string(fd.p3),
		CallGroup     : fd_string(fd.p4)   
	};
	
	ctiClient.setComposeMessage(param, (lenBuffer, ctiClient) => {
		if (fd_length(lenBuffer)>3)
		{
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	
	return 0;
}; 

// ctiKawani.setCallTransfer =  Blind Transfer Call Tapa Melakukan Verifikasi terelebih 
// danulu ke No tujuan yang akan di Transfer 

 ctiKawani.prototype.setCallTransfer = function(destination, callgroup)
{
	if (typeof(callgroup)!= 'undefined' &&(callgroup.length<4)) callgroup =  '3000'; // default Group;
	else if (typeof(callgroup) == 'undefined')
	{
	   callgroup = '3000';
	}
	
	
	// check phone number 
	if ( fd_length(destination)<1)
	{
		console.log('invalid extension');
		return 0;
	}
	
	var fd = {};
		fd.p0 = 'CallTransfer';
		fd.p1 = ctiClient.getCallSessionId();
		fd.p2 = ctiClient.getClientSession('ctiAgentExten');
		fd.p3 = destination;
		fd.p4 = callgroup;
		
	var param = 
	{
		Action		  : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Originating   : fd_string(fd.p2),
		Destination   : fd_string(fd.p3),
		CallGroup     : fd_string(fd.p4)   
	};
	
	ctiClient.setComposeMessage(param, (lenBuffer, ctiClient) => {
		if (fd_length(lenBuffer)>3)
		{
			ctiClient.setWriteMessage(lenBuffer);	
		}
	});	
	
	return 0;
}; 

/*! \brief setCallBackToIVR 
 *  \details sent call to back IVR process like of transfer
 *  \param no description 
 *  \retval mixed return value
 */ 
 ctiKawani.prototype.setCallBackToIVR = function(mCallback)
{	
	// /* Make New Parameter */
	// var CallSessionId = ctiClient.getCallSessionId();
	// var Originating = ctiClient.getClientSession('ctiAgentExten');
	
	/* empty call session */
	var ctivar = {
		error : 0,
		message	: "succces",
		data : {
			CallSessionId : ctiClient.getCallSessionId(),
			Originating : ctiClient.getClientSession('ctiAgentExten'),
		}
	}; /* not empty callsession */
	if (ctivar.data.CallSessionId === ""){
		ctivar.error = 1;
		ctivar.message = "empty callsession";
		if (typeof mCallback === "function"){
			mCallback.apply(this, [ctiClient, ctivar]);
		} return 0; 
	} /* validation call session */
	if (fd_length(ctivar.data.CallSessionId) < 3){
		ctivar.error = 1;
		ctivar.message = "invalid callsession"; 
		if (typeof mCallback === "function"){
			mCallback.apply(this, [ctiClient, ctivar]);
		} return 0;
	} /* next to process */
	var param  = {
		Action : "CallToIVR",
		CallSessionId : ctivar.data.CallSessionId,
		Originating : ctivar.data.Originating
	}; /* sent message to server */
	ctiClient.setComposeMessage(param, (buf, o) => {
		if (fd_length(buf)>3) {
			o.setWriteMessage(buf);	
			if (typeof mCallback === "function"){
				mCallback.apply(this, [o, ctivar]);
			}
		}
	});	
	return 0; 
}; 

// ctiKawani.setCallSplitToIVR([agentTarget:string, [custTarget:string, [assignId:string, [callback:interface ]]]]])
 ctiKawani.prototype.setCallSplitToIVR = function(agentTarget, custTarget, assignId, mCallback)
{
	// check validation: agentTarget
	if (fd_length(agentTarget)<1)
	{
	   writeloger('INFO: Invalid agentTarget');
	   return 0;
	}
	
	// check validation: custTarget
	if (fd_length(custTarget)<1)
	{
	   writeloger('INFO: Invalid custTarget');
	   return 0;
	}
	
	// check validation: assignId
	if (fd_length(assignId)<1)
	{
	   writeloger('INFO: Invalid assignId');
	   return 0;
	}
	
	// check data porposional 
	var callAction = 'CallSplitIVR',
		callSessionId = ctiClient.getCallSessionId(),
		callExten = ctiClient.getClientSession('ctiAgentExten');
		
	// check fd_session 	
	var fd = {};
		fd.p0 = callAction;
		fd.p1 = callSessionId;
		fd.p2 = callExten;
		fd.p3 = agentTarget;
		fd.p4 = custTarget;
		fd.p5 = assignId
		
	// check param 	
	var param = 
	{
		Action 		  : fd_string(fd.p0),
		CallSessionId : fd_string(fd.p1),
		Extension 	  : fd_string(fd.p2),	
		AgentTarget   : fd_string(fd.p3),
		CustTarget 	  : fd_string(fd.p4),
		AssignId 	  : fd_string(fd.p5)
	};
	
	// check cti client <process> :	
	ctiClient.setComposeMessage(param, (buf, s) => { 
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if ( fd_isfunc(mCallback) )
			{
				mCallback.apply(this, [s, buf]);
				return 0;
			}
		}
	});	
	
	return 0;
};			

// ctiKawani.setRegisterIP[void]
 ctiKawani.prototype.setRegisterIP = function(Address, Extens, PbxId, Type)
{
	if (fd_length(Address)<1)
	{
		return 0;
	}
	
	// default PBX ID 
	if (fd_undef(PbxId))
	{
		PbxId = window.sprintf('%s', '1');
	}
	
	if (fd_undef(Type))
	{
		Type = window.sprintf('%s', '0');
	}
	
	// then set data event:
	var Event = window.sprintf('%s', '3'),
		param = 
	{
		Action  : 'AstTool',
		Event 	: Event,
		Extens  : Extens,
		PbxId 	: PbxId,
		Type 	: Type,
		Address : Address
	};
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3) 
		{
			s.setWriteMessage(buf);	
		}
	});	
	
	return 0;
};

/*! \brief ctiKawani.setCallHangup[callback]:: tambahan untuk triger di sisi client 
 * jika ada request callback : 
 *
 */
 ctiKawani.prototype.setCallHangup = function(mCallbacks)
{
	var fd = {};
		fd.p0 = 'CallHangup';
		fd.p1 = ctiClient.getClientSession('ctiAgentExten');
		fd.p2 = ctiClient.getCallerSession('CallSessionId');
		
	var param = 
	{
		Action : fd_string(fd.p0),
		Extens : fd_string(fd.p1),
		CallSessionId : fd_string(fd.p2) 
	};
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks))
			{
				mCallbacks.apply(this, [s, buf]);
			}
		}
	});
	
	return 0;
}; 

/*! 
 * \brief ctiKawani.setCallCtxfer[string, string] 
 * \note  Call Yang di Lakukan Ketika terjadi Busy Tujuannya Untuk Process Brideg Call 
 */
 ctiKawani.prototype.setCallCtxfer = function(Called, mCallbacks)
{
	var agentAct = "CallCtxfer",
		agentExt = ctiClient.getClientSession('ctiAgentExten'), 
		param = 
		{
			Action : fd_string(agentAct),
			Exten  : fd_string(agentExt),
			Called : fd_string(Called)
		};
		
		/*! \remark validation of dialString */
		if (param.Called === "" ){
			var buf  = "empty dial number.";
			if (fd_isfunc(mCallbacks)) {
				mCallbacks.apply(this, [ctiClient, "empty dial number"]);
			}
			return 0;
		}
		
		ctiClient.setComposeMessage(param, (buf, s) => {
			if (fd_length(buf)>3)
			{
				s.setWriteMessage(buf);	
				if (fd_isfunc(mCallbacks))
				{
					mCallbacks.apply(this, [s, buf]);
				}
			}
		});  
		return 0;
}
/*! 
 * \brief ctiKawani.setCallBridge[string, string] 
 * \note  Bridge two channels already in the PBX , Use Softphone Minimum 2 Line
 */
 ctiKawani.prototype.setCallBridge = function(mCallbacks)
{
	var agentAct = "CallBridge",
		agentExt = ctiClient.getClientSession('ctiAgentExten'), 
		param = {
		Action : fd_string(agentAct),
		Exten  : fd_string(agentExt)	
	};
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks))
			{
				mCallbacks.apply(this, [s, buf]);
			}
		}
	}); 
	
	return 0;
}

/*! 
 * \brief ctiKawani.setCallAnswer[string, string] 
 * \note  Must Integrate with Microsip Service Client Plugin on desktop 
 */
 ctiKawani.prototype.setCallAnswer = function(mCallbacks)
{
	var agentAct = "CallAnswer",
		agentExt = ctiClient.getClientSession('ctiAgentExten'), 
		param = {
		Action : fd_string(agentAct),
		Exten  : fd_string(agentExt)	
	};
	
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks))
			{
				mCallbacks.apply(this, [s, buf]);
			}
		}
	}); 
	
	return 0;
}

/*! 
 * \brief ctiKawani.setCallOnHold[SessionId:string] 
 * \note  Must Integrate with Microsip Service Client Plugin on desktop 
 *
 * \note state On| OnHold | Off  = UnHold 
 */
 ctiKawani.prototype.setCallOnHold = function(SessionId, mCallbacks)
{
	/*! for description
	 *  
	 * state :   
	 *  1	= On  | Hold 
	 *  0 	= Off | Unhold 
	 */ 
	 
	/*! \by default state is On */
	 var HoldState = "ON";  
	 if (typeof SessionId === "undefined" || typeof SessionId === ""){
		 var SessionId  = ctiClient.getCallSessionId();
	 }
	/*! create Paramater */
	 var agentAct 	= "CallHold",
		 agentExt 	= ctiClient.getClientSession('ctiAgentExten'),   
		 param = {
			Action 	  : fd_string(agentAct),
			Extension : fd_string(agentExt),
			SessionId : fd_string(SessionId),
			State 	  : fd_string(HoldState)	 
		};
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks)) {
				mCallbacks.apply(this, [s, buf]);
			}
			
			/*! \remark after compose then set its */ 
			ctiClient.setClientAddContact(SessionId, 'CallHold', HoldState);
			
		}
	}); 
	return 0;
}

/*! 
 * \brief ctiKawani.setCallOnHold[SessionId:string] 
 * \note  Must Integrate with Microsip Service Client Plugin on desktop 
 *
 * \note state On| OnHold | Off  = UnHold 
 */
 ctiKawani.prototype.setCallUnHold = function(SessionId, mCallbacks)
{
	/*! \by default state is On */
	 var HoldState = 'OFF'; 
	 console.log( typeof SessionId);
	 console.log( SessionId);
	 
	 if (typeof SessionId === "undefined" || typeof SessionId === ""){
		 var SessionId = ctiClient.getCallSessionId();
	 } 
	 
	 /*! create Paramater */
	 var agentAct 	= "CallHold",
		 agentExt 	= ctiClient.getClientSession('ctiAgentExten'),   
		 param = {
			Action 	  : fd_string(agentAct),
			Extension : fd_string(agentExt),
			SessionId : fd_string(SessionId),
			State 	  : fd_string(HoldState)	 
		};
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks)) {
				mCallbacks.apply(this, [s, buf]);
			}
			
			/*! \remark after compose then set its */ 
			ctiClient.setClientAddContact(SessionId, 'CallHold', HoldState);
		}
	}); 
	return 0;
}

/*! 
 * \brief ctiKawani.setCallMute[string, string] 
 * \note  Must Integrate with Microsip Service Client Plugin on desktop 
 */
 ctiKawani.prototype.setCallMute = function(State, Type, mCallbacks)
{
	/*! for description
	 *  
	 * state :   
	 *  1	= On 
	 *  0 	= Off 
	 *  
	 * type :
	 * 0 	= All
	 * 1	= In 
	 * 2 	= Out 
	 */ 
	 
	/*! \by default state is On */
	 ctiMuteState = State; 
	 ctiMuteType  = Type; 
	 if (typeof State == 'undefined'){
		 ctiMuteState = AST_MUTE_STATE_ON;
	 }
	 
	 /*! type of Mute */
	 if (typeof Type == 'undefined'){
		 ctiMuteType = AST_MUTE_TYPE_IN;
	 }
	 
	 /*! create Paramater */
	 var agentAct 	= "CallMute",
		 agentExt 	= ctiClient.getClientSession('ctiAgentExten'),  
		 param = {
			Action : fd_string(agentAct),
			Exten  : fd_string(agentExt),
			Type   : fd_string(ctiMuteType),
			State  : fd_string(ctiMuteState)	
			
		};
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks)) {
				mCallbacks.apply(this, [s, buf]);
			}
			
			/*! \remark after compose then set its */ 
			ctiClient.setCallSession('ctiAgentMuteState', ctiMuteState);
			ctiClient.setCallSession('ctiAgentMuteType' , ctiMuteType);
		}
	}); 
	return 0;
}


// ctiKawani.setCallDial[string, string]
ctiKawani.prototype.setCallDial = function(ctiCallerId, ctiAssignId, ctiContext, mCallbacks)
{ 
	var ctiProjectId  = ctiClient.getClientSession('ctiAgentProjectId'),
	    ctiAgentExten = ctiClient.getClientSession('ctiAgentExten');
		
		
	// -- VALIDATION : no have data to call be terminate. 
    writeloger(sprintf("INFO: call destination= %s [ID:%s]",ctiCallerId, ctiAssignId));
	if (fd_undef(ctiContext))
	{
	   ctiContext = 0; 
	}
	
	// untuk context dari function lama jika tidak di definisikan isi selalu menajdi == 0 
	if (fd_isfunc(ctiContext))
	{
	   ctiContext = 0; 
	}
	
	if (fd_undef(ctiCallerId))
	{
		return 0;
	}
	
    // -- VALIDATION : assign object data ID 
	if (fd_undef(ctiAssignId))
	{
		ctiAssignId = 0;
	}
	
    // -- VALIDATION : cannot be empty dialing number 
	if (fd_length(ctiCallerId)<3)
	{
		return 0;
	}
	
	var fd = {};
		fd.p0 = 'MakeCall';
		fd.p1 = ctiCallerId;
		fd.p2 = ctiAssignId;
		fd.p3 = ctiProjectId;
		fd.p4 = ctiAgentExten;
		fd.p5 = ctiContext;
		
	var param = 
	{
		Action 	   : fd_string(fd.p0),	/*! Action name */
		Extens 	   : fd_string(fd.p4),	/*! Local channel */  
		Context    : fd_string(fd.p5),	/*! Dialing Context */	
		CustomerId : fd_string(fd.p2),  /*! CustomerId / AssignId */ 
		CallerId   : fd_string(fd.p1),	/*! CallerID show on Softphone , uniqueID destination   */
		ProjectId  : fd_string(fd.p3)	/*! Call Project Separated	 */
	};
		
	ctiClient.setComposeMessage(param, (buf, s) => {
		if (fd_length(buf)>3)
		{
			s.setWriteMessage(buf);	
			if (fd_isfunc(mCallbacks))
			{
				mCallbacks.apply(this, [s, buf]);
			}
		}
	}); 
	
	return 0;
}; 

/*! \brief ctiKawani.setCallSessionId(sessionid) */
 ctiKawani.prototype.setCallSessionId = function(sessionid)
{ 
	if (fd_undef(sessionid)) 
	{
		var sessionid = "";
    }
    
	// set callSessionId = "" empty;
    ctiClient.setCallSession('CallSessionId', sessionid);
    return 0;
}; 

/*! \brief ctiKawani.setLocalPopup[object] */
ctiKawani.prototype.setLocalPopup = function(s)
{ 
	if (typeof window.callOpen === "undefined"){
		return 0;
	}
	window.callOpen = s; 
}

/*! @brief Service Agent Call Reset */
 ctiKawani.prototype.setSACAgentReset = function(mcallback)
{
	
}

/*! @brief Service Agent Call Start */
ctiKawani.prototype.setSACAgentStart = function(mcallback)
{
	
} 
/*! @brief Service Agent Call Stop */
 ctiKawani.prototype.setSACAgentStop = function(mcallback)
{
	
}


/*! \brief  ctiKawani.prototype.setLocalAgentStatus([id:integer, [data:object]]) */
 ctiKawani.prototype.setLocalAgentStatus = function(id,data)
{
	var c = {}; c.id = id; c.s = {}; c.s = ctiClient.getLocalAgentStatus(c.id); 
	if (!fd_isobj(c.s) &&(fd_isobj(data)))
	{
		c.s = (!fd_isobj(c.s) ? {} : c.s);
		for (var i in data)
		{
			if (fd_undef(c.s[i])) c.s[i] = data[i];
			else if (!fd_undef(c.s[i]) &&(fd_length(data[i])>0 ))
			{
				c.s[i] = data[i];
			}
		}
	}  
	else if (fd_isobj(c.s) &&(fd_isobj(data)))
	{  
		for (var i in data)
		{
			if (fd_undef(c.s[i])) c.s[i] = data[i];
			else if (!fd_undef(c.s[i]) &&(fd_length(data[i])>0 ))
			{
				c.s[i] = data[i];
			}
		} 
	}
	
	// ubah data dan check apkah process ini termasuk Session Object :
	c.k = ( c.s === null ? '' : JSON.stringify(c.s));
	if (fd_length(c.k)>0 &&(typeof window.localStorage == 'object'))
	{
		window.localStorage.setItem(c.id, c.k);
	}	 
	
	return 0;
} 

/*! \brief ctiKawani.prototype.getLocalAgentStatus([id:integer]) */
 ctiKawani.prototype.getLocalAgentStatus = function(id)
{
	var session = window.localStorage.getItem(id);
	if (session === null) return 0;
	else if (fd_undef(session)) return 0;
	else if (!fd_undef(session)) 
	{
		var data = JSON.parse(session);
		if ( fd_isobj(data) )
		{
			return data;
		}
	}
	return 0; 
}

/*! \brief ctiKawani.prototype.setParseUrl */
 ctiKawani.prototype.setParseUrl = function(url)
{
	var a = document.createElement('a'); a.href = url;
    return a;
}

/*! \brief ctiKawani.prototype.setParseQuery */
 ctiKawani.prototype.setParseQuery = function(s) 
{
	var vars = s.split( '&' ), query_string = {};
	for (var i = 0; i < vars.length; i++) 
	{
		var pair = vars[i].split( '=' ), key = decodeURIComponent(pair[0]), value = decodeURIComponent(pair[1]);
		if (typeof query_string[key] === "undefined" ) {
			query_string[key] = decodeURIComponent(value); 
		} else if (typeof query_string[key] === "string") {
			var arr = [query_string[key], decodeURIComponent(value)];
			query_string[key] = arr; 
		} else {
			query_string[key].push(decodeURIComponent(value));
		}
	}
	return query_string;
}

/*! \brief ctiKawani.prototype.setUrlCallback */
 ctiKawani.prototype.setUrlCallback = function(o, event)
{
	/*! \remark for chack callback if true */
	if ( typeof o  != 'object' )
	{
		console.log("Error 'ctiKawani.prototype.setUrlCallback()' Invalid Event");
		event.apply(this, [false]);
		return 0;
	}
	
	/*! \next to createDocumentFragment 'URL' */
	try {
		var s = this.setParseQuery(o.event_p), ret = {}, url = '';
		ret['event'] = o.event_e;
		for (const [key, val] of Object.entries(s)) {
			ret[key] = typeof eval(`${val}`) == 'undefined' ? '' : eval(`${val}`);
		} 
		/*! \remark get back to String */
		// url = sprintf('%s?/%s', o.event_u, Object.entries(ret).map(([key, val]) => `${key}=${val}`).join('&'));
		if (typeof event == 'function')
		{
			event.apply(this, [o.event_u, o.event_m, ret]);
			return ;
		}
	}
	/*! \remark if error handleEvent */
	catch (error){
		console.log("Error 'ctiKawani.prototype.setUrlCallback()' Invalid Object");
	}
	return 0;
}

/*! \brief ctiKawani.prototype.setUrlCallback */
 ctiKawani.prototype.getEventCallback = function(event)
{
	var ev = typeof ctiClient.session.callbacks[event]  == 'object' ? ctiClient.session.callbacks[event] : false;
	if ( !ev ){
		return 0;
	}
	return ev;
}

/*! \brief ctiKawani.setCallDial[integer, integer] return (string) */
ctiKawani.prototype.getCtiLabel = function(type, kode){
 return fd_string(fd_libs[type][kode]);
} 
/*! \brief  ctiKawani.getAgentStatus[null] return (integer) */
ctiKawani.prototype.getAgentStatus = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentStatus'));
}; 
/*! \brief  ctiKawani.getAgentId[null] return (integer) */
ctiKawani.prototype.getAgentId = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentId'));
};
/*! \brief  ctiKawani.getAgentGroupId[null] return (integer) */
ctiKawani.prototype.getAgentGroupId = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentGroupId'));
};
// ctiKawani.getAgentStatus[null] return (integer)
ctiKawani.prototype.getAgentSkill = function(){
 return fd_sigint(ctiClient.getClientSession('ctiAgentSkill'));
};
// ctiKawani.getCallStatus[null] return (integer)
ctiKawani.prototype.getCallStatus = function(){ 
 return fd_sigint(ctiClient.getCallerSession('CallStatus'));
};
// ctiKawani.getCallSessionId[null] return (string)
ctiKawani.prototype.getCallSessionId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallSessionId'));
};
// ctiKawani.getCallerId[null] return (string)
ctiKawani.prototype.getCallerId = function(){ 
 return fd_string(ctiClient.getCallerSession('CallCallerId'));
};
// ctiKawani.getCallChannels[null] return (string)
ctiKawani.prototype.getCallChannels = function(){ 
 return fd_string(ctiClient.getCallerSession('CallChannel1'));
};
// ctiKawani.getCallDirection[null] return (string)
ctiKawani.prototype.getCallDirection = function(){ 
 return fd_string(ctiClient.getCallerSession('CallDirection'));	
};

// ctiKawani.getCallIvrdata[null] return (string)
ctiKawani.prototype.getCallIvrdata = function(index)
{ 
	/*!if( typeof(index) == 'undefined') { index = 0;}*/
	return fd_string(ctiClient.getIvrSession(index));	
}; 	

// ctiKawani.getCallIvrdata[null] return (string)
ctiKawani.prototype.getCallSplitIvrData = function()
{ 
  return fd_string(ctiClient.getCallerSession('ctiCallSplitIVRData'));	
};	 

// ctiKawani.getCallSplitElement[null] return (string)
ctiKawani.prototype.getCallSplitElement = function()
{ 
  return fd_string(ctiClient.getClientSession('ctiCallSplitElements'));	
};

// ctiKawani.getCallIvrdata[null] return (string)
ctiKawani.prototype.getCallAssignId = function(){ 
  return fd_string(ctiClient.getCallerSession('CallAssignId'));	
};	 
// ctiKawani.getCtiMediaData[integer, integer] return (string)
ctiKawani.prototype.getMediaType = function(type){
 return fd_Media[type];
} 
// ctiKawani.getClientCallContact[integer, integer] return (string)
ctiKawani.prototype.getClientCallContact = function(sessionid){
	
	var storage = window.localStorage.getItem('cti_ws_contact');
	if ( typeof storage !== "string" ){
		return {};
	}
	
	if ( fd_undef(sessionid) ){
		return (typeof storage === "string" 
		? JSON.parse(storage) : {});
	}
	
	ws_storage = typeof storage === "string" ? JSON.parse(storage) : false;
	if (!ws_storage){
		return {};
	}
	
	if ( typeof ws_storage[sessionid] === "object"){
		return ws_storage[sessionid];
	}
	return {};
} 

// ctiKawani.getClientListContactcallback] return (string)
ctiKawani.prototype.getClientListContact = function(callback){ 
	var cti_var_contact = ctiClient.getClientCallContact(), cti_var_childs = false,  cti_var_maping  = {};
	for (var s in cti_var_contact) {
	  if (typeof cti_var_contact !== "object") {
		  continue;
	  }
	  /*! \brief get childElementCount */
	  cti_var_childs = cti_var_contact[s];
	  if (typeof cti_var_childs === "object" ) {
		 cti_var_maping[cti_var_childs.CallSessionId] = cti_var_childs.CallerID;
	  }
	}	
	/*! \sent to interface callback function */
	if (typeof callback === "function"){
		callback.apply(this, [cti_var_maping]);
	}
	return 0;
} 
 
/** 
 * https://ccm.net/forum/affich-57878-firefox-connection-interrupted
 * Type [firefox] About:config in the address bar.
 * search ipv6
 * make it FALSE If TRUE.
 * 
 */	 
 try{
	$(window).on('beforeunload', function(){
		if( typeof(ctiClient) != 'undefined' ){
			if(fd_isobj(ctiClient) && fd_isfunc(ctiClient.setWebSocketDisconect)){
				ctiClient.setWebSocketDisconect();
				return undefined;
			}
		}
	});
 }
catch(err){
	console.log(err)
}

// ============= USER  ========================
/*!
 *  \file kawani.cti.user.src-4.6.3.2.js
 *  \brief js for client section
 */
window.ctiOnPopupHandler = 0;
window.ctiOnButtonHandler = null;
window.ctiOnEventsHandler = 
{
    'onSocketInfo'	 : 'ctiOnReceiveSocketInfo',
    'onCallRinging'	 : 'ctiOnReceiveCallRinging',
    'onCallConnect'	 : 'ctiOnReceiveCallConnect',
    'onCallHangup'	 : 'ctiOnReceiveCallHangup',
    'onCallPopup'	 : 'ctiOnReceiveCallPopup',
	'onCallSplit'	 : 'ctiOnReceiveCallSplit',
	'onCallBridge'	 : 'ctiOnReceiveCallBridge',
	'onCallCtxfer'	 : 'ctiOnReceiveCallCtxfer',
	'onAgentStatus'	 : 'ctiOnReceiveAgentStatus',
    'onAgentSkill'	 : 'ctiOnReceiveAgentSkill',
	'onAgentPark'	 : 'ctiOnReceiveAgentParkCall',
	'onRecording'	 : 'ctiOnReceiveRecording',
	'onAcwTimeout'	 : 'ctiOnReceiveAcwTimeout',
	
};

/*!
 *  \brief ctiSetAgentPopup
 *
 *  \param [in] status Description for status
 *  \return Integer if More than '0' istrue status
 *
 *  \details Set Activated Popup if Active no Open Popup
 */

window.ctiSetAgentPopup = function (status) {
    localStorage.setItem('cti_ws_popup', status);
    if (typeof window.ctiGetAgentPopup === "function") {
        return window.ctiGetAgentPopup();
    }
    return 0;
}

/*!
 * \brief 	ctiSetAppPlayer
 * \remark  Get file for ringing Alert If have notification from channel
 *  		Note: this function custom only not for Public
 *
 * \param   {calback:method} method if success response , with response "Audio"
 * \retval	{void} return data of JSON Interface
 *
 */
if (window.ctiSetAppPlayer !== "function") {
    window.ctiSetAppPlayer = function (calback) {
        // ceck data 'undefined'
        var Notice = null,
        files = {
            'mail': 'samsung-notice.mp3',
            'sms': 'samsung-notice.mp3'
        },
        s = window.location;
        s.file = files['sms'];
        s.path = window.sprintf('smart-btn-collection/library/audio/%s', s.file);
        if (typeof url !== "undefined")
            url = window.sprintf('%s//%s/%s', s.protocol, s.hostname, s.path);
        else if (typeof url !== "undefined") {
            url = window.sprintf('%s//%s/%s', s.protocol, s.hostname, s.path);
        }

        Notice = new Audio(url);
        if (typeof Notice === "object" && typeof calback === "function") {
            calback.apply(this, [Notice]);
            return 0; /*! soundPlayer.pause(); soundPlayer.currentTime = 0; */
        }
    }
}

/*!
 * \brief 	ctiSetAppChannel
 * \remark  Find Or get data from your local database for check data
 *  		Note: this function custome only
 *
 * \param	{param:object}  parameter sent to backend server
 * \param   {calback:method} method if success response
 * \retval	{void} return data of JSON Interface
 *
 */
if (window.ctiSetAppChannel !== "function") {
    window.ctiSetAppChannel = function (param, callback) {
        /*! Ext.EventUrl(['MediaChannel', 'find']).Apply(); */
        var url = sprintf("//your.domain.com/find/channel/%s", param.type);
        $.getJSON(url, param, (res) => {
            if (typeof res === "object" && (typeof callback === "function")) {
                callback.apply(this, [res]);
                return 0;
            }
        });
    }
}

/*!
 * \brief 	ctiSetBinaryToString
 *
 * \remark  convert to encode string output
 * \private	{no}
 *
 * \param   {s:string} string data non encode
 * \retval 	{string}
 */
window.ctiSetBinaryToString = function (s) {
    var chr = s.split('x'),
    out = '';
    for (var i in chr) {
        if (chr[i] == '13')
            continue;
        else if (chr[i] == '10')
            continue;
        else if (chr[i] != '') {
            out += String.fromCharCode(chr[i]);
        }
    }

    return fd_string(out);
}

/*!
 * \brief 	ctiSetValidation
 *
 * \remark	Finall of validation starting load configuration Of CTI client
 *
 * \private {yes}
 * \param   {ctiConf:object}
 * \param   {callbacks:method}

 * \retval [type]         []
 */
window.ctiSetValidation = function (ctiConf, callbacks) {
	var export_modules = $( '.cti-panel-plugin' );
    if (!ctiConf) { // hide if fail process 
		if (typeof export_modules  === "object"){ 
			export_modules.hide();
		}
        console.log("Error 'window.ctiSetValidation()'");
		return 0;
    }
    /*! \remark if true process */
    var ctiSession = (typeof(ctiConf.session) != 'undefined' ? ctiConf.session : false); // jika data bukan object :
    $('.cell-display').hide();
    if (!ctiSession) {
        return false;
    }

    // ambil data yang sudah teregister :
    var ctiHost = ctiSession.server.host,
    ctiPort = ctiSession.server.port,
    ctiIpAgent = ctiSession.client.ctiAgentHost,
    ctiContext = ctiSession.label.extenState,
    agentContext = ctiSession.label.agentState;

    // validation HOST of CTI :
    if (ctiHost != '' && (ctiPort == '')) {
        ctiContext.html(window.sprintf("Ip-Address not registered [%s]", ctiIpAgent));
        agentContext.html('');
        return 0
    }

    // jika host dan port kosong :
    else if (ctiHost == '' && (ctiPort == '')) {
        ctiContext.html(window.sprintf("Ip-Address not registered [%s]", ctiIpAgent));
        agentContext.html('');
        return 0
    }

    if (typeof callbacks == 'function') {
        callbacks.apply(this, new Array(window));
    }
    return 0;
}

/*!
 * \brief 		ctiSetCallbackConfig
 * \remark 	    Get Call back for Incoming Call, Outgoing call , Predictive Call On Listen Receive
 *				message from cti websock client
 *
 * \private		{yes}
 *
 * \param  		{cfg:object}
 * \param  		{callbacks:method}
 *
 * \retval 		{no return}
 */
if (typeof window.ctiSetCallbackConfig != 'function') {
    window.ctiSetCallbackConfig = function (cfg, callbacks) {
        /*! \if false process */
        if (!cfg) {
            return 0;
        }

        /*! \remark get from storage window */
        var ws_callbacks = false, url = null , callbacks_ajax_settings = null;
        if ((ws_callbacks = window.localStorage.getItem('cti_ws_callback')) !== null) {
            ws_callbacks = JSON.parse(ws_callbacks);
            if (typeof callbacks === "function" && ws_callbacks) {
                callbacks.apply(this, [ws_callbacks]);
            }
            return 0;
        }
		
		
		/*! \remark check for header auth */
		callbacks_ajax_settings = {}; 
		if (API.auth)
		{	callbacks_ajax_settings.crossDomain = true;
			callbacks_ajax_settings.headers = {
				'Authorization' : API.auth,	
				'Content-Type' : "application/x-www-form-urlencoded"
			};
		} /*! Periksa ini lewat port atau lewat reverseProxy */
		var proxyUrl = sprintf("%s:%s/%s", API.host, API.port, API.path.callback); 
		if (API.port == ''){
			proxyUrl = sprintf("%s/%s", API.host, API.path.callback);  
		}/*! set data to ajaxSetup */
		callbacks_ajax_settings.url = proxyUrl;
		callbacks_ajax_settings.method = 'POST';
		callbacks_ajax_settings.async = false;
		callbacks_ajax_settings.cache = false;
		callbacks_ajax_settings.timeout = 0;
		callbacks_ajax_settings.data = {
			'username': cfg.ctiAgentName,
			'site': 'PDS'
		}  
		/*! \remark sent post data */
		$.ajax(callbacks_ajax_settings).done((rs) => {
			var codeType = false;
            if (typeof rs.result !== "undefined") {
                codeType = true;
            } /*! Sent via Router Or directly method ? */
            if (codeType) { 
                if (typeof rs.result === "object" && rs.result.error === true) {
                    window.alert(rs.result.message);
                    $.ajaxSetup({
                        async: true
                    });
                    return 0;
                } 
                /*! @overider */
                if (typeof rs.result.cti === "object") {
                    rs = rs.result.cti;
                }
            }
			
            if (typeof rs == 'object' && parseInt(rs.error)) {
                $.ajaxSetup({
                    async: true
                });
                return 0;
            } else if (typeof rs == 'object' && !parseInt(rs.error)) {
                if (typeof callbacks === "function") {
                    window.localStorage.setItem('cti_ws_callback', JSON.stringify(rs.data));
                    if ((ws_callbacks = window.localStorage.getItem('cti_ws_callback')) !== null) {
                        ws_callbacks = JSON.parse(ws_callbacks);
                        callbacks.apply(this, [ws_callbacks]);
                    }

                    $.ajaxSetup({
                        async: true
                    });
                    return 0;
                }
            } 
		}); 
    }
}
/*!
 * \brief 	ctiSetServerConfig
 *
 * \remark 	read Config from Cti API service to get detail ref of User Login
 * to CRM or Application Like "extension, userid, group, name , etc."
 *
 * \private {yes}
 * \param  	{post:object} post.url = {URL_Service}, post.data = { extension:string, username:string, password:string }
 *
 *			\@extension is required to login CTI
 *			\@username	is required to login CTI
 *			\@password	is required to login CTI
 *
 * \param  	{callback:method} response will sent to callback function
 * \retval 	{void}
 *
 */

if (typeof window.ctiSetServerConfig != 'function') {
    window.ctiSetServerConfig = function (post, callback) {
        if (!post) {
            return 0;
        }
        /*! \remark Url Config from API */
        var cfg = {}, hooks = {}, retval = {}; 
        /*! \remark get Data from API */
		var profiles_ajax_settings = {};
			profiles_ajax_settings.url = post.url;
			profiles_ajax_settings.method = 'POST';
			profiles_ajax_settings.timeout = 0;
			profiles_ajax_settings.data = post.data;
			profiles_ajax_settings.async = false;
			
		if (API.auth){
			profiles_ajax_settings.crossDomain = true;
			profiles_ajax_settings.headers = {
				'Authorization' : API.auth,
                'Content-Type' : "application/x-www-form-urlencoded"
			}
		} 
		
		/*! \remark hits to this domain */
		$.ajax(profiles_ajax_settings).done((o) =>{
			// recondtion {production}
            var codeType = false;
            if (typeof o.result !== "undefined") {
                codeType = true;
            }

            // from production server 
            if (codeType) { 
                if (typeof o.result === "object" && o.result.error === true) {
                    window.alert(o.result.message);
                    $.ajaxSetup({
                        async: true
                    });
                    return 0;
                }

                // @overide
                if (typeof o.result.cti === "object") {
                    o = o.result.cti;
                }
            }
            if (typeof o === "object" && typeof o.error !== "undefined" && o.error) {
                window.alert(o.message);
                $.ajaxSetup({
                    async: true
                });
                return 0;
            }
            
            else if (typeof o === "object" && typeof o.error !== "undefined" && !o.error) {
				cfg.cmdCtiServerDomain = o.data.APIServerDomain;
                cfg.cmdCtiServerProxy = o.data.APIServerProxy;
                cfg.cmdCtiServerHost = o.data.APIServerHost;
                cfg.cmdCtiServerPort = o.data.APIServerPort;
                cfg.cmdCtiServerUdp = o.data.APIServerUdp;
                cfg.cmdCtiServerWss = o.data.APIServerWss;

                /*! set All default for contsructoor make connection to WS */
                var s = window.location, urlHost = null, ctiHost = cfg.cmdCtiServerHost;
				/* set this object for check next on write connection */
				this.reverseProxy = API.reverseProxy;
				this.proto = (s.protocol == 'https:' ? 'wss' : 'ws');
                if (typeof s == 'object') {
                    if (s.hostname.length > 0) {
                        this.host = s.hostname.toString();
                        if (this.host == ctiHost) {
                            this.host = cfg.cmdCtiServerHost;
                            this.port = cfg.cmdCtiServerPort;
                        }
                        // jika hostname  == IP Real Sama :
                        else if (this.host != ctiHost) {
                            this.host = cfg.cmdCtiServerProxy;
                            this.port = cfg.cmdCtiServerUdp;
                        }
                    }
                }

               
                this.type = this.proto;
                if (this.type == 'wss') {
					this.host = cfg.cmdCtiServerDomain;
                    this.port = cfg.cmdCtiServerWss;
                } 
				/* Add new object for setup connection */
				retval.reverseProxy = this.reverseProxy; /* Apache 2.4 , nginx */
				retval.type = this.type; /* type ws|wss */
				retval.host = this.host; /* host to bind */
				retval.port = this.port;  /* port if exist */
				/* sent to callback function */
				if (typeof callback == 'function') {
                    callback.apply(this, [retval, o.data]);
                } $.ajaxSetup({
                    async: true
                });
                return;
            }
        }); 
    }
}
 
/*!
 * \brief 	ctiSetInitialize
 * \param   {API:object} 	$config [object]
 * \retval  {void}          []
 *
 */
if (typeof window.ctiSetServerInitialize !== "function") {
    window.ctiSetServerInitialize = function (API) {
        window.API = typeof API === "object" ? API : false;
        if (!window.API) {
            return false;
        }

        /*! \note {Initialize cti client socket}*/
        window.ctiClient = new ctiKawani();
        window.ctiGetUserAuth = false;
        window.ctiGetClientStorage((sf) => {

            /*! \note jika process di ambil dari storage {Url Config from API} */
            if (typeof sf !== "object") {
                delete window.ctiClient;
                return 0;
            } /*! periksa apakah client connect ke port tertentu */
			API.cfgs = sprintf("%s:%s/%s", API.host, API.port, API.path.login);
			if (API.port == '' ){
				API.cfgs = sprintf("%s/%s", API.host, API.path.login);
			} /* sent for get Auth login */
            window.ctiGetUserAuth = {
                auth : API.auth,
                url  : API.cfgs,
                data : {
                    'extension': sf.extension, // $('#extension').val(),
                    'username': sf.username, // $('#username').val(),
                    'password': sf.password // $('#password').val()
                }
            };
        });

        /*! \remark create setPanelConfig */
        window.ctiSetPanelConfig();

        /*! \remark {Starting Login Get Session from Server} */
        window.ctiSetServerConfig(ctiGetUserAuth, (cfg, auth) => {

            /*! \remark After success read coonfig setup */
            var __constructCfg = {};

            /*! \remark init config "label"  */
            __constructCfg.label = {
                agentState: $('#cmdAgentStatus'),
                callState: $('#cmdCallStatus'),
                serverState: $('#cmdServerStatus'),
                extenState: $('#cmdInfoStatus')
            };

            /*! \remark init config server */
            __constructCfg.server = {
				reverseProxy : cfg.reverseProxy,
                type : cfg.type,
                host : cfg.host,
                port : cfg.port
            };

            /*! \remark init config "client" */
            __constructCfg.client = {
                ctiAgentId: auth.APIUserId,
                ctiAgentName: auth.APIUserName,
                ctiAgentSkill: auth.APIUserSkill,
                ctiAgentPass: auth.APIUserPassword,
                ctiAgentProjectId: auth.APIProjectId,
                ctiAgentGroupId: auth.APIUserGroupId,
                ctiAgentExten: auth.APIUserExten,
                ctiAgentHost: auth.APIUserHost,
                ctiAgentKey: auth.APIUserKey,
                ctiAgentStatus: auth.APIUserStatus
            };

            /*! \remark for Caller If exits */
            __constructCfg.caller = {
                CallStatus: auth.APICallStatus,
                CallAssignId: auth.APICallAccountId,
                CallSessionId: auth.APICallSessionId,
                CallDirection: auth.APICallDirection,
                CallCallerId: auth.APICallCallerId,
                CallChannel1: auth.APICallChannel,
                CallChannel2: auth.APICallChannel2
            };

            /*! \remark init config "callback" */
            __constructCfg.callbacks = {};
            window.ctiSetCallbackConfig(__constructCfg.client, (callbacks) => {
                if (typeof callbacks == 'object') {
                    __constructCfg.callbacks = callbacks;
                }
            });

            /*! \remark init config "Interface"  */
            __constructCfg.interface = 'ctiOnReceiveMessage';

            /*! \remark starting launce config  */
            if (typeof __constructCfg == 'object') {
                ctiClient.setConfigServer(__constructCfg);
                delete __constructCfg;
            }
        });

        /*! \remark
         * Jika process connection cti Melalui IP Router dimana memiliki satu IP yang sama yang dapat di aksess publik
         * Maka Process Open Port Tergatung Port yang diberikan aksess oleh Applikasi:
         *
         */
        window.ctiSetValidation(window.ctiClient, (o) => {
            /*! \remark debug session on client to development process */
            var session = o.ctiClient.session.client;
            if (typeof(session))
                for (var i in session) {
                    writeloger(sprintf("%s = %s\n", i, session[i]));
                }

            /*! \remark  set display none on toolbar properties */
            o.ctiSetDisplayNone((e) => {
                writeloger("display:none");
            });

            /*! \remark
             * handleEvent UniqueId for tab will detected on ctiServer;
             * for cti revision version 4.6.2 rev-1
             */

            o.ctiClient.setWebSocketPrepare((s, uniqueID) => {
                writeloger(sprintf("INFO: tab session look ID = %s and Now ID= %s", uniqueID, window.strtotime()));
                s.setWebSocketConnect();
            });
        });
    }
};

/*!
 * \brief 	ctiSetLayoutConfig
 *
 * \remark  Auto Create Layout Toobar Of Cti On client window
 * if element exist custom by client no created layout
 *
 * \private	{yes}
 * \param 	{callback:method} method callback after success create layout
 * \retval	{void}	no return
 */
if (typeof window.ctiSetLayoutConfig != 'function') {
    window.ctiSetLayoutConfig = function (callback) {
        /*! \remark container */
        $('<div></div>').attr('id', 'cti-panel').addClass('cti-panel-plugin').appendTo($('body'));
        $('<div></div>').addClass('cti-panel-table').appendTo($('.cti-panel-plugin'));
        $('<div></div>').addClass('cti-panel-row').appendTo($('.cti-panel-table'));

        /*! \remark content */
        $('<div></div>').addClass('cti-panel-cell cell-agent').html('<i class="fa fa-user"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-agent').html('<span id="cmdAgentStatus"></span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-call').html('<span class="panel-vertical-line">&nbsp;</span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-call').html('<i class="fa fa-phone"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-call').html('<span id="cmdCallStatus"></span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-info').html('<span class="panel-vertical-line">&nbsp;</span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-info').html('<i class="fa fa-info"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-info').html('<span id="cmdInfoStatus"></span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-skill').html('<span class="panel-vertical-line">&nbsp;</span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-skill').html('<i class="fa fa-pencil"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-skill').html('<span id="cmdSkillStatus"></span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-display').html('<span class="panel-vertical-line">&nbsp;</span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-display').html('<i class="fa fa-bar-chart"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-display').html('<span id="cmdReportStatus"></span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-channel').html('<span class="panel-vertical-line">&nbsp;</span>').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-channel').html('<i class="fa fa-envelope"></i>&nbsp;').appendTo($('.cti-panel-row'));
        $('<div></div>').addClass('cti-panel-cell cell-channel').html('<span id="cmdMediaChannel"></span>').appendTo($('.cti-panel-row'));
		$('<div></div>').addClass('cti-panel-cell cell-parkcall').html('<span id="cmdParkStatus"></span>').appendTo($('.cti-panel-row'));


        /*! \set style csss after success "create" */
        if (typeof callback == 'function') {
            /*! \remark custom style*/
            var s = $('.cti-panel-plugin').css({
                'font-size': '12px',
                'font-family': 'sans-serif',
                'position': 'fixed',
                'z-index': '9999',
                'bottom': '0',
                'left': '0',
                'padding': '0px 5px 5px 5px',
                'text-align': 'center',
                'color': '#FFFFFF',
                'width': 'auto',
                'height': '18px',
                'backgroundColor': '#6E2503',
                'border': '1px solid #93331A',
                'border-top-right-radius': '5px',
                'overflow': 'no'
            })
                .find('.cti-panel-table').css({
                'display': 'table',
                'padding': '0px 0px 0px 0px',
                'margin': '0px 0px 0px 0px',
                'float': 'left'
            })
                .find('.cti-panel-row').css({
                'display': 'table-row',
                'padding': '5px 2px 5px 2px',
                'margin': '3px 3px 3px 3px'
            })
                .find('.cti-panel-cell').css({
                'display': 'table-cell',
                'padding': '5px 2px 1px 2px',
                'white-space': 'nowrap',
                'margin': '10px 1px 0px 1px',
                'vertical-align': 'middle'
            });
            callback.apply(this, [s]);
        }
        return 0;
    }
}

/**
 * \brief 	ctiSetPanelConfig
 * \remark 	This Method usage for Internal Only nit for public
 *
 * \private {yes}
 * \param  	{callback:method}	callback method if success load config
 * \retval  {void}
 */

if (typeof window.ctiSetPanelConfig != 'function') {
    window.ctiSetPanelConfig = function (callback) {
        /*! \remark check cti-panel panel of UI */
        var panelConfig = $('#cti-panel');
        if (panelConfig.length > 0) {
            if (typeof callback === "function") {
                callback.apply(this, [panelConfig]);
            }
            return 0;
        }
        /*! \remark after create Layout then */
        window.ctiSetLayoutConfig((s) => {
            if (typeof callback === "function") {
                callback.apply(this, [s]);
            }
        });

        return;
    }
}
/*!
 * \brief window.setButtonHandler]
 *
 * \note
 * Handler for manage multiple tab process of root assesment
 * dont add new tab sections
 *
 * \param  [type] $config [object]
 * \retval [type]         []
 *
 */
window.ctiSetButtonHandler = function (handleEvent, callback) {
    if (handleEvent === null) {
        window.ctiOnButtonHandler = null;
        return 0;
    }

    if (typeof handleEvent === "undefined") {
        window.ctiOnButtonHandler = null;
        return 0;
    }

    window.ctiOnButtonHandler = handleEvent;
    if (typeof callback === "function") {
        callback.apply(this, [true, window]);
        return 0;
    }
    return 0;
}

/*!
 * \brief ctiSetDisplayNone
 * \remark this method customize layout on client state
 *
 * \private {yes}
 * \param  	{mCallback:method}  response : [Global window]
 * \retval 	[type]         []
 */
window.ctiSetDisplayNone = function (mCallback) {
    if ($('.cell-channel').length) {
        $('.cell-channel').hide();
    }

    if (typeof mCallback == 'function') {
        mCallback.apply(this, [window]);
    }
    return 0;
}

/*!
 * \brief  	ctiSetCallSurvey
 *
 * \remark 	sent current call to IVR survey / sastification
 *
 * \private {no}
 * \param   {null}	no value
 * \retval  {void}	no value
 *
 */
if (typeof(window.ctiSetCallSurvey) != 'function') {
    window.ctiSetCallSurvey = function (calback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallSurvey(calback);
            return true;
        }
        return false;
    }
}

/*!
 * \brief   ctiSetCallHangup
 *
 * \remark  Call to Destination number with customerid / AssignId / CustomerId / Account , etc ..
 * this will relation on Mix recording generate
 *
 * \private {no}
 * \param 	{callback:method}  response([cti:object, [buf:string]])
 *
 * \retval 	{mixed} if boolean === false , fail process call or Not an Object
 */
if (typeof(window.ctiSetCallHangup) != 'function') {
    window.ctiSetCallHangup = function (callback) {
        if (typeof(window.ctiClient) === "object") {
            window.ctiClient.setCallHangup(callback);
            return;
        }
        return false;
    }
}
/*!
 * \brief   ctiSetCallCustomer
 *
 * \remark  Call to Destination number with customerid / AssignId / CustomerId / Account , etc ..
 * this will relation on Mix recording generate
 *
 * \private {no}
 * \param 	{destno:string}  destination number to call
 * \param 	{assignid:string} assigmnet data Id to relation for recording and call session
 * \param 	{callback:method} callback customize method with response([cti:object, [string:command ]])
 *
 * \retval 	{mixed} if boolean === false , fail process call or Not an Object
 */
if (typeof(window.ctiSetCallCustomer) != 'function') {
    window.ctiSetCallCustomer = function (destno, assignid, callback) {
        const context = 0; // default context
        if (typeof(window.ctiClient) === "object") {
            if (window.ctiGetAgentStatus() == AST_AGENT_BUSY)
                return false;
            else if (window.ctiGetAgentStatus() == AST_AGENT_ACW)
                return false;
            else {
                window.ctiClient.setCallDial(destno, assignid, context, callback);
                return true;
            }
        }
        return false;
    }
}

/*!
 * \brief   ctiSetDial
 *
 * \remark  Call to Destination number with customerid / AssignId / CustomerId / Account , etc ..
 * this will relation on Mix recording generate
 *
 * \private {no}
 * \param 	{destno:string}  destination number to call
 * \param 	{callback:method} callback customize method with response([cti:object, [string:command ]])
 *
 * \retval 	{mixed} if boolean === false , fail process call or Not an Object
 */
if (typeof(window.ctiSetDial) != 'function') {
    window.ctiSetDial = function (destno, callback) {
        const Context = 0, AssignDataId = 0// default context 
        if (typeof(window.ctiClient) === "object") {
            if (window.ctiGetAgentStatus() == AST_AGENT_BUSY)
                return false;
            else if (window.ctiGetAgentStatus() == AST_AGENT_ACW)
                return false;
            else {
                window.ctiClient.setCallDial(destno, AssignDataId, Context, callback);
                return true;
            }
        }
        return false;
    }
}

/*!
 * \brief 	ctiSetCallSplitToIVR
 *
 * \remark 	Split Call on current Call to caller = "source" And to Called = "Destination"
 * after "Called" success input data on IVR call bridge Again with "Caller"
 *
 * \private {no}
 * \param  	{AssignId:string}  sent parameter assigmnet for manage on popup and incoming message
 * \retval 	{void}
 *
 */
if (typeof(window.ctiSetCallSplitToIVR) != 'function') {
    window.ctiSetCallSplitToIVR = function (AssignId) {
        var agentTarget = fd_string('3800'),
        custTarget = fd_string('3900');
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallSplitToIVR(agentTarget, custTarget, AssignId, (cti, buf) => {
                writeloger(sprintf("INFO : We Got Split Call to IVR on CallSessionId = %s", cti.getCallSessionId()));

            });
        }

        return 0;
    }
}

/*! \brief 	ctiSetCallBackToIVR
 *  \private {yes}
 *  \param   null receipient to accepted current call
 *  \retval  {type} 
 */
if (typeof window.ctiSetCallBackToIVR != 'function') {
    window.ctiSetCallBackToIVR = function(mCallback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallBackToIVR(mCallback);
        } return 0;
    }
}

/*!
 * \brief 	ctiSetCallForward
 *
 * \private {no}
 * \param   {destno:string} receipient to accepted current call
 * \retval  {type}
 *
 */
if (typeof window.ctiSetCallForward != 'function') {
    window.ctiSetCallForward = function (destno) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallForward(destno);
            writeloger("INFO : We Got Forward Call");
        }

        return 0;
    }
}

/*!
 * \brief	ctiSetCallParking
 *
 * \private {no}
 * \remark 	sent current active call to IVR Hold
 * \param  	[null] 	no value
 * \retval 	[void]  no return
 *
 */

if (typeof(window.ctiSetCallParking) != 'function') 
{
    window.ctiSetCallParking = function () {
        if (typeof window.ctiClient === "object") {
            var agentStatus = window.ctiClient.getAgentStatus();
            window.ctiClient.setParkingCall((cti) => {
                writeloger("INFO : We Got Park Call");
				if (typeof window.ctiClient.setAgentNotReady === "function"){
					window.ctiClient.setAgentNotReady(6);
				}
            });
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallRetrive
 *
 * \remark 	get current call ParkCall before
 * \private {no}
 * \param  	[null] 	 no value
 * \retval 	[type]   no return
 */

if (typeof window.ctiSetCallRetrive != 'function') {
    window.ctiSetCallRetrive = function () {
        if (typeof window.ctiClient === "object") {
            var agentStatus = window.ctiClient.getAgentStatus();
            if (agentStatus != window.AST_AGENT_BUSY) {
                window.ctiClient.setRetriveCall((cti) => {
                    writeloger("INFO : We Got Retrive Call");
                });
            }
        }

        return 0;
    }
}

/*! \brief 	ctiSetCallSpying 
 *  \details Spy Call by Extension custom target, Revision on CTI version 4.6.5
 *  \private public aksess for client from browser {no}
 *  \param   {from_exten:string} remote extension to spy: Target
 *  \param   {to_exten:string} local extension to spy : receiver 
 *  \retval  {void} on success ringing on softphone , false == 0
 */

if (typeof window.ctiSetCallSpying != 'function') {
    window.ctiSetCallSpying = function (from, to) {
        if (typeof window.ctiClient === "object") {
            if (from !== "") {
					console.log(sprintf("from: SIP/%s to: SIP/%s", from, to));
                window.ctiClient.setAgentSpying(from, to);
            }
        }  return 0;
    }
}

/*!
 * \brief 	ctiSetCallCoaching
 *
 * \remark 	get current call ParkCall before
 * \private {no}
 * \param  	{exten:string} remote extension to barge
 * \retval  {void} on success ringing on softphone , false == 0
 */

if (typeof window.ctiSetCallCoaching != 'function') {
    window.ctiSetCallCoaching = function (from, to) {
        if (typeof window.ctiClient === "object") {
            if (from !== "") {
                window.ctiClient.setAgentCoaching(from, to);
            }
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallBarge
 *
 * \remark 	get current call ParkCall before
 * \private {no}
 * \param  	{exten:string} remote extension to barge
 * \retval  {void} on success ringing on softphone , false == 0
 */

if (typeof window.ctiSetCallBarge !== "function") {
    window.ctiSetCallBarge = function (from, to) {
        if (typeof window.ctiClient === "object") {
            if (from !== "") {
                window.ctiClient.setAgentBarge(from, to);
            }
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallAnswer
 *
 * \remark 	get current call ParkCall before
 * \private {no}
 * \param  	[null] 	 no value
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallAnswer != 'function') {
    window.ctiSetCallAnswer = function (callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallAnswer((s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}




/*!
 * \brief 	ctiSetCallCtxfer
 *
 * \remark 	Bridge two channels already in the PBX
 * \private {no}
 * \param  	[null] 	 calback method
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallCtxfer != 'function') {
    window.ctiSetCallCtxfer = function (destno, callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallCtxfer(destno, (s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallBridge
 *
 * \remark 	Bridge two channels already in the PBX
 * \private {no}
 * \param  	[null] 	 calback method
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallBridge != 'function') {
    window.ctiSetCallBridge = function (callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallBridge((s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallOnHold
 *
 * \remark 	Mute Current Call of Extension / User 
 * \private {no}
 * \param  	[session] if undefined 'session' default current 'sessionid'
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallOnHold != 'function') {
    window.ctiSetCallOnHold = function (sessionid, callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallOnHold(sessionid, (s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}
/*!
 * \brief 	ctiSetCallUnHold
 *
 * \remark 	Mute Current Call of Extension / User 
 * \private {no}
 * \param  	[session] if undefined 'session' default current 'sessionid'
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallUnHold != 'function') {
    window.ctiSetCallUnHold = function (sessionid, callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallUnHold(sessionid, (s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}

/*!
 * \brief 	ctiSetCallMute
 *
 * \remark 	Mute Current Call of Extension / User 
 * \private {no}
 * \param  	[null] 	 no value
 * \retval 	[type]   no return
 */
if (typeof window.ctiSetCallMute != 'function') {
    window.ctiSetCallMute = function (state, type, callback) {
        if (typeof window.ctiClient === "object") {
            window.ctiClient.setCallMute(state, type, (s, o) => {
                if (typeof callback === "function") {
                    callback.apply(this, [s, o]);
                }
            });
        }
        return 0;
    }
}

/*!
 * window.ctiSetBlindTransfer
 *
 * \remark 	Transfer Current Call To Other number
 *
 * \private {no}
 * \param  	{destno:string} Destination Number to transfer
 * \return 	false;
 */
if (typeof window.ctiSetBlindTransfer != 'function') {
    window.ctiSetBlindTransfer = function (destno) {
		 if (typeof window.ctiClient === "object") { 
            window.ctiClient.setCallTransfer(destno);
            return true;
        }
        return false;
    }
}

/*!
 * \brief ctiSetAttendTransfer
 *
 * \remark	this method cannt be apply on webscoket client
 *
 * \private {no}
 * \param  	{destno:string} Destination Number to transfer
 * \return	false;
 */
if (typeof window.ctiSetAttendTransfer != 'function') {
    window.ctiSetAttendTransfer = function (destno) {
        return 0
    }
}




/*!
 * \brief   ctiSetAgentAuxReason
 *
 * \remark  Set Current Agent/user  to  Not Ready With Parameter ReasonId
 *
 * \private {no}
 * \param  	{reasonid:integer}
 * \retval 	{boolean] if true  = success, false = failed
 *
 */
if (typeof window.ctiSetAgentAuxReason != 'function') {
    window.ctiSetAgentAuxReason = function (reasonid) {
        if (typeof window.ctiClient === "object") {
            window.ctiOnPopupHandler = 0;
            if (reasonid) {
                window.ctiClient.setAgentNotReady(reasonid);
                return true;
            }
        }
        return false;
    }
}

/*!
 * \brief   ctiSetAgentAcwTime
 *
 * \remark  Set Custome Timeout for ACW time every user 
 *
 * \private {no}
 * \param  	{reasonid:integer}
 * \retval 	{boolean] if true  = success, false = failed
 *
 */
if (typeof window.ctiSetAgentAcwTime != 'function') {
    window.ctiSetAgentAcwTime = function (timeout, callback) {
        if (typeof window.ctiClient === "object") {
            if (timeout) {
                window.ctiClient.setAgentAcwTime(timeout, callback);
                return true;
            }
        }
        return false;
    }
}

/*!
 * \brief   ctiSetAgentAbsoluteTimeout
 *
 * \remark  Set Customize Timeout for Absolute timeout every user 
 *
 * \private {no}
 * \param  	{reasonid:integer}
 * \retval 	{boolean] if true  = success, false = failed
 *
 */
if (typeof window.ctiSetAgentAbsoluteTimeout != 'function') {
    window.ctiSetAgentAbsoluteTimeout = function (param, callback) {
        if (typeof window.ctiClient === "object") {
            if (fd_isobj(param)) {
                window.ctiClient.setAgentAbsoluteTimeout(param, callback); 
				return true;
            }
        } return false;
    }
}
/*!
 * \brief ctiSetAgentNotReady
 *
 * \remark  Set Current Agent/user  to  Not Ready / Aux with reasonid Or empty reasonid
 *
 * \private {no}
 * \param   {type} [object]
 * \retval  {type} []
 *
 */
if (typeof window.ctiSetAgentNotReady != 'function') {
    window.ctiSetAgentNotReady = function (reasonid) {
        if (typeof window.ctiClient === "object") {
            window.ctiOnPopupHandler = 0;
            window.ctiClient.setAgentNotReady(reasonid);
        }
        return false;
    }
}

/*!
 * \brief 	 ctiSetAgentReady
 *
 * \remark 	 Change Status current agent / client  to Ready Or Idle
 *
 * \private {no}
 * \param   {null} 		 no value
 * \retval  {boolean}    true / false , if true = success, false = failed
 */
if (typeof window.ctiSetAgentReady != 'function') {
    window.ctiSetAgentReady = function () {
        if (typeof window.ctiClient === "object") {
            window.ctiOnPopupHandler = 0;
            window.ctiClient.setAgentReady();
            return true;
        }
        return false;
    }
}
/*!
 * \brief 	ctiSetAgentSkill
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {skill:integer}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiSetAgentSkill != 'function') {
    window.ctiSetAgentSkill = function (skill) {
        if (typeof window.ctiClient === "object") {
            if (parseInt(skill)) {
                window.ctiClient.setAgentSkill(skill);
                return true;
            }
        }
        return false;
    }
}
/*!
 * \brief 	ctiSetAgentRegister
 *
 * \remark 	Register  User from client socket to server CTI
 *
 * \private {no} option on user process
 * \param   {callback:method} 	method for callback response
 * \retval  {void} calback response
 *
 */
if (typeof window.ctiSetAgentRegister !== "function" ) 
{
	 window.ctiSetAgentRegister = function(calback) 
	{
		if (typeof window.ctiClient === "object" ) 
		{
			window.ctiClient.setAgentRegister((s, buf) => { 
				if (typeof calback === "function" ){
					calback.apply(this, [s, buf]);
				}
			});
		}
		return 0;
	}
}

/*!
 * \brief 	ctiSetAgentLogin
 *
 * \remark 	Login User If Register success,  from client socket to server CTI
 *
 * \private {no} option on user process
 * \param   {callback:method} 	method for callback response
 * \retval  {void} calback response
 *
 */
if (typeof window.ctiSetAgentLogin !== "function") 
{
	 window.ctiSetAgentLogin = function(calback) 
	{
		if (typeof window.ctiClient === "object")
		{
			window.ctiClient.setAgentLogin((s, buf) => {
				if ( typeof calback === "function" ){
					calback.apply(this, [s, buf]);
				}					
			});
		}
		return 0;
	}
}

/*!
 * \brief 	ctiSetAgentLogout
 *
 * \remark 	Logout from client socket to server CTI
 *
 * \private {no} option on user process
 * \param   {callback:method} 	method for callback response
 * \retval  {void} calback response
 *
 */
if (typeof window.ctiSetAgentLogout != 'function') {
    window.ctiSetAgentLogout = function (callback) {
        if (typeof window.ctiClient !== "object") {
            if (typeof callback === "function") {
                callback.apply(this, [0]);
            }
            return 0;
        } else if (typeof window.ctiClient === "object") {
            window.ctiClient.setAgentLogout(AST_AGENT_LOGOUT, (s) => {
                const sf = [ctiGetAgentId(), 'cti_ws_service', 'cti_ws_callback', 'cti_ws_popup'];
                sf.forEach((currentValue, index, arr) => {
                    localStorage.removeItem(currentValue);
                });

                if (typeof callback === "function") {
                    callback.apply(this, [s]);
                }
            });
        }
        return false;
    }
}
/*!
 * \brief 	ctiGetAgentPopup
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \retval  {boolean]  integer value
 *
 */
window.ctiGetAgentPopup = function () {
    var popup = localStorage.getItem('cti_ws_popup');
    if (typeof popup !== "string") {
        return 0;
    }
    return parseInt(popup);
}

/*!
 * \brief 	ctiGetAgentSkill
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {skill:integer}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetAgentSkill != 'function') {
    window.ctiGetAgentSkill = function (skill) {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getAgentSkill();
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetAgentId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {skill:integer}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetAgentId != 'function') {
    window.ctiGetAgentId = function () {
        if (typeof window.ctiClient === "object") {
            return ctiClient.getAgentId();
        }
        return false;
    }
}

/*!
 * \brief 	ctiGetAgentStatus
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {skill:integer}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetAgentStatus != 'function') {
    window.ctiGetAgentStatus = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getAgentStatus();
        }
        return "";
    }
}

/*!
 * \brief 	ctiGetAgentGroupId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetAgentGroupId != 'function') {
    window.ctiGetAgentGroupId = function () 
	{
        if (typeof window.ctiClient === "object") {
			return ctiClient.getAgentGroupId(); 
		}
        return false;
    }
}



/*!
 * \brief 	ctiGetClientContact
 *
 * \remark 	get All Caller Line connected on Current call / multiline 
 * \private {no}
 * \param   {sessionid} if sessionid undefined by default all contact like here 
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetClientContact != 'function') {
    window.ctiGetClientContact = function (sessionid) {
        if (typeof window.ctiClient === "object") {
            return ctiClient.getClientCallContact(sessionid);
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetClientListContact
 *
 * \remark 	get ListContact by { session:caller } 
 * \private {no}
 * \param   {calback} calback for iterator  
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetClientListContact != 'function') {
    window.ctiGetClientListContact = function(callback) {
        if (typeof window.ctiClient === "object") {
            return ctiClient.getClientListContact(callback);
        }
        return 0;
    }
}



/*!
 * \brief 	ctiGetClientSession
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetClientSession != 'function') {
    window.ctiGetClientSession = function (param) {
        if (typeof window.ctiClient === "object") {
            return ctiClient.getClientSession(param);
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetCallDirection
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetCallDirection != 'function') {
    window.ctiGetCallDirection = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallDirection();
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetCallCallerId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetCallCallerId != 'function') {
    window.ctiGetCallCallerId = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallerId();
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetCallIvrData
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */
if (typeof window.ctiGetCallIvrData != 'function') {
    window.ctiGetCallIvrData = function (position) 
	{
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallIvrdata(position);
        }
        return "";
    }
}

/*!
 * \brief 	ctiGetCallStatus
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */

if (typeof window.ctiGetCallStatus != 'function') {
    window.ctiGetCallStatus = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallStatus();
        }
        return 0;
    }
}

/*!
 * \brief 	ctiGetCallSessionId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */

if (typeof window.ctiGetCallSessionId != 'function') {
    window.ctiGetCallSessionId = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallSessionId();
        }
        return "";
    }
}

/*!
 * \brief 	ctiGetCallAssignId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {no_param}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */

if (typeof window.ctiGetCallAssignId != 'function') {
    window.ctiGetCallAssignId = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallAssignId();
        }
        return "";
    }
}

/*!
 * \brief 	ctiGetCallCustomerId
 *
 * \remark 	change skill of current agent / user 1= Inbound, 2= outbound 3 = predictive
 *
 * \private {no}
 * \param   {null}
 * \retval  {boolean]  if true == success, if false = Failed
 *
 */

if (typeof window.ctiGetCallCustomerId != 'function') {
    window.ctiGetCallCustomerId = function () {
        if (typeof window.ctiClient === "object") {
            return window.ctiClient.getCallAssignId();
        }
        return "";
    }
}
/*!
 * \brief 	document ready implementation
 * \param   {null} $config [object]
 * \retval  {void}         []
 *
 */
window.ctiGetClientStorage = function (callback) {
    var ws_demo_data = false,
    ws_demo_session = window.localStorage.getItem('cti_ws_service');
    if (typeof ws_demo_session === "string") {
        ws_demo_data = JSON.parse(ws_demo_session);
        if (typeof ws_demo_data == 'object' && ws_demo_data.extension != '' && ws_demo_data.username != '' && ws_demo_data.password != '') {
			ws_demo_data = ws_demo_data;
			/** if exist element "telephony" check attr if 0:Disabled , 1: Enabled */
			if (typeof ws_demo_data.telephony === "number" && ws_demo_data.telephony == 0){ // check telephony 
				if (typeof window.ctiClient === "object"){
					window.ctiClient.setWebSocketDisconect(1);
				}
				ws_demo_data = false;
			}
        }
    }

    /*! \remark sent feedback */
    if (typeof callback == 'function') {
        callback.apply(this, [ws_demo_data]);
    }
    return 0;
}

/*!
 * \brief  ctiOnReceiveMessage Interface Method
 *
 * \remark This Event Customize by user On config CTI all response call from socket
 * will sent this methode , for user can customize of process
 *
 * \private {no}
 * \param   {cti:object} all object response from event socket realtime
 *
 * \return  {typ}      [1 = INBOUND, 2= OUTBOUND, 3=PREDICTIVE]
 *
 */
 window.ctiOnReceiveMessage = function (cti) 
{
   /**!
    * \brief   {Event Handler on "agentstate / agent status"}
    * \remark  {.......................................... }
    *  Event yang menunjukan process call yang sedang terjadi seperti sibuk, call terjwab , Tidak Terjwab
    *  cti Clients hanya memberikan data ke sisi Applikasi dengan tujuan agar clients bisa customize di sisi applikasi
    *
    */   
    if (fd_isobj(cti) && (cti.event == 'callstate')) 
	{
        var call = cti.body, fd = false;
		/*! \brief {Event Handler For Call State "INBOUND" } */
        if (fd_isobj(call) && (call.type == AST_CALL_INBOUND)) {
            fd = typeof call.data === "object" ? call.data : false;
            if (fd_isobj(fd)) {
                /*! \handler for "popup" */
                const popupState = [AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING];
                if (window.search(fd.CallStatus, popupState)) {
                    window.ctiOnPopupHandler = window.ctiGetAgentPopup();
                    if (window.ctiOnPopupHandler < 1) {
                        window.ctiSetAgentPopup(fd.CallStatus);
                        ctiClient.setUrlCallback(ctiClient.getEventCallback('Inbound'), (url, method, data) => {
                            if (url && typeof window.ctiOnEventsHandler.onCallPopup === "string" && ctiOnEventsHandler.onCallPopup !== "") {
                                if (typeof window[ctiOnEventsHandler.onCallPopup] === "function") {
                                    window[ctiOnEventsHandler.onCallPopup].apply(this, [url, method, data]);
                                }
                            }
                        });
                    }
                }

                /*! \get state from this like /* 102,  */
                if (typeof window.ctiOnEventsHandler.onCallRinging === "string" && ctiOnEventsHandler.onCallRinging !== "") {
                    if (typeof window[ctiOnEventsHandler.onCallRinging] === "function") {
                        const ringingState = [AST_CALL_INIATED, AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING],
                        callStatus = fd.CallStatus;
                        if (window.search(fd.CallStatus, ringingState)) {
                            window[ctiOnEventsHandler.onCallRinging].apply(this, [AST_CALL_INBOUND, callStatus, call.data]);
                        }
                    }
                }

                /*! \get state for "ConnectedHandler" */
                if (typeof ctiOnEventsHandler.onCallConnect === "string" && ctiOnEventsHandler.onCallConnect !== "") {
                    if (typeof window[ctiOnEventsHandler.onCallConnect] === "function") {
                        const connectState = [AST_CALL_CONNECTED],
                        callStatus = fd.CallStatus;
                        if (window.search(fd.CallStatus, connectState)) {
                            window[ctiOnEventsHandler.onCallConnect].apply(this, [AST_CALL_INBOUND, callStatus, call.data]);
                        }
                    }
                }

                /*! \get state for "HangupHandler" */
                if (typeof ctiOnEventsHandler.onCallHangup === "string" && ctiOnEventsHandler.onCallHangup !== "") {
                    if (typeof window[ctiOnEventsHandler.onCallHangup] === "function") {
                        const hangupState = [AST_AGENT_NO_ANSWER, AST_CALL_MISSING_CALL, AST_CALL_ANSWERED, AST_CALL_NO_ANSWER, AST_CALL_ROUTED_TERMINATE],
                        callStatus = fd.CallStatus;
                        if (window.search(fd.CallStatus, hangupState)) {
							/* for version 4.6.3 revision 20211229 delete after drop hangup clients */
							if (typeof window.ctiClient.setClientDeleteContact === "function"){
								window.ctiClient.setClientDeleteContact(fd.CallSessionId); 
							} if (typeof window.ctiSetAgentPopup === "function"){
								window.ctiSetAgentPopup(0);
							} window[ctiOnEventsHandler.onCallHangup].apply(this, [AST_CALL_INBOUND, callStatus, call.data]);
						}
                    }
                }
            }
        }

        /*! \brief   {Event Handler For Call State "OUTBOUND" } */
        else if (fd_isobj(call) && (call.type == AST_CALL_OUTBOUND)) {
            fd = typeof call.data == 'object' ? call.data : false;
            if (fd_isobj(fd)) {
                /*! \handler for "popup" */
                const popupState = [AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING];
                if (window.search(fd.CallStatus, popupState)) {
                    window.ctiOnPopupHandler = window.ctiGetAgentPopup();
                    if (window.ctiOnPopupHandler < 1) {
                        window.ctiSetAgentPopup(fd.CallStatus);
                        ctiClient.setUrlCallback(ctiClient.getEventCallback('Outbound'), (url, method, data) => {
                            if (url && typeof window.ctiOnEventsHandler.onCallPopup === "string" && ctiOnEventsHandler.onCallPopup !== "") {
                                if (typeof window[ctiOnEventsHandler.onCallPopup] === "function") {
                                    window[ctiOnEventsHandler.onCallPopup].apply(this, [url, method, data]);
                                }
                            }
                        });
                    }
                }
            }

            /*! \get state for "RingingHandler" */
            if (typeof ctiOnEventsHandler.onCallRinging === "string" && ctiOnEventsHandler.onCallRinging !== "") {
                if (typeof window[ctiOnEventsHandler.onCallRinging] === "function") {
                    const ringingState = [AST_CALL_INIATED, AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING],
                    callStatus = fd.CallStatus;
                    if (window.search(fd.CallStatus, ringingState)) {
                        window[ctiOnEventsHandler.onCallRinging].apply(this, [AST_CALL_OUTBOUND, callStatus, call.data]);
                    }
                }
            }

            /*! \get state for "ConnectedHandler" */
            if (typeof ctiOnEventsHandler.onCallConnect === "string" && ctiOnEventsHandler.onCallConnect !== "") {
                if (typeof window[ctiOnEventsHandler.onCallConnect] === "function") {
                    const connectState = [AST_CALL_CONNECTED],
                    callStatus = fd.CallStatus;
                    if (window.search(fd.CallStatus, connectState)) {
                        window[ctiOnEventsHandler.onCallConnect].apply(this, [AST_CALL_OUTBOUND, callStatus, call.data]);
                    }
                }
            }

            /*! \get state for "HangupHandler" */
            if (typeof ctiOnEventsHandler.onCallHangup === "string" && ctiOnEventsHandler.onCallHangup !== "") { 
                if (typeof window[ctiOnEventsHandler.onCallHangup] === "function") {
                    const hangupState = [AST_AGENT_NO_ANSWER, AST_CALL_MISSING_CALL, AST_CALL_ANSWERED, AST_CALL_NO_ANSWER],
                    callStatus = fd.CallStatus;
                    if (window.search(fd.CallStatus, hangupState)) { 
						/*! \remark for version 4.6.3 revision 20211229 delete after drop hangup clients */
						if (typeof window.ctiClient.setClientDeleteContact === "function"){
							window.ctiClient.setClientDeleteContact(fd.CallSessionId); 
						}
						window[ctiOnEventsHandler.onCallHangup].apply(this, [AST_CALL_OUTBOUND, callStatus, call.data]);
                    }
                }
            }
        }

        /*! \brief   {Event Handler For Call State "PREDICTIVE" } */
        else if (fd_isobj(call) && (call.type == AST_CALL_PREDICTIVE)) {
            fd = typeof call.data == 'object' ? call.data : '';
            if (fd_isobj(fd)) {
                /*! \handler for "popup" */
                const popupState = [AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING];
                if (window.search(fd.CallStatus, popupState)) {
                    window.ctiOnPopupHandler = window.ctiGetAgentPopup();
                    if (window.ctiOnPopupHandler < 1) {
                        window.ctiSetAgentPopup(fd.CallStatus);
                        ctiClient.setUrlCallback(ctiClient.getEventCallback('Predictive'), (url, method, data) => {
                            if (url && typeof window.ctiOnEventsHandler.onCallPopup === "string" && ctiOnEventsHandler.onCallPopup !== "") {
                                if (typeof window[ctiOnEventsHandler.onCallPopup] === "function") {
                                    window[ctiOnEventsHandler.onCallPopup].apply(this, [url, method, data]);
                                }
                            }
                        });
                    }
                }

                /*! \get state from this like /* 102,  */
                if (typeof window.ctiOnEventsHandler.onCallRinging === "string" && ctiOnEventsHandler.onCallRinging !== "") {
                    if (typeof window[ctiOnEventsHandler.onCallRinging] === "function") {
                        const ringingState = [AST_CALL_INIATED, AST_CALL_OFFER, AST_CALL_ORIGNATING, AST_CALL_TRUNKSIZE, AST_CALL_DIALING],
                        callStatus = fd.CallStatus;
                        if (window.search(fd.CallStatus, ringingState)) {
                            window[ctiOnEventsHandler.onCallRinging].apply(this, [AST_CALL_PREDICTIVE, callStatus, call.data]);
                        }
                    }
                }

                /*! \get state for "ConnectedHandler" */
                if (typeof ctiOnEventsHandler.onCallConnect === "string" && ctiOnEventsHandler.onCallConnect !== "") {
                    if (typeof window[ctiOnEventsHandler.onCallConnect] === "function") {
                        const connectState = [AST_CALL_CONNECTED],
                        callStatus = fd.CallStatus;
                        if (window.search(fd.CallStatus, connectState)) {
                            window[ctiOnEventsHandler.onCallConnect].apply(this, [AST_CALL_PREDICTIVE, callStatus, call.data]);
                        }
                    }
                }

                /*! \get state for "HangupHandler" */
                if (typeof ctiOnEventsHandler.onCallHangup === "string" && ctiOnEventsHandler.onCallHangup !== "") {
					
                    if (typeof window[ctiOnEventsHandler.onCallHangup] === "function") {
                        const hangupState = [AST_AGENT_NO_ANSWER, AST_CALL_MISSING_CALL, AST_CALL_ANSWERED, AST_CALL_NO_ANSWER],
                        callStatus = fd.CallStatus; 
                        if (window.search(fd.CallStatus, hangupState)) {
							/*! \remark for version 4.6.3 revision 20211229 delete after drop hangup clients */
							if (typeof window.ctiClient.setClientDeleteContact === "function"){
								window.ctiClient.setClientDeleteContact(fd.CallSessionId); 
							}
							window[ctiOnEventsHandler.onCallHangup].apply(this, [AST_CALL_PREDICTIVE, callStatus, call.data]);
						}
                    }
                }
            }
        }
    }

    /*!
     * \brief  {Event Handler on "agentstate / agent status"}
     * \remark
     *  Event yang terjadi di sisi agent/user seperti "Ready", "Aux", "Busy", "Acw"
     *  process cti client hanya memberikan nilai balikan data yang di perlukan
     *  yang selanjut process kustomisasi sepenuhnya ada pada application clients
     *
     */
    else if (fd_isobj(cti) && (cti.event == 'agentstate')) {
        var call = cti.body,
        s = {};
        if (typeof call !== "object") {
            return 0;
        }

        s = call.data;

        /*! \handler for on Receive "AgentState" */
        if (typeof ctiOnEventsHandler.onAgentStatus === "string" && ctiOnEventsHandler.onAgentStatus !== "") {
            if (typeof window[ctiOnEventsHandler.onAgentStatus] === "function") {
                window[ctiOnEventsHandler.onAgentStatus].apply(this, [s.Value, s]);
            }
        }

        /*! \cleanup process if state on exts === REGISTER */
        if (!s.Value.localeCompare(AST_AGENT_REGISTER)) {
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }

        }

        /*! \cleanup process if state on exts === LOGIN */
        if (!s.Value.localeCompare(AST_AGENT_LOGIN)) {
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }
            return;
        }

        /*! \cleanup process if state on exts === READY */
        if (!s.Value.localeCompare(AST_AGENT_READY)) {
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }
            return;
        }

        /*! \cleanup process if state on exts === AUX */
        if (!s.Value.localeCompare(AST_AGENT_AUX)) {
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }
            return;
        }

        /*! \cleanup process if state on exts === BUSY */
        if (!s.Value.localeCompare(AST_AGENT_BUSY)) {
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }
            return;
        }

        /*! \cleanup process if state on exts === ACW */
        if (!s.Value.localeCompare(AST_AGENT_ACW)) {
            /* contoh: menambahkan HangupHandler */
            if (typeof window.ctiOnButtonHandler === "function" && typeof window.ctiClient === "object") {
                window.ctiOnButtonHandler.apply(this, [window.ctiClient, s.Value]);
            }
            return;
        }
        return 0;
    }

    /*! \remark { Event Handler on "Skill" } */
    else if (fd_isobj(cti) && (cti.event == 'skill')) {
        var o = cti.body, s = o.data,
        panelSkill = $('.cell-skill'),
        panelReport = $('.cell-display'),
        barSkill = $('#cmdSkillStatus');
        if (!fd_isobj(o.data)) {
            return 0;
        }  /* set handler implementation */
        if (typeof ctiOnEventsHandler.onAgentSkill === "string" && ctiOnEventsHandler.onAgentSkill !== "") {
            if (typeof window[ctiOnEventsHandler.onAgentSkill] === "function") {
                window[ctiOnEventsHandler.onAgentSkill].apply(this, [s.Skill, s]);
            }
        } /* set layout on bottom state */
        if (!fd_isobj(panelSkill)) {
            panelSkill.hide();
            return 0;
        } if (typeof panelSkill === "object"){
			panelSkill.show();
		} /* AST_SKILL_INBOUND */
		if (fd_sigint(s.Skill) == AST_SKILL_INBOUND) {
            panelReport.hide(200);
            if (fd_isobj(panelSkill)) {
                barSkill.html("Skill: Inbound");
            }
        } /* AST_SKILL_OUTBOUND */
        if (fd_sigint(s.Skill) == AST_SKILL_OUTBOUND) { 
			if (!window.WebSocketSACAgent){
				panelReport.hide(200);
			} if (fd_isobj(panelSkill)){		
				barSkill.html("Skill: Outbound");
			} 
        } /* AST_SKILL_PREDICTIVE */
        if (fd_sigint(s.Skill) == AST_SKILL_PREDICTIVE) {
            panelReport.show(200);
            if (fd_isobj(panelSkill)) {
                barSkill.html("Skill: Predictive");
            }
        } /* next return: */ 
		return 0;
    }
    /*! \remark { Event Handler on "CallLater" } */
    else if (fd_isobj(cti) && (cti.event == 'callater')) {
        var o = cti.body,
        s = o.data;
        if (fd_isobj(req.data)) {
            return 0;
        }
    }

    /*! \remark { Event Handler on "TransferCall" } */
    else if (fd_isobj(cti) && (cti.event == 'transfercall')) {
        var o = cti.body,
        bar = $('.transfer-call-style'),
        delay = 100,
        response = "";
        if (fd_isobj(o) && (o.type == 1)) {
            response = fd_string(o.data.Response),
            destination = fd_string(o.data.Destination);
            if (!response.localeCompare('failed')) {
                bar.show(delay * 3, () => {
                    var bodyContext = $(this).find('#cmdTransferStatus');
                    if (fd_isobj(bodyContext)) {
                        bodyContext.html(sprintf("Transfer Call to ( %s ) %s.", destination, response));
                        if (bar.css('display') != 'none') {
                            window.setTimeout(() => {
                                bar.hide(delay * 4, () => {
                                    bodyContext.html('');
                                });
                            }, delay * 50);
                        }
                    }
                });
            }

            // if response process is success:
            if (!response.localeCompare('success')) {
                bar.show(delay * 3, () => {
                    var bodyContext = $(this).find('#cmdTransferStatus');
                    if (fd_isobj(bodyContext)) {
                        bodyContext.html(sprintf("Transfer Call to ( %s ) %s.", destination, response));
                        if (bar.css('display') != 'none') {
                            window.setTimeout(() => {
                                bar.hide(delay * 4, () => {
                                    bodyContext.html('');
                                });
                            }, delay * 50);
                        }
                    }
                });
            }
        }

        return;
    }

    /*! \remark { Event Handler on "ParkCall" } */
    else if (fd_isobj(cti) && (cti.event == 'parkcall')) {
        // global body text
        var req = cti.body, row = {}, contentBar = $('.cell-parkcall'), contentBody = $( '#cmdParkStatus' ), delay= 100;
			
        /*! \remark style content layout minibar */ 
        var Content = '&nbsp;Parking Call ( %s ) <span class="panel-vertical-line">&nbsp;</span> Duration : %s', 
			CallerId = fd_string(req.data.CallerId);
			
		/*! next process on state parkCall */
		Timeout = window.duration(fd_sigint(req.data.Timeout));
		
		/*! \set handler implementation */
        if (typeof ctiOnEventsHandler.onAgentPark === "string" && ctiOnEventsHandler.onAgentPark !== "") {
            if (typeof window[ctiOnEventsHandler.onAgentPark] === "function") {
                window[ctiOnEventsHandler.onAgentPark].apply(this, [req.type, CallerId, Timeout]);
            }
        }
		
		 /*! \remark monitor:start */
		if (fd_isobj(req) && (req.type == 1)) 
		{
            contentBar.show(delay, () => { 
				contentBody.html(window.sprintf(Content, CallerId, Timeout));
            });
        }
        /*! \remark monitor:progress */
        else if (fd_isobj(req) && (req.type == 2)) { 
			contentBar.show(delay, () => {
				contentBody.html(window.sprintf(Content, CallerId, Timeout));
            }); 
        }
		
        /*! \remark monitor:stop */
        if (fd_isobj(req) && (req.type == 3)) 
		{
			contentBody.html(window.sprintf(Content, CallerId, Timeout));
			contentBar.hide(delay, () => {
              contentBody.html('');
            }); 
        }
    }
 
    /*! \remark {Event Handler on Split IVR} */
    else if (fd_isobj(cti) && (cti.event == 'callsplitivr')) {
        var o = cti.body,
        form = $('#ivrInputData'),
        retval = window.ctiClient.getCallSplitIvrData();
		
		console.log(o);
		
        if (!fd_isobj(o)) {
            return 0;
        } 
		
		/*! \set handler implementation for New Version Of CTI */
        if (typeof ctiOnEventsHandler.onCallSplit === "string" && ctiOnEventsHandler.onCallSplit !== "") {
            if (typeof window[ctiOnEventsHandler.onCallSplit] === "function") {
                window[ctiOnEventsHandler.onCallSplit].apply(this, [o.type, o, window.ctiClient]);
            }
        } 
		/*! \remark default Exceptions */
		if (o.type == 100)
            return 0; // start split call to IVR
        else if (o.type == 101)
            return 0; // start Listen channel
        else if (o.type == 102)
            return 0; // get local channel
        /*! \remark transfer data */
        else if (o.type == 103) {
            /*! \remark get Data Input By Remote Channel */
            if (fd_length(retval) > 1) {
                form.val(retval); 
            }
        }
        /*! \remark drop Split Call By Remote / Called  */
        else if (fd_isobj(o) && (o.type == 105)) {
            if (typeof window.ctiClient.setCallHangup === "function") {
                window.ctiClient.setCallHangup((cti, buf) => {
                    console.log('Remote Channel Is Hangup');
                });
            }
        } 
        return 0;
    }

    /*!
     * \brief   on Receive "MediaChannel"
     * \remark  Thread Process Event untuk Parking Call hanya satu slot saja dan interface berikut
     *		   ini untuk customize di sisi User
     */
    else if (fd_isobj(cti) && (cti.event == 'mediachannel')) {
        var o = cti.body,
        s = o.data,
        data = {},
        htmlContext = $('.cell-channel'),
        bodyContext = $('#cmdMediaChannel');
        if (!fd_isobj(o)) {
            return 0;
        }
        /*! \remark type for "SMS" */
        if (o.type && (o.type == 3)) {
            /*! \remark play notification Alert */
            data = {
                type: s.MediaType,
                agentid: s.AgentId
            };
            window.ctiSetAppPlayer((s) => {
                s.play();
            });

            window.ctiSetAppChannel(data, (res) => {
                if (typeof res === "object" && (res.message == 'success')) {
                    if (typeof res.data === "object") {
                        var message = sprintf("You have new Sms (%s)", res.data.total);
                        htmlContext.show(500, () => {
                            if (bodyContext.length > 0) {
                                bodyContext.html(message).on('click', () => {
                                    htmlContext.hide(100);
                                });
                            }
                        });
                    }
                }
            });
        }

        /*! \remark type for "Mail" */
        if (o.type && (o.type == 4)) {
            data = {
                type: s.MediaType,
                agentid: s.AgentId
            };
            window.ctiSetAppPlayer((s) => {
                s.play();
            });

            /*! \remark Triger event to server  */
            window.ctiSetAppChannel(data, (res) => {
                if (typeof res === "object" && (res.message == 'success')) {
                    if (typeof res.data === "object") {
                        var message = sprintf("You have new Mail (%s)", res.data.total);
                        htmlContext.show(500, () => {
                            if (bodyContext.length > 0) {
                                bodyContext.html(message).on('click', () => {
                                    htmlContext.hide(100);
                                });
                            };
                        });
                    }
                }
            });
        }
        return 0;
    }
    /*! \remark { Event Handler on "report" } */
    else if (fd_isobj(cti) && (cti.event == 'report')) { 
		var fd = cti.body, row = {}; 
		if (!fd_isobj(fd.data)){
			return 0;
		} /*! set data value to process */  
		row = typeof fd.data === "object" ? fd.data : false;
		if (!fd_isobj(row)){
			return 0;
		} /*! define all varible session */
		var total = 0, calling = 0, utilize = 0, process = 0, ready = 0, context  = false; 
		/*! report "PDS" */ 
		if (row.Type === "PDS"){ 
			window.WebSocketSACAgent = 0;
			total = fd_sigint(row.Total); calling = fd_sigint(row.Calling); utilize = fd_sigint(row.Utilize); process = fd_sigint(row.Process); ready = fd_sigint(row.Ready);
			if (row.Skill== AST_SKILL_PREDICTIVE){
				context = window.sprintf('Data: %s <span class="panel-vertical-line">&nbsp;</span> Utilize: %s <span class="panel-vertical-line">&nbsp;</span> Calling: %s <span class="panel-vertical-line">&nbsp;</span> Process: %s', total,ready,calling, process);
				$( "#cmdReportStatus" ).html(context);
			} return 0;  
		} /* report "SAC" */
		if (row.Type === "SAC"){  
			window.WebSocketSACAgent = total = fd_sigint(row.Total); utilize = fd_sigint(row.Utilize); process = fd_sigint(row.Process); ready = fd_sigint(row.Ready);  
			if (total){
				$(".cell-display").show(200);
			} if (row.Skill== AST_SKILL_OUTBOUND){
				context = window.sprintf('Total: %s <span class="panel-vertical-line">&nbsp;</span> Data: %s <span class="panel-vertical-line">&nbsp;</span> Called: %s', total, ready, process);
				$( "#cmdReportStatus" ).html(context);
			} return 0; 	
		} 
		
       // var req = cti.body, o = {};
		//console.log(req);
		
        // if (fd_isobj(req.data) && (o = req.data)) {
            // var total = fd_sigint(o.Total),
            // calling = fd_sigint(o.Calling),
            // utilize = fd_sigint(o.Utilize),
            // process = fd_sigint(o.Process),
            // ready = fd_sigint(o.Ready),
            // context = false;

            // update content HTML display :
            // context = window.sprintf('Data: %s <span class="panel-vertical-line">&nbsp;</span> Utilize: %s <span class="panel-vertical-line">&nbsp;</span> Calling: %s <span class="panel-vertical-line">&nbsp;</span> Process: %s', total, ready, calling, process);
            // if (o.Skill == AST_SKILL_PREDICTIVE) {
                // $('#cmdReportStatus').html(context);
            // }  else if (o.Skill == AST_SKILL_OUTBOUND) {
				 // $('#cmdReportStatus').html(context);
			// }
        // }
    }
	/*! \remark Event Handler Recording from Server */
	else if (fd_isobj(cti) && (cti.event == 'recording')) {
		var s = (typeof cti.body == 'object' ? cti.body : 0);
		
		/*! \remark for version 4.6.3 revision 20211229 delete after drop hangup clients */
		if (typeof window.ctiClient.setClientDeleteContact === "function"){
			window.ctiClient.setClientDeleteContact(s.data.SessionId); 
		}
		
		/*! \set handler implementation */
		if (typeof ctiOnEventsHandler.onRecording === "string" && ctiOnEventsHandler.onRecording !== "") {
            if (typeof window[ctiOnEventsHandler.onRecording] === "function") {
				window[ctiOnEventsHandler.onRecording].apply(this, [s.type, s.data]);
				delete s;
            }
        } 
		return 0;
	}
	/*! \remark Event Handler callbridge from Server */
	else if (fd_isobj(cti) && (cti.event == 'callbridge')) {
		var s = (typeof cti.body == 'object' ? cti.body : 0); 
		/*! \set handler implementation */
		if (typeof ctiOnEventsHandler.onCallBridge === "string" && ctiOnEventsHandler.onCallBridge !== "") {
			if (typeof window[ctiOnEventsHandler.onCallBridge] === "function") {
				window[ctiOnEventsHandler.onCallBridge].apply(this, [s.type, s.data]);
				delete s;
            }
        } 
		return 0;
	}
	
	/*! \remark Event Handler callctxfer from Server */
	else if (fd_isobj(cti) && (cti.event == 'callctxfer')) {
		var s = (typeof cti.body == 'object' ? cti.body : 0); 
		/*! \set handler implementation */
		if (typeof ctiOnEventsHandler.onCallCtxfer === "string" && ctiOnEventsHandler.onCallCtxfer !== "") {
			if (typeof window[ctiOnEventsHandler.onCallCtxfer] === "function") {
				window[ctiOnEventsHandler.onCallCtxfer].apply(this, [s.type, s.data]);
				delete s;
            }
        } 
		return 0;
	}
	/*! \remark Event Handler If error from Server */
    else if (fd_isobj(cti) && (cti.event == 'acwtimeout')) {
		var s = (typeof cti.body == 'object' ? cti.body : 0); 
		/*! \set handler implementation */
		if (typeof ctiOnEventsHandler.onAcwTimeout === "string" && ctiOnEventsHandler.onAcwTimeout !== "") {
			if (typeof window[ctiOnEventsHandler.onAcwTimeout] === "function") {
				window[ctiOnEventsHandler.onAcwTimeout].apply(this, [s.data]);
				delete s;
            }
        }  
		return 0
	}
	
    /*! \remark Event Handler If error from Server */
    else if (fd_isobj(cti) && (cti.event == 'ctierror')) {
        var req = (typeof cti.body == 'object' ? cti.body : 0);
        if (req) {
            /*! \set handler implementation */
            if (typeof ctiOnEventsHandler.onSocketInfo === "string" && ctiOnEventsHandler.onSocketInfo !== "") {
                if (typeof window[ctiOnEventsHandler.onSocketInfo] === "function") {
                    window[ctiOnEventsHandler.onSocketInfo].apply(this, [req.data.header, req.data.message]);
                }
            }

            /*! \remark for development only */
            writeloger(sprintf('EVT: %s', req.data.header));
            writeloger(sprintf('ERR: %s', req.data.message));
            writeloger("\n");
        }
    }
    return 0;
}

/*!
 * \brief 	document ready implementation
 * \param   {null} $config [object]
 * \retval  {void}         []
 *
 *! \note CTI Websocket Toolkit
 *
 * Untuk Login CTI Websocket diperlukan Langkah:
 *
 * 1. Embed JS source CTI Toolkit di sisi applikasi CRM
 *
 * 2. Mendefinisikan < @Extension, @Username, @Password> Pada saat Login ke Applikasi
 *    CRM dengan menyimpan Informasi tersebut di "localStorage" untuk di kenali oleh
 *	  CTI dengan nama "cti_ws_service" dengan berisi { "extension": "", "username": "", "password": ""}
 *
 * 3. Ketika Logout Sebaiknya LocalStorage di destroy saja
 *
 */
 
/*! \note change configure to Allowed Embed from client */
$(document).ready(() => {
    /* http://58.65.241.83:8886/pds/api/open/authcall */
    try {
		
		/*!
        // Production 	
		window.ctiSetServerInitialize
		({
			host :'//58.65.241.83',// Host of API 
			port :'8886',// Port of API 
			auth :'Bearer eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOjYz'  +// Auth Header 	
				  'LCJ1c2VybmFtZSI6IjcxN2FlMTA3MTg5ODYyOGUwYWU3ZGIxZDgyZjIwM' +
				  'jczIiwicHJvZmlsZV9pZCI6MCwiYXV0aF90aW1lIjoiMjAyMy0wMS0zMC' + 
				  'J9.9MsVhk_KGynyRBjkaU0vklcbd2hIkoY9bWy3yejUGPY',  
			path : {
				login: 'pds/api/open/authcall', // static method
				callback : 'pds/api/open/callbackcall' // static method
			} 	
		});
		*/
		
		// Development 
        window.ctiSetServerInitialize
        ({
            auth: false, // Auth Header
            host: "//192.168.10.236", // Host of API
            port: "16000", // Port of API
            path: {
                login: "APIActionUserAuth", // static method
                callback: "APIActionUserCallback" // static method
            },
			reverseProxy : false /* add new setting */
        });
		
    } catch (error) {
        console.log(error);
    }
});