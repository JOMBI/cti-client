<?php  
// get session object from session store:  
$cti->_setCtiPBX(CK()->get('KodeUser'));
if(CK()->find('Telphone') &&(CK()->get('Telphone') == 1)) {
?>
<form id="ctiAccessData" class=".ctiAccessData ctiAccessData" name="ctiAccessData" onsubmit="return false;">
	<input type="hidden" id="cmdCtiServerAddress" 	name="cmdCtiServerAddress"  value="<?php echo stringToBinary(CK()->get('ctiServerAddress'));?>">
	<input type="hidden" id="cmdCtiServerPort" 		name="cmdCtiServerPort" 	value="<?php echo stringToBinary(CK()->get('ctiServerPort'));?>">
	<input type="hidden" id="cmdCtiServerUdp" 		name="cmdCtiServerUdp" 		value="<?php echo stringToBinary(CK()->get('ctiServerUdp'));?>">
	<input type="hidden" id="cmdCtiServerWss" 		name="cmdCtiServerWss" 		value="<?php echo stringToBinary(CK()->get('ctiServerWss'));?>">
	<input type="hidden" id="cmdCtiAgentId" 		name="cmdCtiAgentId" 		value="<?php echo stringToBinary(CK()->get('ctiAgentId'));?>">
	<input type="hidden" id="cmdCtiAgentName" 		name="cmdCtiAgentName" 		value="<?php echo stringToBinary(CK()->get('ctiAgentName'));?>">
	<input type="hidden" id="cmdCtiAgentAddress" 	name="cmdCtiAgentAddress" 	value="<?php echo stringToBinary(CK()->get('ctiAgentAddress'));?>">
	<input type="hidden" id="cmdCtiAgentExtens"		name="cmdCtiAgentExtens"  	value="<?php echo stringToBinary(CK()->get('ctiAgentExtens'));?>">
	<input type="hidden" id="cmdCtiAgentPwd" 		name="cmdCtiAgentPwd" 		value="<?php echo stringToBinary(CK()->get('ctiAgentPassword'));?>">
	<input type="hidden" id="cmdCtiPabxTac" 		name="cmdCtiPabxTac" 		value="<?php echo stringToBinary(CK()->get('ctiPbxTac'));?>">
	<input type="hidden" id="cmdCtiProjectId" 		name="cmdCtiProjectId" 	 	value="<?php echo stringToBinary(CK()->get('ctiProjectId'));?>">
	<input type="hidden" id="cmdCtiUserModul" 		name="cmdCtiUserModul"  	value="<?php echo stringToBinary(CK()->get('ctiModulId'));?>">
</form>	
<div id="ctiClientSocket"></div>   
<?php }; ?> 
<!-- END OF FILE  -->
<!-- location : // ../application/layout/UserCti.php -->