<?php 
/**
 * Kawani CTI Server @version 4.6.1
 * An open source application used under PHP 5.1.6 or newer
 * system operation unix centos
 
 * @package			Kawani CTI Project 
 * @since			2018-01-01
 * @revsion			2019-03-20
 * @filesource  	Manager.php
 * @link 			https://www.voip-info.org/asterisk-cmd-extenspy/
 *
 */
 
if( !defined('CAPI_PATH_LOGER') ) define('CAPI_PATH_LOGER', '/var/log/kawani');
class APIManagerService{
/**
 * @method 	 APIManagerService.Properties 
 * @param	 array([host:, [port: ] ])
 * @return 	 $.this;
 */ 	
var $host = NULL;
var $port = NULL;
var $sock = NULL;

/**
 * [Instance]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
 private static $Task = NULL;
 
/**
 * @method 	 APIManagerService.__construct
 * @param	 array([host:, [port: ] ])
 * @return 	 $.this;
 */ 
function __construct( $config = NULL ){
 if( is_array( $config ) ){
	 foreach( $config as $k => $cfg ){
		$this->$k = $cfg;
	 }
 } 
}

/**
 * @method 		CApi.APIInstance 
 * @url 		NULL
 * @param		NULL
 */
public static function &Task(){
   if(is_null(self::$Task) ){
		self::$Task = new self();	
	}	
	return self::$Task;
 }

/**
 * @method 		CApi.connect 
 * @url 		NULL
 * @param		NULL
 */
function APIConnect(){
	$obj = new stdclass(); $obj->errno= 0; $obj->error= NULL; $obj->timeout = 10;
	$obj->sock= @fsockopen($this->host, $this->port, $obj->errno, $obj->error, $obj->timeout);
	if(!$obj->sock) {
	 // sent Message Error Server  
	 exit( json_encode( array(
		  'APIEvent'   => 'APIError',
		  'APICode'    => $obj->errno,
		  'APIMessage' => $obj->error,
		)
	  )); 
	}
	// finaly return :
	return $obj->sock;
	
 }

/**
 * @method 		CApi.param 
 * @url 		NULL
 * @param		NULL
 */
function APIHeader($req = NULL){
   $headers = getallheaders();	
   if( array_key_exists($req, $headers) ){
	 return $headers[$req];
  }
  return '';	
 } 
 
/**
 * @method 		CApi.param 
 * @url 		NULL
 * @param		NULL
 */
function APIParam($req = NULL){
   if( array_key_exists($req, $_REQUEST) ){
	 return $_REQUEST[$req];
  }
  return '';	
 } 
 
 /**
 * @method 		CApi.APIParsing
 * @url 		NULL
 * @param		NULL
 */
 function APIParsing( $ret = NULL ){
	$this->ret = trim($ret);
	$this->val = array();
	$this->got = 0;
	
	// if process parsing data is NULL;
	if( is_null($this->ret) ){
		$this->ret = $this->retval;
	}
	
	// get back data 
	$this->err = 0;
	while( !$this->got ){ 
		// parsing of data 
		if( $this->ret == 'OK' ){
			$this->val = array(
			  'event' => 'APIError',
			  'error' => 1,			
			  'message' => 'Action_Method_Not_Found',
			  'data' => array()	
			   
			);
			break;
		}
		
		// check if is NOK
		if( $this->ret == 'NOK' ){
			$this->val = array(
			  'event' => 'APIError',
			  'error' => 1,			
			  'message' => 'Action_Method_Not_Found',
			  'data' => array()	 
			);
			break;
		}
	
		if( !stristr($this->ret, '|')){
			
			$this->val = array(
			  'event' => 'APIError',
			  'error' => 1,			
			  'message' => 'Action_Ivalid_Message',
			  'data' => array()	 
			);
			break;
		}
	
		// get data Process:
		$this->ret = array_map('trim', explode('|', $this->ret));
		if(is_array( $this->ret ) &&( sizeof($this->ret)>0 )){
			foreach( $this->ret as $j => $r ){
				$k= NULL; $v = "";
				if( sscanf($r, '%[^:]:%s', $k, $v) ){
					
				  // check data string Body 	
				  if( stristr($v, '{')){
					 $v = @json_decode($v,true);
				  }
				  
				  // check on array
				  if( is_array($v)) foreach( $v as $d => $q ){
					  // case on Type Data DateTime 	
					  if($q&&!strcmp($d,'APIStartTime')) $q = date('Y-m-d H:i:s', $q);
					  if($q&&!strcmp($d,'APIRinging')) $q = date('Y-m-d H:i:s', $q);
					  if($q&&!strcmp($d,'APIAnswered')) $q = date('Y-m-d H:i:s', $q);
					  if($q&&!strcmp($d,'APIEndTime')) $q = date('Y-m-d H:i:s', $q); 
					  
					  $v[$d] = $q; 
				  }
				  
				  $this->val[$k] = $v;
				}
			}
		} 
		
		// break SKIP 
		++ $this->got;
	}
	
	// finaly return(0)
	return (object)$this->val;
 }
 
 
 /**
 * @method 		CApi.APIDebug 
 * @url 		NULL
 * @param		NULL
 */
function APIDebug( $params = NULL){
 if(!is_array($params)){
	$params = $_REQUEST;
 }	 
 $this->APILoger(sprintf("%s\n", date('Y-m-d H:i:s')));
 if(is_array($params)) 
 foreach( $params as $k => $val ){
	if( !strcmp($k,'eui_session')) continue;
	else if ( strcmp($k,'eui_session')) {
		$this->APILoger(sprintf(">> %s:%s\n",$k, $val) );
		
	}
 }	 
 // end;
 $fp = $this->APILoger("\r\n");
 if(is_resource($fp)){
	fclose($fp);
 }
 return 0;
}

/**
 * @method 		Manager.APILoger 
 * @url 		NULL
 * @param		NULL
 */
function APILoger($data){
	$path = CAPI_PATH_LOGER. "/API-Manager.log";
	$fp = NULL;
	if( !is_resource($fp)){
	  $fp = fopen( $path , "a+");
	}
	fwrite( $fp, $data); 
	return $fp;
}

/**
 * @method 		Manager.APITrace 
 * @url 		NULL
 * @param		NULL
 */
 
function APITrace($action = NULL, $data = NULL){
   $obj = new stdClass();
   $obj->f = $action;
   $obj->d = (array)$data;
   $obj->q = "";
   
   // check action arguments 
   if( is_null($obj->f) ) {
		return $obj->q;
   }
   
   if( !is_array($obj->d) ){ 
		return $obj->q; 
   }
   
   // then if arguments is exist 
   $obj->p = @implode( ",", $obj->d);
   $obj->q = sprintf("%s(%s)\r\n". "\r\n", $obj->f, $obj->p);
   return (string)$obj->q;
 }
 
/**
 * @method 		Manager.APIWrite 
 * @url 		NULL
 * @param		NULL
 */
function APIWrite( $buff = NULL, $time = 5){ 
	// new object 
	$obj = new stdClass();
	$obj->buff = $buff;
	$obj->time = $time;
	
	// get Static connection 
	$obj->sock = self::APIConnect();  
	if( !$obj->sock) {
	  @fclose($obj->sock); 
	  return 0;
	}
	// APIWrite data to sent Message :
	$obj->length = strlen($obj->buff); 
	if(false === @fwrite($obj->sock, $obj->buff, $obj->length)){
	   return 0;
	}
	
	// waite timeout :
	if(function_exists('stream_set_timeout')) stream_set_timeout($obj->sock,$obj->time);
	else if(!function_exists('stream_set_timeout')){ 
	  socket_set_timeout($obj->sock, $obj->time);
	} 
	// get Response from socket Manager :
	$obj->retval = 0; $obj->wait = 0;
	while(true){
	  $obj->retval = @fgets($obj->sock,8192);  
	  ++$obj->wait; // interval looping 
	  if(!is_bool($obj->retval)){
		 return $this->APIParsing($obj->retval); 
	  }
	  // jika data invalida:
	  else if(is_bool($obj->retval) &&($obj->wait>30)){
		 $obj->retval = "NOK"; 
		 break;
	  } 
	  if($obj->retval == "\n\r"){
		 $obj->retval = "NOK";
		 break;
	  }
	} 
	// close to socket :
	if(is_resource($obj->sock)){
		@fclose($obj->sock);
		$obj->sock = false;
	} 
	// finaly return 
	return $obj->retval; 
 } 
}

?>