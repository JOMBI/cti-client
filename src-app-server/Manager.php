<?php  
/**
 * Kawani CTI Server @version 4.6.1
 * An open source application used under PHP 5.1.6 or newer
 * system operation unix centos
 
 * @package			Kawani CTI Project 
 * @since			2018-01-01
 * @revsion			2019-03-20
 * @filesource  	Manager.php
 * @link 			https://www.voip-info.org/asterisk-cmd-extenspy/
 *
 * @Description
 *
 * ExtenSpy(extension[@context][,options])
 *
 * Parameters
 *
 * The options parameter, which is optional, is a string containging zero or more of the 
 * following flags and parameters:
 * 
 *	b: Only listens to channels which belong to a bridged call.
 *	g(grp): Only listens to channels where the channel variable ${SPYGROUP} is set to grp. ${SPYGROUP} can contain a : separated list of values.
 *	q: Do not play a tone or say the channel name when listening starts on a channel.
 *	r(name): Records the listening session to the spool directory. A filename may be specified if desired; chanspy is the default.
 *	v(value): Sets the initial volume. The value may be between -4 and 4.
 *	w: Enables “whisper” mode. Lets the spying channel talk to the spyed-on channel.
 *	W: Enables “private whisper mode”. The “spying” channel can whisper to the spyed-on channel, but cannot listen.
 *
 *
 * @Reference: 
 *
  [01] show-agents ------ 		 : show agent register on cti server with all status or by option eg: show-agents([s1,[s2, [s3, ..]]]) .
  [02] show-clients ----- 		 : show client socket connected to cti server.
  [03] show-extens ------ 		 : show all exten SIP register from all database pbx.
  [04] show-ivrextens --- 		 : show all exten IVR regsiter from all database pbx.
  [05] show-queues ------ 		 : show on call queued monitoring
  [06] reset-extens ----- 		 : reset extension to null process
  [07] show-api --------- 		 : show api thread party process
  [08] create-api ------- 		 : show api thread party process
  [09] reset-dbagent ---- 		 : reset agent activity on database
  [10] show-utilize ----- 		 : show cti server performance.
  [11] agent-skill ------ 		 : set agent skill if neded on via telnet eg : agent-skill([agent, [skill]])
  [12] show-regexten ---- 		 : show extension ready / register on cti server to monitor.
  [13] state-thread ----- 		 : monitoring PID process eg : state-thread([pid]).
  [14] queues-thread ---- 		 : queue monitoring and if exist will delivered to agent state ready
  [15] agents-thread ---- 		 : monitoring all agent active all session.
  [16] show-timeout ----- 		 : show how timeout on session with PID eg : show-timeout([pid]).
  [17] agents-skill ----- 		 : add skill agent with multiple agent_id eg : agents-skill([arg1^arg2^arg3..,[skilID]]).
  [18] show-address ----- 		 : show session finding with host state  example: show-address([host]).
  [19] show-matrix ------ 		 : show PDS callculation context eg: show-matrix([configID]).
  [20] start-matrix ----- 		 : start PDS process context eg: start-matrix([configID]).
  [21] pause-matrix ----- 		 : pause PDS Process context eg: pause-matrix([configID]).
  [22] stop-matrix ------ 		 : stop PDS process context eg: stop-matrix([configID]).
  [23] call-matrix ------ 		 : running on realtime process a PDS config call-matrix([configID]).
  [24] call-hangup ------ 		 : terminate  a channel call from console manager eg: call-hangup([exts]) .
  [25] call-spying ------ 		 : spy a channel call from console manager eg: call-spying([from, [destination ]]).
  [26] call-coaching ---- 		 : coach a channel call from console manager eg: call-coaching([from, [destination ]]).
  [27] call-barge ------- 		 : barge a channel call from console manager eg: call-barge([from, [destination ]]).
  [28] reload-config ---- 		 : reloading config all cti common .
  [29] register-extens--- 		 : register extension to PABX server eg: register-extens([exts,[pass,[host, [ipPBX, [type,[status, [site ]]]]]]]).
  [30] show-cahnnels ---- 		 : show all channels active.
  [31] show-session ----- 		 : show all callsession actives on server CTI.
  [32] call-dial -------- 		 : direct call from console manager eg: call-dial([exts, [destination, [assignid]]]).
  [33] drop-session ----- 		 : drop call/hangup by remote cahnnels : drop-session([callsessionid]).
  [34] report-matrix ---- 		 : sent info to client from PDS : report-matrix([skill,[object]]).
  [35] show-agentskill--- 		 : show all agents by skill show-agentskill([skill]).
  [36] agents-predictive  		 : show all agent ready to predictive and update agents-predictive([cond = 1,0]).
  [37] update-amaflag --- 		 : update agent amaflag predictive update-amaflag([extens, [cond = 1,0]]).
  [38] show-matrix-extens 		 : update agent amaflag predictive show-matrix-extens([config]).
  [39] update-matrix-extens      : update agent amaflag predictive update-matrix-extens([extens, [config = 1,0]]).
  [40] reset-matrix-extens       : update agent amaflag predictive reset-matrix-extens([config]).
  [41] show-worker-thread 		 : show all service addon running show-worker-thread
  [42] stop-worker-thread 		 : stop service addon running stop-worker-thread([title]).
  [43] start-worker-thread       : start service addon running start-worker-thread([title]).
  [44] restart-worker-thread     : restart service addon running restart-worker-thread([title]).
  [45] set-after-call-work       : update after call work to CTI manager set-after-call-work([pbx_host,[time(s),[default_state]]])
  [46] set-exten-state    		 : change state / status Extension ([exten,[state]])
  [47] delete-session     		 : remove call session from cti server([arg1,[arg2,[arg3], ... ]])
  [48] delete-queue       		 : remove call session from cti Queue DB([s:string,[s2:string,[s3:string], ... ]])
  [49] set-context-dial   		 : set context dial outbound ([idx:integer,[context:string]])
  [50] show-context-dial  		 : show context dial outbound
  [51] set-param-spying   		 : set parameter option spy|coach|Barge eg: set-param-spying(node, args)
  [52] utils-reload-config       : reload configuration [PDS|SNM|AGT|QUE] from console eg: util-reload-config(PDS)
  [53] utils-client-logout       : remote logout from console : utils-client-logout(exts)
  [54] utils-client-reload       : remote reload client from console eg: utils-client-reload(exts)
  
 */
 
class Manager extends EUI_Controller {
	
/**
 * @method 		Manager.__construct 
 * @url 		NULL
 * @param		NULL
 */
 function __construct(){
   parent::__construct();	
   $this->load->helper(array('EUI_APIManager')); 
   // call objects "CTI API Manager Service" AMS
   if( !isset($this->APIManager) &&( class_exists( 'APIManagerService' ))){	
	  $this->APIManager = new APIManagerService(array( 
		'host' => 'localhost',
		'port' => '19100',
		'user' => 'user',
		'pass' => 'pass'
	  ));
   }
   
 } 
 
/**
 * @method 		API Manager.APIActionCall 
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCall/?exts=6000&destnum=08716716717&custid=90900&uniqueid=
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionCall($exts = NULL, $destnum = NULL, $custid= NULL, $uniqueid=0) {
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json; charset=UTF-8');	 

	$obj		   = new stdclass();
	$obj->exts     = $exts;
	$obj->destnum  = $destnum;
	$obj->custid   = $custid;
	$obj->uniqueid = $uniqueid; 
	
    // default response to clients 
	$obj->rets = array();
	
	// extension yang akan melakukan call 
	if( is_null($obj->exts) ){
	   $obj->exts = $this->APIManager->APIParam('exts');  
	}
	// no tujuan yang akan di dial */
	if( is_null($obj->destnum) ){
		$obj->destnum = $this->APIManager->APIParam('destnum'); 
	} 
	// assign data varchar / ID  
	if( is_null($obj->custid) ){
	   $obj->custid = $this->APIManager->APIParam('custid'); 
	} 	
		
	// userid called varchar / ID  
	if( !$obj->uniqueid ){
		$obj->uniqueid = $this->APIManager->APIParam('uniqueid');  
	}
	
	// default proc_close ($.)
	if( !$obj->uniqueid ){
		$obj->uniqueid = md5(microtime(true));
	}	
	// buat pesan untuk di kirim ke CTI Manager
	$obj->q = $this->APIManager->APITrace("APIActionCall", array( $obj->exts, $obj->destnum, $obj->custid, $obj->uniqueid ));
	$obj->o = $this->APIManager->APIWrite($obj->q);  
	if(is_object($obj->o) &&(isset($obj->o->event))){
	   $obj->rets = (array)$obj->o;
	}
	// finaly response:
	exit( json_encode($obj->rets) ); 
 }
  
  /**
  * @method 	Manager.APICallRetrive
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APICallRetrive
  * @param		[$from, $to, $pq]
  */
  
 function APIActionCallRetrive($from = NULL, $to = NULL, $pq= "bq"){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 exit( json_encode( array(
		"Test" => "APICallRetrive"
	 ))); 
 }
 
 /**
  * @method 	Manager.APIActionCallMute
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallMute
  * @param		[$from, $to, $pq]
  */
  
 function APIActionCallMute($exts = NULL, $type = NULL, $state= NULL){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' ); 
	 // new data object 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->exts = $exts; 
	 $obj->type = $type;  
	 $obj->state = $state;
	 
	 // check all parameter from 
	 if(is_null($obj->exts)){
		 $obj->exts = $this->APIManager->APIParam('exts');
	 }
	 
	 // check parameter to 
	 if(is_null($obj->type)){
		 $obj->type = $this->APIManager->APIParam('type');
	 }
	 
	 // get parameter context 
	 if(is_null($obj->state)){
		 $obj->state = $this->APIManager->APIParam('state');
	 }
	 
	 // buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallMute", array($obj->exts, $obj->type, $obj->state));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }
	 
	 // finaly response:
	 exit( json_encode($obj->rets) );  
 }
 
 
 
 /**
  * @method 	Manager.APIActionCallPark
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallPark
  * @param		[$from, $to, $pq]
  */
  
 function APIActionCallPark($from = NULL, $to = NULL, $pq= "bq"){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 exit( json_encode( array(
		"Test" => "APICallPark"
	 ))); 
 }
 
 /**
  * @method 	Manager.APIActionCallSurvey
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallSurvey/?from=6000&to=9990
  * @param		[$from, $to, $p]
  */
 function APIActionCallSurvey($from = NULL, $to = NULL, $p= NULL){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 // new data object 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to = $to;  
	 $obj->p = $p;
	 
	 // check all parameter from 
	 if(is_null($obj->from)){
		 $obj->from = $this->APIManager->APIParam('from');
	 }
	 
	 // check parameter to 
	 if(is_null($obj->to)){
		 $obj->to = $this->APIManager->APIParam('to');
	 }
	 
	 // get parameter context 
	 if(is_null($obj->p)){
		 $obj->p = $this->APIManager->APIParam('p');
	 }
	 
	 // buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallSurvey", array($obj->from, $obj->to));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }		 
	 
	  // finaly response:
	exit( json_encode($obj->rets));  
 }
 
 
 /**
  * @method 	Manager.APIActionCallForward
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallForward/?from=6000&to=5001&q=smart-outgoing
  * @param		[$from, $to, $pq]
  */
 function APIActionCallForward($from = NULL, $to = NULL, $p='smart-forward'){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 
	 // new data object 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to = $to;  
	 $obj->p = $p;
	 
	 // check all parameter from 
	 if(is_null($obj->from)){
		 $obj->from = $this->APIManager->APIParam('from');
	 }
	 
	 // check parameter to 
	 if(is_null($obj->to)){
		 $obj->to = $this->APIManager->APIParam('to');
	 }
	 
	 // get parameter context 
	 if(is_null($obj->p)){
		 $obj->p = $this->APIManager->APIParam('p');
	 }
	 
	 // buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallForward", array($obj->from, $obj->to, $obj->p));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }		 
	 
	  // finaly response:
	exit( json_encode($obj->rets)); 
 }
 
 
 /**
  * @method 	Manager.APIActionCallTransfer
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallTransfer/?to=6000 
  * @param		[$from, $to, $pg]
  */
 function APIActionCallTransfer($from = NULL, $to = NULL, $pg=3000){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8');
	 
	 // new data object 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to = $to;  
	 $obj->pg = $pg;
	 
	 // check all parameter from 
	 if(is_null($obj->from)){
		 $obj->from = $this->APIManager->APIParam('from');
	 }
	 
	 // check parameter to 
	 if(is_null($obj->to)){
		 $obj->to = $this->APIManager->APIParam('to');
	 }
	 
	 // get parameter context 
	 if(is_null($obj->pg)){
		 $obj->pg = $this->APIManager->APIParam('pg');
	 }
	 
	 // buat pesan untuk di kirim ke CTI Manager 
	 $obj->q = $this->APIManager->APITrace("APIActionCallTransfer", array($obj->from, $obj->to, $obj->pg));	
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 } 
	 
	 // finaly response:
	 exit( json_encode($obj->rets)); 
 }
 
 /**
  * @method 	Manager.APIActionHold
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionHold
  * @param		[$from, $to, $pq]
  */
  
 function APIActionCallHold($from = NULL, $to = NULL, $pq= "bq"){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 exit( json_encode( array(
		"Test" => "APIHold"
	 ))); 
 }
 
 /**
  * @method 	Manager.APIActionCallSpy
  * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallSpy/?from=6000&to=4004&pq=bq 
  * @param		[$from, $to, $pq]
  */
  
 function APIActionCallSpy($from = NULL, $to = NULL, $pq= "bq"){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	
	 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to 	= $to;  
	 $obj->pq 	= ""; 
	 
	/**
	 * extension yang melakukan spy bisa di ambil dari Session 
	 * misalnya $_SESSION['agentExt']
	 */ 
	 if(is_null($obj->from)){
		$obj->from = $this->APIManager->APIParam('from'); //(isset($_REQUEST['from']) ? $_REQUEST['from'] : '');
	 }
	 
	/**
	 * Extension yang akan di spy atau Extension Tujuan 
	 */
	 if(is_null($obj->to)){
		$obj->to = $this->APIManager->APIParam('to'); //(isset($_REQUEST['to']) ?  $_REQUEST['to']: '');
	 } 
	 
	/**
	 * parameter yang di set dari client
	 */
	 $obj->pq = trim($pq);
	 if(is_null($pq)){
		$obj->pq = $this->APIManager->APIParam('pq'); //(isset($_REQUEST['pq']) ?  $_REQUEST['pq']: '');
	 } 
	 
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallSpy", array($obj->from, $obj->to, $obj->pq));		
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }		 
	 
	  // finaly response:
	exit( json_encode($obj->rets)); 
 }

 
/**
 * @method 		Manager.APIActionCoach 
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCoach/?from=6000&to=4004&pq=bqw
 * @param		[$from, $to, $pq]
 */
 function APIActionCallCoach($from = NULL, $to = NULL, $pq= "bqw"){
	 header( 'Access-Control-Allow-Origin: *' );
	 header( 'Content-Type: application/json; charset=UTF-8' );
	 
	 
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to 	= $to;   
	 $obj->pq 	= $pq;
	 
	 
	/**
	 * extension yang melakukan spy bisa di ambil dari Session 
	 * misalnya $_SESSION['agentExt']
	 */ 
	 if(is_null($obj->from)){
		$obj->from = (isset($_REQUEST['from']) ? 
			$_REQUEST['from'] : '');
	 }
	/**
	 * Extension yang akan di spy atau Extension Tujuan 
	 */
	 if(is_null($obj->to)){
		$obj->to = (isset($_REQUEST['to']) ? 
			$_REQUEST['to']: '');
	 }
	 
	/**
	 * parameter yang di set dari client
	 */
	 if(is_null($obj->pq)){
		$obj->pq = (isset($_REQUEST['pq']) ? 
			$_REQUEST['pq']: '');
	 } 
	 
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallCoach", array($obj->from, $obj->to, $obj->pq));	
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }	
	  	 
	 // finaly response:
	exit( json_encode($obj->rets) ); 
 }
 
/**
 * @method 		Manager.APIActionBarge 
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionBarge/?from=6000&to=4004&pq=qB
 * @param		[$from, $to, $pq ]
 */
 function APIActionCallBarge($from = NULL, $to = NULL,$pq= "qB"){
	 
	 header("Access-Control-Allow-Origin: *");
	 header("Content-Type: application/json; charset=UTF-8");
	
	 $obj = new stdclass();
	 $obj->rets = array('success' => 0,  'message' => 'no body response');
	 $obj->from = $from; 
	 $obj->to 	= $to;   
	 $obj->pq 	= $pq;
	 
	/**
	 * extension yang melakukan spy bisa di ambil dari Session 
	 * misalnya $_SESSION['agentExt']
	 */ 
	 
	 if(is_null($obj->from)){
		$obj->from = (isset($_REQUEST['from']) ? 
			$_REQUEST['from'] : '');
	 }
	/**
	 * Extension yang akan di spy atau Extension Tujuan 
	 */
	 if(is_null($obj->to)){
		$obj->to = (isset($_REQUEST['to']) ? 
			$_REQUEST['to']: '');
	 }
	 
	 /**
	 * parameter yang di set dari client
	 */
	 $obj->pq = trim($pq);
	 if(is_null($obj->pq)){
		$obj->pq = (isset($_REQUEST['pq']) ? 
			$_REQUEST['pq'] : '');
	 } 
	 
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallBarge", array($obj->from, $obj->to, $obj->pq));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }			 
	// finaly response:
	exit( json_encode($obj->rets)); 
 }

/**
 * @method 		API Manager.APIActionCallSession([ exts:string ])
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionCallSession/?exts=6000&uniqueid=78cfada1d1d5b92b1440774cc27e4d17
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionCallSession($ext = NULL, $uniq = NULL){
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");	 
	
	$obj = new stdclass();
	$obj->rets 	 = array();
	$obj->exts   = $ext; 
	$obj->uniq	 = $uniq; 
	
	// extension yang akan melakukan call 
	if(is_null($obj->exts)){
	   $obj->exts = $this->APIManager->APIParam('exts');  
	}
	
	if(is_null($obj->uniq)){
	   $obj->uniq = $this->APIManager->APIParam('uniqueid');  
	}
	  
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionCallSession", array($obj->exts, $obj->uniq));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if( is_object($obj->o) &&( isset($obj->o->event))){
		 $obj->rets = (array)$obj->o;
		
	 }		 
	// finaly response:
	exit( json_encode($obj->rets));  
 }
 
  
/**
 * @method 	 API Manager.APIActionSipAuth
 * @url 	 http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionSipAuth/?action=Login&username=ROOT&password=1234&type=API
 * @param	 [$action, $username, $password]
 * 
 */ 
function APIActionSipAuth($action= NULL, $username = NULL, $password = NULL, $type= NULL ){
	header( 'Access-Control-Allow-Origin: *' );
	header( 'Content-Type: application/json; charset=UTF-8');
	
	// stdclass
	$obj 			= new stdclass();
	$obj->action 	= $action;  
	$obj->username  = $username;
	$obj->password 	= $password;
	$obj->type 		= $type;
	$obj->allow     = NULL;
	$obj->skip 		= 0; 
	
	// check header 
	if( is_null($obj->allow)){
	   $obj->allow = $this->APIManager->APIHeader('User-Agent');
	} 
	
	// extension yang akan melakukan call 
	if( is_null($obj->action) ){
	  $obj->action = $this->APIManager->APIParam( 'action' );
	}
	
	// username called varchar / ID	
	if( is_null($obj->username) ){
	  $obj->username = $this->APIManager->APIParam( 'username' );
	} 	
		
	if( is_null($obj->password)){
	  $obj->password = $this->APIManager->APIParam( 'password' ); 
	} 	 
	 
	if( is_null($obj->type)){
	  $obj->type= $this->APIManager->APIParam( 'type' ); 
	} 	
	 
	// buat pesan untuk di kirim ke cti manager 
	 $obj->q = $this->APIManager->APITrace("APIActionSipAuth", array($obj->action, $obj->username, $obj->password));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		while( !$obj->skip){
			// if user aksess Allow used POSTMAN
			$obj->q= NULL; 
			if($obj->type &&(!strcmp($obj->type,'API'))){
				$obj->rets = (array)$obj->o; 
				break;
			} 
			
			// if user aksess Allow used MINISIP
			if($obj->type &&(!strcmp($obj->type,'SIP'))){
				// end check data reponse "default"
				$obj->rets = array(
					'error'  => 1,
					'message' => $obj->o->APIMessage
				); 
				
				// Not Allowed 
				if($obj->allow &&(strcmp($obj->allow, 'SMiniSIP'))){
					$obj->rets = array(
						'error' => 1,
						'message' => 'Your_Access_Is_Denied'
					); 
					break;
				}
				
				// if object Response NOK
				if($obj->o->APIResponse &&(!strcmp($obj->o->APIResponse,'NOK'))){
					$obj->rets = array(
						'error' => 1,
						'message' => $obj->o->APIMessage
					);
					break;
				} 
				
			   /** 
				Login User Di lakukan Untuk mendapatakan configuration Extension 
				Secara dynamic yang kemudian akan di tampilkan di sisi softphone */
				
				if((!strcmp($obj->o->APIAppAction, 'Login')) &&(!strcmp($obj->o->APIResponse,'OK'))){
					$obj->rets = array(
						'error'   => 0,
						'message' => $obj->o->APIMessage,
						'data'    => array(
							'realApiAddress'	=> $obj->o->APISipRealHost,
							'server' 			=> $obj->o->APISipServer,
							'proxy' 			=> $obj->o->APISipProxy,
							'username' 			=> $obj->o->APISipUsername,
							'domain' 			=> $obj->o->APISipDomain,
							'authID' 			=> $obj->o->APISipAuthID,
							'password' 			=> $obj->o->APISipPassword,
							'displayName' 		=> $obj->o->APISipDisplay,
							'SRTP' 				=> $obj->o->APISipSRTP,				 
							'transport' 		=> $obj->o->APISipTransport,		 
							'publish' 			=> $obj->o->APISipPublish,
							'ICE' 				=> $obj->o->APISipICE,
							'voicemailNumber' 	=> $obj->o->APISipVoiceMailNumber,
							'allowRewrite' 		=> $obj->o->APISipAllowRewrite, 
							'updateInterval' 	=> $obj->o->APISipUpdateInterval
						)
					);
					
					// Dan register buat pesan untuk di kirim ke cti manager
					if( $obj->o->APISipAuthID ){
						$obj->q = $this->APIManager->APITrace("APIActionRegister", array($obj->o->APISipUsername, $obj->o->APIAppUsername, $obj->o->APIAppPassword));
						$obj->o = $this->APIManager->APIWrite($obj->q);
						if(is_object($obj->o) &&(isset($obj->o->APIEvent))){
						   $obj->o= NULL;	
						   break;
						}
					}
					break;
				} 
				
			   /** 
				Jika Action Logout dari MiniSIP Maka Unregister data yang ada di sip Phone 
				Dengan Melakukan Update Null di sisi extension yang di gunakan agar bisa di gunakan
				Oleh User Yang Lain */
				
				if((!strcmp($obj->o->APIAppAction, 'Logout')) &&(!strcmp($obj->o->APIResponse,'OK'))){
					$obj->q = $this->APIManager->APITrace("APIActionUnregister", array( $obj->o->APISipUsername,$obj->o->APIAppUserId,$obj->o->APIAppUsername));
					$obj->o = $this->APIManager->APIWrite($obj->q);
					if(is_object($obj->o) &&(isset($obj->o->APIEvent))){
						break;
					}		 
			   } 
			}
			// Break skip
			++ $obj->skip;
		} 
	 }		 
	 
	// finaly response:
	exit( json_encode($obj->rets) ); 
	
	
}	
 
/**
 * @method 	 API Manager.APIActionRegister 
 * @url 	 http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionRegister/?exts=6000&username=ROOT&password=1234
 * @param	 [$exts, $username, $password]
 * 
 */
 function APIActionRegister($exts = NULL, $userid=0, $username = NULL, $password= NULL){
	header( 'Access-Control-Allow-Origin: *' );
	header( 'Content-Type: application/json; charset=UTF-8' );
	
	// stdclass
	$obj 			= new stdclass();
	$obj->exts 		= $exts;  
	$obj->username  = $username;
	$obj->password 	= $password;
	
	// extension yang akan melakukan call 
	if( is_null($obj->exts) ){
		$obj->exts = $this->APIManager->APIParam( 'exts' );
	}
	
	// username called varchar / ID	
	if( is_null($obj->username) ){
		$obj->username = $this->APIManager->APIParam( 'username' );
	 } 	
		
	if(is_null($obj->password)){
		$obj->password = $this->APIManager->APIParam( 'password' ); 
	 } 	 
	 
	// buat pesan untuk di kirim ke cti manager
	 $obj->q = $this->APIManager->APITrace("APIActionRegister", array( $obj->exts, $obj->username, $obj->password));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }		 
	// finaly response:
	exit( json_encode($obj->rets) ); 
 }

 
 
/**
 * @method 		API Manager.APIActionUnregister 
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionUnregister/?exts=6000&userid=193&username=ROOT
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionUnregister($exts = NULL, $userid=0, $username = NULL){
	header('Access-Control-Allow-Origin: *');
	header('Content-Type: application/json; charset=UTF-8');	 
	
	$obj 			= new stdclass();
	$obj->rets 		= array();
	$obj->exts 		= $exts;  
	$obj->userid 	= $userid;
	$obj->username 	= $username; 
	
	// extension yang akan melakukan call 
	if(is_null($obj->exts)){
		$obj->exts = $this->APIManager->APIParam('exts'); 
	}
	  
	// userid called varchar / ID 	
	if(!$obj->userid){
		$obj->userid = $this->APIManager->APIParam('userid'); 
	} 	
	
	// username called varchar / ID	
	if(is_null($obj->username)){
		$obj->username = $this->APIManager->APIParam('username'); 
	} 	
		
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionUnregister", array($obj->exts, $obj->userid, $obj->username));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if( is_object($obj->o) &&($obj->o->event)){
		 $obj->rets = (array)$obj->o; 
	 }		 
	 
	 // finaly response:
	 exit( json_encode($obj->rets) ); 
 }
 
 
/**
 * @method 		API Manager.APIActionUserSession
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionUserSession/?exts=6000
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionUserSession($ext = NULL, $status = NULL){
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");	 
	
	$obj = new stdclass();
	$obj->rets 	 = array();
	$obj->exts   = $ext; 
	
	// extension yang akan melakukan call 
	if(is_null($obj->exts)){
	   $obj->exts = $this->APIManager->APIParam('exts');  
	}
	  
	// buat pesan untuk di kirim ke CTI Manager
	 $obj->q = $this->APIManager->APITrace("APIActionUserSession", array($obj->exts)); 
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if( is_object($obj->o) &&( isset($obj->o->event))){
		 $obj->rets = (array)$obj->o;
		
	 }		 
	// finaly response:
	exit( json_encode($obj->rets));  
 }
 
 
 /**
 * @method 		API Manager.APIActionSkill
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionSkill/?exts=6000&skill=1
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionSkill($ext = NULL, $skill = NULL){
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");	 
	
	$obj = new stdclass();
	$obj->rets 	 = array();
	$obj->exts   = $ext;
	$obj->skill = $skill; 
	
	
	// extension yang akan melakukan call 
	if(is_null($obj->exts)){
	   $obj->exts = $this->APIManager->APIParam('exts');  
	}
	// no tujuan yang akan di dial 
	if(is_null($obj->skill)){
		$obj->skill = $this->APIManager->APIParam('skill'); 
	}  
	// buat pesan untuk di kirim ke CTI Manager
	// APIActionSkill
	 $obj->q = $this->APIManager->APITrace("APIActionSkill", array($obj->exts, $obj->skill));  
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if( is_object($obj->o) &&( isset($obj->o->event))){
		 $obj->rets = (array)$obj->o;
		
	 }		 
	// finaly response:
	exit( json_encode($obj->rets));  
 }

 /**
 * @method 		API Manager.APIActionStatus
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionStatus/?exts=6000&status=1
 * @param		[$exts, $destnum, $custid]
 */
 function APIActionStatus($ext = NULL, $status = NULL){
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");	 
	
	$obj = new stdclass();
	$obj->rets 	 = array();
	$obj->exts   = $ext;
	$obj->status = $status; 
	
	// extension yang akan melakukan call 
	if(is_null($obj->exts)){
	   $obj->exts = $this->APIManager->APIParam('exts');  
	}
	// no tujuan yang akan di dial 
	if(is_null($obj->status)){
		$obj->status = $this->APIManager->APIParam('status'); 
	}  
	// buat pesan untuk di kirim ke CTI Manager
	// APIActionStatus
	 $obj->q = $this->APIManager->APITrace("APIActionStatus", array($obj->exts, $obj->status));
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if( is_object($obj->o) &&( isset($obj->o->event))){
		 $obj->rets = (array)$obj->o;
	 }		 
	// finaly response:
	exit( json_encode($obj->rets));  
 }
 
/**
 * @method 		API Manager.APIActionHangup 
 * @url 		http://192.168.10.236/smart-btn-collection/index.php/Manager/APIActionHangup/?exts=6000
 * @param		[$from, $to]
 */
 function APIActionHangup($exts = NULL) {
	header("Access-Control-Allow-Origin: *");
	header("Content-Type: application/json; charset=UTF-8");
	$obj = new stdclass();
	$obj->exts = $exts;
	
	// get extension from reques process 
	if(is_null( $obj->exts )){
		$obj->exts = $this->APIManager->APIParam('exts'); 
	} 
	
	// buat pesan untuk di kirim ke CTI Manager 
	 $obj->q = $this->APIManager->APITrace("APIActionHangup", array($obj->exts));
	 
	 $obj->o = $this->APIManager->APIWrite($obj->q); 
	 if(is_object($obj->o) &&(isset($obj->o->event))){
		$obj->rets = (array)$obj->o; 
	 }		 
	// finaly response:
	exit( json_encode($obj->rets) ); 
 }
 // endSource :
}
?>