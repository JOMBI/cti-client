<?php
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */ 
class M_ModCtiLogin extends EUI_Model {
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */
  
 private static $Instance = null; 
 public static function &Instance() {
  if( is_null(self::$Instance)) {
	self::$Instance = new self();	
  }
  return self::$Instance;
} 
/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */ 
function __construct() { } 

/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */ 
function _getUserAvail( $UserName=NULL ) {
	$resultFetch = false;
	$resultArray = self::_setUserLogin($UserName);
	if(count($resultArray)> 0 &&(!is_null($resultArray))){
		$resultFetch  = (array)$resultArray;
	}
	// return 
	return (array)$resultFetch;
}


/** 
 * @properties  [description]
 * @Methode	 	[@param]		[construtor class]
 * @param 		[uknown]
 * @author		[uknown]
 */ 
function _setUserLogin( $UserName = NULL ) 
{
	$_conds = null;
	$sql = " select * from kc_agent a where a.userid ='$UserName'";
	$qry = $this->db->query($sql);
	if($qry &&($qry->num_rows()>0))
	foreach($qry->result_assoc() as $rows ) {
		foreach( $rows as $keys => $values ) {
			$_conds[$keys] = $values;
		}
	}
	
	return $_conds;
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _setCtiPBX($UserName) {
	// set data from user agent:
	$UserAvail = (array)self ::_getUserAvail($UserName);
	$agentId = (CK()->find('agentId') ? CK()->get('agentId') : $UserAvail['id'] );
	$agentExt = (CK()->find('agentExt') ? CK()->get('agentExt') : 0 );
	$agentIpAddress  = RealIpAddress();
	
	// cari agent_yang login terlebih dahulu untuk dapet modulnya :
	$sql = " SELECT a.id, a.userid , a.name, a.password, a.domain, a.occupancy, now() 'login_time', a.agent_group , a.mod as modul
			 FROM kc_agent a, kc_agent_group b  
			 WHERE a.id = '$agentId' and a.agent_group = b.id";
			 
	$qry = $this->db->query($sql);
	if($qry &&($row = $qry -> result_first_assoc())) {
		CK()->set('ctiAgentId',$row['id']); 				// agent_uid 
		CK()->set('ctiAgentName',$row['userid']); 			// agent_name 
		CK()->set('ctiAgentPassword', $row['password']); 	// agent_password 
		CK()->set('ctiAgentGroup',$row['agent_group']); 	// agent_group 
		CK()->set('ctiModulId',$row['modul']);  			// agent_modul 
		CK()->set('ctiProjectId',$row['domain']); 			// agent_project 
	 }
	 else {
		echo "<script> $(document).ready(function(){ 
			$('#cmdAgentStatus').html('Agent not found'); 
		});</script> ";
	}
	
	// setal di set session ambil berikut ini untuk modul ==  1 ambil dari socket :
	$Session = CK();
	$ctiModulUser = CK()->get('ctiModulId');
	$ctiUserlogId = CK()->get('ctiAgentId');
	
	if(!strcmp($ctiModulUser,1) &&($agentIpAddress)){
	  $Session->set('ctiAgentAddress',$agentIpAddress);
	  if($agentIpAddress &&(strlen($agentExt)>2)){
		  $this->db->reset_write();
		  $this->db->set("ext_location",$agentIpAddress );
		  $this->db->where("ext_number", $agentExt);
		  $this->db->update("kc_extension_agent"); 
	  }
	}
	// untuk Modul jenis atau API IP addresss akan di manipulasi dengan manambahkan parametr agent_ID
	else if(!strcmp($ctiModulUser,2)){
		// jika session extension sudah ADA:
		if(strlen($agentExt)>2){
			$agentIpAddress = sprintf("%s.%s", $agentIpAddress, $agentExt);
			
			// ini untuk methode dynamicIp tanpa harus settings IP di config Extension -nya :
			$this->db->reset_write();
			$this->db->set("ext_location",$agentIpAddress );
			$this->db->where("ext_number", $agentExt);
			$this->db->update("kc_extension_agent"); 
		}
		// jika extension saat login tidak ada maka tetap concate dengan USERID saja :
		if(strlen($agentExt)<2){
			$agentIpAddress = sprintf("%s.%s", $agentIpAddress, $ctiUserlogId);	
		}
		CK()->set('ctiAgentAddress',$agentIpAddress);
	}
	
	// check data By agent: 
	if(!$UserAvail){
		exit("Invalid agent id");
	}
	
	if($agentExt) {
		$dynamicIp = true; CK()->set('ctiAgentDynamic',1); 
		CK()->set('ctiAgentExtens',$agentExt); 
		$sql = " select a.pbx from kc_extension_agent a where a.ext_number = '$agentExt'";
		
		// get data from Here: 
		$qry = $this->db->query($sql);
		if($qry) foreach( $qry ->result_assoc() as $rows ) {
			$pbxid = $rows['pbx'];
		}	
	}
	
	// ambil extension :
	else if(!$agentExt) {
		$sql = " select a.ext_number, a.pbx from kc_extension_agent a where a.ext_location = '$agentIpAddress'";
		$qry  = $this->db->query($sql);
		if( $rows = $qry -> result_first_assoc()){
			CK()->set('ctiAgentExtens',$rows['ext_number']);
			$pbxid= $rows['pbx'];
		} 
		// then get from here :
		else {
			print "
					<script> 
						$(document).ready(function(){
							Ext.Cmp(\"cmdAgentStatus\").setText(\"Ip-Address not registered [$agentIpAddress]\");
						});
				</script>";
		}
	}	
	// get configuration from server by IP && Extension
	$config = $this->_getPbxServerConfig($pbxid);
	if(is_object($config) &&($config->find('ctiServerHost'))){ // set data on session :
		CK()->set('ctiServerAddress',$config->field('ctiServerHost'));
		CK()->set('ctiServerPort',$config->field('ctiServerPort'));
		CK()->set('ctiServerUdp',$config->field('ctiServerUdp'));
		CK()->set('ctiServerWss',$config->field('ctiServerWss'));
		CK()->set('ctiPbxTac',$config->field('ctiServerTac'));
		
	}
	// inavalid settings : 
	else{
		echo "<script> $(document).ready(function(){ $('#idCallStatus').html('Invalid settings'); });</script> ";
	} 
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _get_instance_id($pbxId)
{	
	$instanceId = 0;

	$sql = "SELECT instance_id FROM kc_settings  WHERE set_modul='cti' AND set_name='pbx.id' AND set_value='$pbxId'";
	$qry = $this -> db -> query($sql);
	if(!$qry -> EOF() ) {
		$instanceId = $qry -> result_singgle_value();
		
	}
	
	return $instanceId;
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _get_app_setting($instanceId='')
{	
 /* read app settings */
 
	$sql = "SELECT set_name, set_value FROM kc_settings WHERE set_modul = 'agent' AND instance_id='$instanceId' ";
	$qry = $this -> db -> query($sql);
	foreach( $qry ->result_assoc() as $rows )
	{
		if( $rows['set_name']=='server.host') {
			$this->EUI_Session -> _set_session('ctiIp',$rows['set_value']);
		}	
		else if( $rows['set_name']=='server.port') {
			$this->EUI_Session -> _set_session('ctiUdpPort',$rows['set_value']);
		}
	}
}

/**
 * [HBC_COLLECTION_2019]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _get_pbx_setting( $pbxId )
{			
	$sql = "SELECT set_name, set_value FROM kc_pbx_settings WHERE pbx = '$pbxId'";
	$qry = $this -> db -> query($sql);
	foreach( $qry ->result_assoc() as $rows )
	{
		if($rows['set_name'] == 'tac') {
			$this->EUI_Session -> _set_session('pbxTAC',$rows['set_value']);
		}
	}
}

/**
 * @class  [@constructor]
 * @param  [type] $CustomerId [description]
 * @return [type]             [description]
 */
function _getPbxServerConfig( $pbxId = 0 ) {	
	$resultArray = false;
	$sql = sprintf("select b.set_kode, b.set_name, b.set_value from kc_pbx_settings a 
					left join kc_settings b on a.pbx_serv=b.set_kode
					where a.pbx_kode = %s and a.pbx_name='kawani.pbx.host'", $pbxId);
					
	$qry = $this->db->query( $sql );
	if( $qry && $qry->num_rows() > 0 ) 
	foreach( $qry->result_assoc() as $row ){
		// get server IP 
		if( !strcmp($row['set_name'], 'kawani.cti.host' )){
			$resultArray['ctiServerHost'] = $row['set_value'];
		}
		// get server Port 
		if( !strcmp($row['set_name'], 'kawani.cti.port' )){
			$resultArray['ctiServerPort'] = $row['set_value'];
		}
		// get server Udp 
		if( !strcmp($row['set_name'], 'kawani.cti.udp' )){
			$resultArray['ctiServerUdp'] = $row['set_value'];
		}
		
		// get Server Tac :
		if( !strcmp($row['set_name'], 'kawani.cti.tac' )){
			$resultArray['ctiServerTac'] = $row['set_value'];
		}
		
		// untuk connect via ssl / wss 
		if( !strcmp($row['set_name'], 'kawani.cti.wss' )){
			$resultArray['ctiServerWss'] = $row['set_value'];
		}
	}	
	return new EUI_Object($resultArray);
}
/*
 * @ def 		: GET Instance ID  
 * -----------------------------------------
 *
 * @ params  	: post & definition paymode 
 * @ return 	: void(0)
 */
 
function _get_manager_setting($instanceId='')
{
	/* manager settings */

	$_array = array();
	$sql = "SELECT set_name, set_value FROM kc_settings WHERE set_modul = 'manager' AND instance_id='$instanceId' ";
	$qry = $this -> db -> query($sql);
	
	foreach( $qry ->result_assoc() as $rows )
	{
		if($rows['set_name'] == "server.host")
		{
			$_array['managerHost'] = $rows['set_value'];
		}
		else if ($rows['set_name'] == "server.port"){
			$_array['managerPort'] = $rows['set_value'];
		}
			  
	}
	
	return $_array;
}

	

}

?>
