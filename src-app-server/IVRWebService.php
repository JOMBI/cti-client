<?php 
class WsApi Extends EUI_Controller{
function __construct(){
	parent::__construct();
}	
// dataCustomer
function getFetchAccount( $account = NULL ){
	$result = false;
	$sql = sprintf("
	SELECT 
	a.deb_id as Customer_ID,
	a.deb_cardno as Customer_CardNo,
	a.deb_id_number as Customer_CardID,
	a.deb_name as Customer_Name,
	date_format(a.deb_last_paydate,'%%d-%%m-%%Y') as Last_Payment_Date,
	a.deb_last_pay as Paid_Amount,
	a.deb_total_pay as Total_tagihan,
	a.deb_tenor as Tenor 
	FROM t_gn_debitur a 
	WHERE a.deb_cardno = '%s'", $account);
	// echo $sql;
	$qry = $this->db->query($sql);
	if($qry &&($qry->num_rows()>0)){
		$result = (array)$qry->result_first_assoc(); 
	}		
	return $result;
}
// dataCustomer
	/*
		'Gender'					=> 'Gender',
		'Marital_Status'			=> 'Marital_Status',
		'Place_of_birth'			=> 'Place_of_birth',
		'Date_of_birth'				=> 'Date_of_birth',
		'Job_Type'					=> 'Job_Type',
		'Industry_Type'				=> 'Industry_Type',
		'Email'						=> 'Email',
		'Mother_Maiden_Name'		=> 'Mother_Maiden_Name',
		'Spouse'					=> 'Spouse',
		'Guarantor'					=> 'Guarantor',
		'Mobile_phone'				=> 'Mobile_phone',
		'Home_Status'				=> 'Home_Status',
		'Stay_Since'				=> 'Stay_Since',
		'Home1'						=> 'Home1',
		'Home2'						=> 'Home2',
		'Office1'					=> 'Office1',
		'Office2 '					=> 'Office2',
		'Residence_Address'			=> 'Residence_Address',
		'Mailing_Address'			=> 'Mailing_Address',
		'Company_Name'				=> 'Company_Name',
		'Office_Address'			=> 'Office_Address',
		'Emergency_Contact_Name'	=> 'Emergency_Contact_Name',
		'EC_Address'				=> 'EC_Address',
		'EC_Mobile_Phone'			=> 'EC_Mobile_Phone',
		'EC_Relationship'			=> 'EC_Relationship',
		'Kecamatan'					=> 'Kecamatan',
		'Kelurahan'					=> 'Kelurahan',
		'City'						=> 'City',
		'Surveyor_Name'				=> 'Surveyor_Name',
		*/
function dataCustomer($account = NULL){
header('Content-Type: application/json');
 $res = $this->getFetchAccount($_REQUEST['Account']);
	if(!is_array($res)) exit(0);
	else if(is_array($res)) {
		$response  = array(
		'Customer_ID'		=> $res['Customer_ID'],
		'Customer_Name'		=> $res['Customer_Name'],
		'ID_Number'			=> $res['Customer_CardID'], 
		'Branch_ID'			=> $res['Branch_ID'],
		'Branch_Name'		=> $res['Branch_Name'] 
	   ); 
		exit( json_encode($response) );
	
	}
	
// get data : OK 	
	
}
// dataAggingDetail
function dataAggingDetail($account = NULL){ 
header('Content-Type: application/json');
	$res = $this->getFetchAccount($_REQUEST['Account']);
	if(!is_array($res)) exit(0);
	else if(is_array($res)) {
		$response  = array(
			'Customer_Name'				=> $res['Customer_Name'],
			'Contract_Number' 			=> $res['Customer_CardNo'],
			'Customer_ID' 				=> $res['Customer_ID'],
			'Last_Payment_Date' 		=> $res['Last_Payment_Date'],
			'Paid_amount' 				=> $res['Paid_Amount'],
			'Due_date' 					=> $res['Last_Payment_Date'],
			'Total_tagihan' 			=> $res['Total_tagihan'],
			'Installment_No' 			=> $res['Tenor'], 
			'OD_Days' 					=> 'OD_Days',
			'Installment_tagihan' 		=> 'Installment_tagihan',
			'Last_Payment_Via' 			=> 'Last_Payment_Via',
			'Collector_Name' 			=> 'Collector_Name',
			'Collector_ID' 				=> 'Collector_ID',
			'Bucket' 				 	=> 'Bucket',
			'OD_Days2' 				  	=> 'OD_Days2',
			'Late_Charge' 			  	=> 'Late_Charge',
			'Outstanding_Late_Charge' 	=> 'Outstanding_Late_Charge',
			'Oustanding_Principal' 	  	=> 'Oustanding_Principal',
			'Outstanding_Interest' 	  	=> 'Outstanding_Interest' );
		exit( json_encode($response) );
	}
}

// data financial :
function dataFinancing($account = NULL){ 
header('Content-Type: application/json');
	$res = $this->getFetchAccount($_REQUEST['Account']);
	if(!is_array($res)) exit(0);
	else if(is_array($res)) {
		$response = array(
			'Financing_Product'	=> 'Financing_Product',
			'Total_OTR'			=> 'Total_OTR',
			'Down_Payment'		=> 'Down_Payment',
			'Tenor'				=> $res['Tenor'] ,
			'Installment'		=> $res['Total_tagihan'],
			'Total_Principal'	=> 'Total_Principal',
			'Total_Interest'	=> 'Total_Interest',
			'DO_Date'			=> 'DO_Date',
			'Due_Date'			=> $res['Last_Payment_Date'] );
		exit( json_encode($response) );
	}
}

// data financial :
function dataAssetDetail($account = NULL){
header('Content-Type: application/json');	
	$res = $this->getFetchAccount($_REQUEST['Account']);
	if(!is_array($res)) exit(0);
	else if(is_array($res)) { 
		$response = array(
			'Contract_Number'			=> 'Contract_Number',
			'Asset_Type'				=> 'Asset_Type',
			'no_rangka'					=> $res['Customer_Name'],
			'no_mesin'					=> 'no_mesin',
			'Color'						=> 'Color',
			'License_Plate'				=> 'License_Plate',
			'Year'						=> 'Year',
			'Asset_Registration_Name'	=> 'Asset_Registration_Name',
			'Used_New'					=> 'Used_New',
			'Supplier_Name'				=> 'Supplier_Name' );	
		exit( json_encode($response) );
  }
 }	
}
?>