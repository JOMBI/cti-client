<?php 
/**
 * Kawani CTI Server @version 4.6.1
 * An open source application used under PHP 5.1.6 or newer
 * system operation unix centos
 
 * @package			Kawani CTI Project 
 * @since			2018-01-01
 * @revsion			2019-03-20
 * @filesource  	MediaChannel.php
 * @link 			https://www.voip-info.org/asterisk-cmd-extenspy/
 *
 */
 
 if(!defined('WEB_API_HOST')) define('WEB_API_HOST', '127.0.0.1');
 if(!defined('WEB_API_PORT')) define('WEB_API_PORT', 16000);
 
 
/**
 * Define All Process for IVR state 
 *
 */
 
 if(!defined('WEB_IVR_REQUEST')) define('WEB_IVR_REQUEST', 500);
 if(!defined('WEB_IVR_TIMEOUT')) define('WEB_IVR_TIMEOUT', 501);
 if(!defined('WEB_IVR_CONFIRM')) define('WEB_IVR_CONFIRM', 502);
 if(!defined('WEB_IVR_STARTED')) define('WEB_IVR_STARTED', 503);
 if(!defined('WEB_IVR_PROCESS')) define('WEB_IVR_PROCESS', 504);
 if(!defined('WEB_IVR_CLOSED'))  define('WEB_IVR_CLOSED',  505);
 
/**
 * Debug Process
 *
 UPDATE serv_mail_inbox a SET a.EmailAssignUID  = '0', a.EmailAssignDataId = 0 , a.EmailUpdated = NULL;
 UPDATE serv_sms_inbox a SET a.UID  = '0', a.Agent = 0 , a.Updated = NULL;
 *
 */
 
 // example class "MediaChannel"
 class MediaChannel extends EUI_Controller{
	// __construct	
	function __construct(){
		parent::__construct();	
	}

	// index
	function index(){ }
	
   /**
	* @method 		event 
	* @desc			update table after response from CTI triger sent to WS browser  
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/receive
	* @param		[]
	*/
	function event(){
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   // default Response from API 
	   $obj= new stdClass();
	   $obj->response = array(
		 'event'   => 'receive',
		 'error'   => 1,
		 'message' => 'failed',
		 'data'    => array(
			  'id'   => NULL,
			  'uid'  => NULL,
			  'type' => NULL 	
		  )	
	   );
	   
	   // break event $
	   $skip = 0;
	   while( !$skip ){
		
		// check validation proceess   
		 if( !isset($_REQUEST['action'] )){
		    $obj->response['error'] = 1;
		    $obj->response['message'] = "Wrong Parameter";
				break;
		 }
		 /**
		    post = IVR 
			action:ctiMediaAction
			&type:ctiMediaType&id:ctiMediaId
			&uid:ctiUniqueId
			&from:ctiAccount
			&contact:ctiContact
			&person:ctiPerson
			&updated:ctiUpdated
			&expired:ctiExpired
		 */
		
		// print_r($_REQUEST);
 
		// then if have new proces then will updated by key
		 $obj->out 			 = new stdClass();
		 $obj->out->Id       = $_REQUEST['id'];
		 $obj->out->Action   = $_REQUEST['action'];
		 $obj->out->Type     = $_REQUEST['type'];
		 $obj->out->From     = $_REQUEST['from'];
		 $obj->out->Uid 	 = $_REQUEST['uid'];
		 $obj->out->UserId   = $_REQUEST['userid'];
		 $obj->out->UserName = $_REQUEST['username'];
		 $obj->out->Updated  = $_REQUEST['updated'];
		 $obj->out->Expired	 = $_REQUEST['expired'];
		 $obj->out->Contact	 = $_REQUEST['contact']; 
		 $obj->out->Person 	 = $_REQUEST['person']; 
		 $obj->out->Link  	 = $_REQUEST['link'];
		 $obj->out->TrackId  = $_REQUEST['trackid'];
		 $obj->out->CallId	 = $_REQUEST['callid'];
		 $obj->out->VdnGroup = $_REQUEST['vdngroup']; 
		 
		// check if type of channel update "Mail" 
		 if($obj->out->Type &&(!strcmp($obj->out->Type, 'Mail'))){
			
		   $this->db->reset_write();
		   $this->db->set('EmailAssignDataId', $obj->out->UserId);
		   $this->db->set('EmailUpdated', $obj->out->Updated);
		   $this->db->where('EmailInboxId', $obj->out->Id);
		   $this->db->where('EmailAssignUID', $obj->out->Uid);
		   
		   if($this->db->update('serv_mail_inbox') 
		   &&($this->db->affected_rows()>0)){
			  $obj->response['error'] = 0;
			  $obj->response['message'] = "success"; 
			  $obj->response['data']['id'] = $obj->out->Id;
			  $obj->response['data']['uid'] = $obj->out->Uid;
			  $obj->response['data']['type'] = $obj->out->Type;
			  
				break;
		   }
		 }
		
		// check if type of channel update "Sms" 
		 if($obj->out->Type &&(!strcmp($obj->out->Type, 'Sms'))){
			 
		    $this->db->reset_write();
		    $this->db->set('Agent', $obj->out->UserId);
		    $this->db->set('Updated', $obj->out->Updated);
		    $this->db->where('SmsId', $obj->out->Id);
		    $this->db->where('UID', $obj->out->Uid); 
			$this->db->where('IsRead',0); 
			
		    if($this->db->update('serv_sms_inbox') 
			&&($this->db->affected_rows()>0)){
			  $obj->response['error'] = 0;
			  $obj->response['message'] = "success"; 
			  $obj->response['data']['id'] = $obj->out->Id;
			  $obj->response['data']['uid'] = $obj->out->Uid;
			  $obj->response['data']['type'] = $obj->out->Type;
				break;
		    }
		 }
		 // IVR => (500:request|501:timeout|502:confirm|503:start|504:process|505:drop)
		 /*
		  
			 if(!defined('WEB_IVR_REQUEST')) define('WEB_IVR_REQUEST', 500);
			 if(!defined('WEB_IVR_TIMEOUT')) define('WEB_IVR_TIMEOUT', 501);
			 if(!defined('WEB_IVR_CONFIRM')) define('WEB_IVR_CONFIRM', 502);
			 if(!defined('WEB_IVR_STARTED')) define('WEB_IVR_STARTED', 503);
			 if(!defined('WEB_IVR_PROCESS')) define('WEB_IVR_PROCESS', 504);
			 if(!defined('WEB_IVR_CLOSED'))  define('WEB_IVR_CLOSED',  505);
					 */
		 // check if type of channel update "IVR" 
		 if($obj->out->Type &&(!strcmp($obj->out->Type, 'ivr'))){
			
			// WEB_IVR_REQUEST
			if(!strcmp($obj->out->Action, WEB_IVR_REQUEST)){
			    $this->db->reset_write();
			    $this->db->set('vbs_cli_custid',	$obj->out->Id);
			    $this->db->set('vbs_cli_account', 	$obj->out->From);
			    $this->db->set('vbs_cli_contact', 	$obj->out->Contact);
			    $this->db->set('vbs_cli_person', 	$obj->out->Person);
			    $this->db->set('vbs_cli_confirm', 	$obj->out->Link);
				$this->db->set('vbs_cli_expired',	$obj->out->Expired);
			    $this->db->set('vbs_cli_status', 	$obj->out->Action);
			    $this->db->set('vbs_cli_session', 	$obj->out->Uid);
				$this->db->set('vbs_cli_vdngroup', 	$obj->out->VdnGroup); 
				$this->db->set('vbs_cli_created', 	'now()', false);
			    $this->db->set('vbs_cli_updated', 	'now()', false);
				$this->db->set('vbs_cli_callid', 	NULL);
			    $this->db->insert('kc_vbs_client');
				break;
			}
			
			// WEB_IVR_TIMEOUT
			if(!strcmp($obj->out->Action, WEB_IVR_TIMEOUT)){
				$this->db->reset_write();
				$this->db->set('vbs_cli_updated', 'now()', false);
				$this->db->set('vbs_cli_status', $obj->out->Action); 
				$this->db->where('vbs_cli_session', $obj->out->Uid);
				$this->db->update('kc_vbs_client');
				break;
			}
			
			// WEB_IVR_CONFIRM
			if(!strcmp($obj->out->Action, WEB_IVR_CONFIRM)){
				$this->db->reset_write();
				$this->db->set('vbs_cli_trackid', $obj->out->TrackId);
				$this->db->set('vbs_cli_updated', $obj->out->Updated);
				$this->db->set('vbs_cli_status', $obj->out->Action); 
				$this->db->where('vbs_cli_session', $obj->out->Uid);
				$this->db->update('kc_vbs_client');
				break;
			}
			
			// WEB_IVR_STARTED
			if(!strcmp($obj->out->Action, WEB_IVR_STARTED)){
				$this->db->reset_write();
				$this->db->set('vbs_cli_callid', $obj->out->CallId);
				$this->db->set('vbs_cli_trackid', $obj->out->TrackId);
				$this->db->set('vbs_cli_updated', $obj->out->Updated);
				$this->db->set('vbs_cli_status', $obj->out->Action); 
				$this->db->where('vbs_cli_session', $obj->out->Uid);
				$this->db->update('kc_vbs_client');
				break;
			}
			
			// WEB_IVR_PROCESS
			if(!strcmp($obj->out->Action, WEB_IVR_PROCESS)){
			   break;
			}
			
			// WEB_IVR_CLOSED
			if(!strcmp($obj->out->Action, WEB_IVR_CLOSED)){
				$this->db->reset_write();
				$this->db->set('vbs_cli_updated', $obj->out->Updated);
				$this->db->set('vbs_cli_status', $obj->out->Action); 
				$this->db->where('vbs_cli_session', $obj->out->Uid);
				$this->db->update('kc_vbs_client');
				break;
			}
			
			// local response on console:
			
			$obj->response['error'] 		= 0;
			$obj->response['message'] 		= 'success'; 
			$obj->response['data']['id'] 	= $obj->out->Id;
			$obj->response['data']['uid'] 	= $obj->out->Uid;
			$obj->response['data']['type'] 	= $obj->out->Type;
		 }
		 
		 // skip and break process;
		 ++ $skip;
	   }
	   // finaly 
	   exit( json_encode($obj->response) );
	}
	
   /**
	* @method 		Find 
	* @desc			Find From client after triger by CTI 
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/find
	* @param		[]
	*/
	function find(){
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   $response = array(
		'event' => 'find',
		'error' => 1,
		'message' => 'failed',
		'data' => array( 
			'total'  => 0
		  )	
	   );
	   
	   $total = 0;
	   $type = $_REQUEST['type']; // type corespodensi 
	   $agentid = $_REQUEST['agentid']; // ctiAgentId
	   if($agentid &&(!strcmp($type, 'Mail'))){
		  $sql = sprintf("select count(EmailInboxId) as total from serv_mail_inbox where EmailAssignDataId = %d and EmailStatus=0 ", $agentid);
		  $qry = $this->db->query($sql);
		  if($qry)foreach($qry->result_assoc() as $row ){
			 $total += $row['total'];
		  }		   
		  // after push get data then add in 
		  if($total){
			 $response['error'] = 0;
			 $response['message'] = 'success';
			 $response['data']['total'] = $total;
 		  }
	   }
	   
	   // *find sms 
	   if($agentid &&(!strcmp($type, 'Sms'))){
		  $sql = sprintf("select count(SmsId) as total from serv_sms_inbox where Agent = %d and IsRead=0 ", $agentid);
		  $qry = $this->db->query($sql);
		  if($qry)foreach($qry->result_assoc() as $row ){
			 $total += $row['total'];
		  }		   
		  // after push get data then add in 
		  if($total){
			 $response['error'] = 0;
			 $response['message'] = 'success';
			 $response['data']['total'] = $total;
 		  }
	   }
	   // query data for response client;
	   exit( json_encode($response) );
	}
	
   /**
	* @method 		mail 
	* @desc			Push data to distrubute by CTI Acd Automaticly 	
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/mail
	* @param		[]
	*/
	function mail($WEB_API_ACTION= "APIActionAddChanEmail"){
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   $response = array(
		'event' => 'sent',
		'error' => 0,
		'message' => 'failed',
		'data' => array( )	
	   );
	   
	   // set Lib Api CTI Console from cURL connect to cti Web Api Server
	   $obj = new stdClass();
	   $obj->lib = new stdClass();
	   $obj->lib->API = sprintf("http://%s:%s/%s", WEB_API_HOST, WEB_API_PORT, $WEB_API_ACTION);
	   $obj->lib->Skip = 0;
	   while( !$obj->lib->Skip ){
		 // query from email 
		 $sql = sprintf("select * from serv_mail_inbox where EmailStatus = 0 and EmailAssignUID= '0' limit %d", 1);
		 $qry = $this->db->query($sql);
		 if($qry) foreach( $qry->result_assoc() as $row ){
			   $row['timeout'] = 120;
			   
			   // create_function cURL 
			   $obj->lib->Conn = curl_init();
			   if( !is_resource($obj->lib->Conn)){
				   $response['error'] = 1;
				   $response['message'] = "Invalid Recsource (#0)";
				   break;
			   }
			   // sent All data from this;
			   $obj->lib->Data = http_build_query( array(
				  'id' 	    => $row['EmailInboxId'],
				  'account' => $row['EmailMsgId'],
				  'contact' => $row['EmailSender'],
				  'person'  => $row['EmailSender'],
				  'timeout' => $row['timeout']				
			   ));
			   
			   // next data 
			   curl_setopt($obj->lib->Conn, CURLOPT_URL, $obj->lib->API);
			   curl_setopt($obj->lib->Conn, CURLOPT_HEADER, FALSE);
			   curl_setopt($obj->lib->Conn, CURLOPT_NOBODY, FALSE); // remove body
			   curl_setopt($obj->lib->Conn, CURLOPT_RETURNTRANSFER, TRUE);
			   curl_setopt($obj->lib->Conn, CURLOPT_POST, TRUE);
			   curl_setopt($obj->lib->Conn, CURLOPT_POSTFIELDS,  $obj->lib->Data);
			   
			   // execute data proceess on here 
			   $obj->lib->Exec = curl_exec($obj->lib->Conn);
			   $obj->lib->Info = curl_getinfo($obj->lib->Conn, CURLINFO_HTTP_CODE);
			   if( curl_errno($obj->lib->Conn) ){ 
				   $response['error'] = 1;
				   $response['message'] = curl_error($obj->lib->Conn);
				   break;
			   }
			   
			   // close cURL 
			   if( is_resource($obj->lib->Conn)){
				  curl_close($obj->lib->Conn);
			   }
			   
			   // jika nilai balikan Error == 0  
			   $response = json_decode($obj->lib->Exec, true);
			   if(is_array($response) &&( !$response['error'])){
				   if( $r = $response['data']){
					   $EmailInboxId = $r['APIId'];
					   $EmailAssignUID = $r['APIUniqueId']; 
					   
					   if( $EmailAssignUID ){
						  $this->db->reset_write();
						  $this->db->update( "serv_mail_inbox", 
							array('EmailAssignUID' => $EmailAssignUID), 
							array('EmailInboxId' => $EmailInboxId)
						  );
					   }
				   }
			   } 
		  }	
		  // end then break proc_close(0); 
		  ++ $obj->lib->Skip;
	   }
	   // return exit(0);
	   exit( json_encode($response) );
	}
	
   /**
	* @method 		sms 
	* @desc			Push data to distrubute by CTI Acd Automaticly 	
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/sms
	* @param		[]
	*/
	function sms( $WEB_API_ACTION = 'APIActionAddChanSMS' ){
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   $response = array(
		'event' => 'sent',
		'error' => 0,
		'message' => 'failed',
		'data' => array( )	
	   );
	   
	   // set Lib Api CTI Console from cURL connect to cti Web Api Server
	   $obj = new stdClass();
	   $obj->lib = new stdClass();
	   $obj->lib->API = sprintf("http://%s:%s/%s", WEB_API_HOST, WEB_API_PORT, $WEB_API_ACTION); 
	   $obj->lib->Skip = 0;
	   while( !$obj->lib->Skip ){
		 // query from email 
		 $sql = sprintf("select * from serv_sms_inbox where IsRead=0 and UID='0' limit %d", 1);
		 $qry = $this->db->query($sql);
		 if($qry) foreach( $qry->result_assoc() as $row ){
			   $row['timeout'] = 120; 
			   // create_function cURL 
			   $obj->lib->Conn = curl_init();
			   if( !is_resource($obj->lib->Conn)){
				   $response['error'] = 1;
				   $response['message'] = "Invalid Recsource (#0)";
				   break;
			   }
			   // sent All data from this;
			   $obj->lib->Data = http_build_query( array(
				  'id' 	    => $row['SmsId'],
				  'account' => $row['MasterId'],
				  'contact' => $row['Sender'],
				  'person'  => $row['Sender'],
				  'timeout' => $row['timeout']				
			   ));
			   
			   // next data 
			   curl_setopt($obj->lib->Conn, CURLOPT_URL, $obj->lib->API);
			   curl_setopt($obj->lib->Conn, CURLOPT_HEADER, FALSE);
			   curl_setopt($obj->lib->Conn, CURLOPT_NOBODY, FALSE); // remove body
			   curl_setopt($obj->lib->Conn, CURLOPT_RETURNTRANSFER, TRUE);
			   curl_setopt($obj->lib->Conn, CURLOPT_POST, TRUE);
			   curl_setopt($obj->lib->Conn, CURLOPT_POSTFIELDS,  $obj->lib->Data);
			   
			   // execute data proceess on here 
			   $obj->lib->Exec = curl_exec($obj->lib->Conn);
			   $obj->lib->Info = curl_getinfo($obj->lib->Conn, CURLINFO_HTTP_CODE);
			   if( curl_errno($obj->lib->Conn) ){ 
				   $response['error'] = 1;
				   $response['message'] = curl_error($obj->lib->Conn);
				   break;
			   }
			   
			   // close cURL 
			   if( is_resource($obj->lib->Conn)){
				  curl_close($obj->lib->Conn);
			   }
			   
			   // jika nilai balikan Error == 0  
			   $response = json_decode($obj->lib->Exec, true);
			   if(is_array($response) &&( !$response['error'] )){
				   if( $r = $response['data']){
					   $SmsId = $r['APIId'];
					   $UID = $r['APIUniqueId']; 
					   
					   if( $UID ){
						  $this->db->reset_write();
						  $this->db->update( 'serv_sms_inbox', 
							array('UID' => $UID), 
							array('SmsId' => $SmsId)
						  );
					   }
				   }
			   }
		  }	
		  // end then break proc_close(0); 
		  ++ $obj->lib->Skip;
	   }
	   // return exit(0);
	   exit( json_encode($response) );
	}
	
   /**
	* @method 		ivr 
	* @desc			Push data to distrubute by CTI Acd Automaticly 	
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/ivr
	* @param		[]
	*/
	function ivr( $WEB_API_ACTION = 'APIActionAddChanIVR' ){
		 // header data process 	
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   $obj->response = array(
		'event' => 'ivr',
		'error' => 1,
		'message' => 'failed',
		'data' => array( )	
	   );
	   
	   // then check Request Info :
	   
	    // set Lib Api CTI Console from cURL connect to cti Web Api Server
	   $obj = new stdClass();
	   $obj->lib = new stdClass();
	   $obj->lib->API = sprintf("http://%s:%s/%s", WEB_API_HOST, WEB_API_PORT, $WEB_API_ACTION); 
	   $obj->lib->Skip = 0;
	   
	   // then will process until end 
	   while( !$obj->lib->Skip ){
		   
		   // set All parameter data 
		   $obj->lib->Param = array(
			'id' 		=> $_REQUEST['id']		,
			'account' 	=> $_REQUEST['account']	,
			'contact' 	=> $_REQUEST['contact']	,
			'person' 	=> $_REQUEST['person']	,
			'type' 		=> $_REQUEST['type']	,
			'timeout' 	=> $_REQUEST['timeout']
		   );
		   
		   // Break if Invalid Process;
		   if( !isset($obj->lib->Param['id']) ){
			   $obj->response['error'] = 1;
			   $obj->response['message'] = 'Opps, Wrong Data.';
			   break;
		   }
		   
		 // create_function cURL 
		  $obj->lib->Conn = curl_init();
		  if( !is_resource($obj->lib->Conn) ){
			$obj->response['error'] = 1;
			$obj->response['message'] = "Connection Refuse.";
			break;
		  }
			   
		  // sent All data from this overide #param 
		  $obj->lib->Param = http_build_query($obj->lib->Param);
		  
		  // next data 
		  curl_setopt($obj->lib->Conn, CURLOPT_URL, $obj->lib->API);
		  curl_setopt($obj->lib->Conn, CURLOPT_HEADER, FALSE);
		  curl_setopt($obj->lib->Conn, CURLOPT_NOBODY, FALSE); // remove body
		  curl_setopt($obj->lib->Conn, CURLOPT_RETURNTRANSFER, TRUE);
		  curl_setopt($obj->lib->Conn, CURLOPT_POST, TRUE);
		  curl_setopt($obj->lib->Conn, CURLOPT_POSTFIELDS,  $obj->lib->Param);
		 
		 // execute data proceess on here 
		  $obj->lib->Exec = curl_exec($obj->lib->Conn);
		  $obj->lib->Info = curl_getinfo($obj->lib->Conn, CURLINFO_HTTP_CODE);
		  if( curl_errno($obj->lib->Conn) ){ 
			$obj->response['error'] = 1;
			$obj->response['message'] = curl_error($obj->lib->Conn);
			break;
		  }
		  
		 // close cURL 
		  if( is_resource($obj->lib->Conn)){
			 curl_close($obj->lib->Conn);
		  }
		  
		 // get live message & break And skip proc_close;
		  $obj->response = json_decode($obj->lib->Exec, true); 
		  ++ $obj->lib->Skip;
	   }
	   
	   exit( json_encode($obj->response) );
	   
	}
	
	/**
	* @method 		event 
	* @desc			update table after response from CTI triger sent to WS browser  
	* @url 			http://192.168.10.236/smart-btn-collection/index.php/MediaChannel/conivr
	* @param		[]
	*/
	function conivr( $WEB_API_ACTION = 'APIActionConChanIVR'){
	   // header data process 	
	   header( 'Access-Control-Allow-Origin: *' );
	   header( 'Content-Type: application/json; charset=UTF-8' );
	   
	   $obj->response = array(
		'event' => 'conivr',
		'error' => 0,
		'message' => 'failed',
		'data' => array( )	
	   );
	   
	   // set Lib Api CTI Console from cURL connect to cti Web Api Server
	   $obj = new stdClass();
	   $obj->lib = new stdClass();
	   $obj->lib->API = sprintf("http://%s:%s/%s", WEB_API_HOST, WEB_API_PORT, $WEB_API_ACTION); 
	   $obj->lib->Skip = 0;
	   
	   // then will process until end 
	   while( !$obj->lib->Skip ){
		 // check request   
		 // check validation proceess   
		 if( !isset($_REQUEST['verfication'] )){
		    $obj->response['error'] = 1;
		    $obj->response['message'] = "Wrong Parameter";
				break;
		 }
		 
		 // create_function cURL 
		 $obj->lib->Conn = curl_init();
		 if( !is_resource($obj->lib->Conn)){
			$obj->response['error'] = 1;
			$obj->response['message'] = "Invalid Recsource (#0)";
			break;
		 }
			   
		  // sent All data from this;
		 $obj->lib->Data = http_build_query( array(
			'verfication' => $_REQUEST['verfication']			
		 ));
		 
		 // next data 
		 curl_setopt($obj->lib->Conn, CURLOPT_URL, $obj->lib->API);
		 curl_setopt($obj->lib->Conn, CURLOPT_HEADER, FALSE);
		 curl_setopt($obj->lib->Conn, CURLOPT_NOBODY, FALSE); // remove body
		 curl_setopt($obj->lib->Conn, CURLOPT_RETURNTRANSFER, TRUE);
		 curl_setopt($obj->lib->Conn, CURLOPT_POST, TRUE);
		 curl_setopt($obj->lib->Conn, CURLOPT_POSTFIELDS,  $obj->lib->Data);
		 
		 // execute data proceess on here 
		 $obj->lib->Exec = curl_exec($obj->lib->Conn);
		 $obj->lib->Info = curl_getinfo($obj->lib->Conn, CURLINFO_HTTP_CODE);
		 if( curl_errno($obj->lib->Conn) ){ 
			$obj->response['error'] = 1;
			$obj->response['message'] = curl_error($obj->lib->Conn);
			break;
		 }
		 
		 // close cURL 
		 if( is_resource($obj->lib->Conn)){
			curl_close($obj->lib->Conn);
		 }
		 // get live message;
		 $obj->response = json_decode($obj->lib->Exec, true);
		 ++ $obj->lib->Skip;	
	   }
	   
	   // return exit(0);
	   exit( json_encode($obj->response) );
	}
	
 }
 
 ?>